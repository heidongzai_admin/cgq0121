package com.test.demo.controller;

import com.test.demo.entity.TConfig;
import com.test.demo.license.VerifyLicenseUtil;
import com.test.demo.mapper.StaOneMinuteMapper;
import com.test.demo.mapper.SySensorInfoMapper;
import com.test.demo.service.CacheService;
import com.test.demo.service.SensorService;
import com.test.demo.service.TConfigService;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/tConfig")
public class TConfigController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TConfigController.class);

    @Resource(name = "tConfigService")
    private com.test.demo.service.TConfigService tConfigService;

    @Autowired
    private SySensorInfoMapper sySensorInfoMapper;

    @Autowired
    private CacheService cacheService;









    /**
     * 新增修改传感器信息
     */
    @RequestMapping(value = "/addConfig", method = RequestMethod.POST)
    @ResponseBody
    public String addSensorInfo(@RequestBody TConfig sensorInfo, HttpServletRequest request) {
        try {
            LOGGER.info(new ObjectMapper().writeValueAsString(sensorInfo));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                String s = tConfigService.addTConfig(sensorInfo);
                cacheService.reloadConfig();
                return s;

            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }

    }


    /**
     * 配置详情
     */
    @RequestMapping(value = "/queryDetail", method = RequestMethod.GET)
    @ResponseBody
    public String querySensorDetail( HttpServletRequest request) {
//        TConfig tc=new TConfig();
//        tc.setId("1");
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return tConfigService.queryDetail("1");
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    @RequestMapping(value = "/testTable", method = RequestMethod.GET)
    public void testTable(){
        Set<Long> testSet =new HashSet();
        testSet.add(1L);
        List<Long> longs = sySensorInfoMapper.selectUnusedUniqueId(testSet);
        return ;
    }



}
