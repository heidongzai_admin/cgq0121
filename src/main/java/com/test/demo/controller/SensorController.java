package com.test.demo.controller;

import com.alibaba.fastjson.JSON;
import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SensorInfo;
import com.test.demo.license.VerifyLicenseUtil;
import com.test.demo.listener.StartSensorEditTask;
import com.test.demo.service.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sensor")
public class SensorController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorController.class);
    @Resource(name = "sensorService")
    private SensorService sensorService;

    /**
     * 查询传感器列表
     */
    @RequestMapping(value = "/querySensorList", method = RequestMethod.POST)
    @ResponseBody
    public String querySensorList(@RequestBody QueryParamVO queryParamVO, HttpServletRequest request) {
        String domain = request.getServerName();
        try {
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {



                return sensorService.querySensorList(queryParamVO);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }

    }

    /**
     * 新增修改传感器信息
     */
    @RequestMapping(value = "/addSensorInfo", method = RequestMethod.POST)
    @ResponseBody
    public String addSensorInfo(@RequestBody SensorInfo sensorInfo, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return sensorService.addSensorInfo(sensorInfo);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 删除传感器信息
     */
    @RequestMapping(value = "/deleteSensorInfo", method = RequestMethod.GET)
    @ResponseBody
    public String deleteSensorInfo(@RequestParam(value = "sensorId") int sensorId, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return sensorService.deleteSensorInfo(sensorId);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 查询传感器详情
     */
    @RequestMapping(value = "/querySensorDetail", method = RequestMethod.GET)
    @ResponseBody
    public String querySensorDetail(@RequestParam(value = "sensorId") int sensorId, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return sensorService.querySensorDetail(sensorId);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 手动控制定时任务的开关
     */
    @RequestMapping(value = "/switchThread", method = RequestMethod.GET)
    @ResponseBody
    public String switchThread(@RequestParam(value = "switchType") String switchType, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return sensorService.switchThread(switchType, null);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }

    }


}
