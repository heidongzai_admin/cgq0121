package com.test.demo.controller;

import cn.hutool.http.server.HttpServerRequest;
import com.test.demo.entity.TConfig;
import com.test.demo.license.LicenseProperties;
import com.test.demo.license.VerifyLicenseUtil;
import com.test.demo.service.SySensorService;
import com.test.demo.service.TConfigService;
import com.test.demo.service.impl.SySensorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @描述： 数据集获取
 * @作者： lxf
 * @创建日期： 2019年4月30日 上午10:15:17
 */
@Controller
@RequestMapping("/data")
public class DataController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataController.class);


    @RequestMapping(value = "/getData", method = RequestMethod.GET)
    public String getData(HttpServletRequest request) {
        //校验
        try {
            String domain = request.getServerName();
            LOGGER.info("domain:" + domain);
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return "data";
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }

    }

    @RequestMapping(value = "/inValid", method = RequestMethod.GET)
    public String inValid(HttpServletRequest request) {

        return "inValid";

    }

    @RequestMapping(value = "/getSyData", method = RequestMethod.GET)
    public String getSyData(HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return "dataSy";
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    @RequestMapping(value = "/getSyDataFsd", method = RequestMethod.GET)
    public String getSyDataFsd(HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return "dataSyFsd";
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public String config(HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return "config";
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }
}
