package com.test.demo.controller;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SySensorInfoFsd;
import com.test.demo.license.VerifyLicenseUtil;
import com.test.demo.service.CacheService;
import com.test.demo.service.SensorService;
import com.test.demo.service.SySensorFsdService;
import com.test.demo.service.SySensorService;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.management.Sensor;

import javax.annotation.Resource;
import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/sySensorFsd")
public class SySensorFsdController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SySensorFsdController.class);

    @Resource(name = "sySensorFsdService")
    private SySensorFsdService SySensorFsdService;

    @Autowired
    private SensorService sensorService;

    @Autowired
    private CacheService cacheService;


    /**
     * 查询传感器列表
     */
    @RequestMapping(value = "/querySensorList", method = RequestMethod.POST)
    @ResponseBody
    public String querySensorList(@RequestBody QueryParamVO queryParamVO, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return SySensorFsdService.querySensorList(queryParamVO);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 新增修改传感器信息
     */
    @RequestMapping(value = "/addSensorInfo", method = RequestMethod.POST)
    @ResponseBody
    public String addSensorInfo(@RequestBody SySensorInfoFsd sensorInfo, HttpServletRequest request) {
        try {
            LOGGER.info(new ObjectMapper().writeValueAsString(sensorInfo));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                String s = SySensorFsdService.addSensorInfo(sensorInfo);
                //更新缓存
                cacheService.reloadsySensorInfoFsdList();
                cacheService.reloadSySensorInfoFsdCode();
                cacheService.reloadSySensorInfoFsdName();
                cacheService.reloadSySensorInfoFsdUniqueId();
                return s;
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }

    }

    /**
     * 删除传感器信息
     */
    @RequestMapping(value = "/deleteSensorInfo", method = RequestMethod.GET)
    @ResponseBody
    public String deleteSensorInfo(@RequestParam(value = "sensorId") int sensorId, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                String s = SySensorFsdService.deleteSensorInfo(sensorId);
                cacheService.reloadsySensorInfoFsdList();
                cacheService.reloadSySensorInfoFsdCode();
                cacheService.reloadSySensorInfoFsdName();
                cacheService.reloadSySensorInfoFsdUniqueId();
                return s;
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 查询传感器详情
     */
    @RequestMapping(value = "/querySensorDetail", method = RequestMethod.GET)
    @ResponseBody
    public String querySensorDetail(@RequestParam(value = "sensorId") int sensorId, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return SySensorFsdService.querySensorDetail(sensorId);
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }
    /**
     * 查询传感器详情
     */
    @RequestMapping(value = "/getCgqList", method = RequestMethod.GET)
    @ResponseBody
    public List<HashMap<String,Object>> getCgqList( HttpServletRequest request) {
//        try {
//            String domain = request.getServerName();
//            Boolean rel = VerifyLicenseUtil.doVerify(domain);
//            if (rel) {
//                return SySensorFsdService.querySensorDetail(sensorId);
//            } else {
//                return "inValid";
//            }
//        } catch (Exception e) {
//            return "inValid";
//        }
        return SySensorFsdService.getCgqList();
    }

    /**
     * 手动控制定时任务的开关

    @RequestMapping(value = "/switchThread", method = RequestMethod.GET)
    @ResponseBody
    public String switchThread(@RequestParam(value = "switchType") String switchType, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return sensorService.switchThread(switchType, "1");
            } else {
                return "inValid";
            }

        } catch (Exception e) {
            return "inValid";
        }
    } */


}
