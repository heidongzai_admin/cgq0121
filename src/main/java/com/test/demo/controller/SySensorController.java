package com.test.demo.controller;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SensorInfo;
import com.test.demo.entity.SySensorInfo;
import com.test.demo.license.VerifyLicenseUtil;
import com.test.demo.service.CacheService;
import com.test.demo.service.SensorService;
import com.test.demo.service.SySensorService;
import com.test.demo.service.impl.SensorServiceImpl;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/sySensor")
public class SySensorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SySensorController.class);


    @Resource(name = "sensorService")
    private SensorService sensorService;
    @Resource(name = "sySensorService")
    private SySensorService sySensorService;

    @Autowired
    private CacheService cacheService;


    /**
     * 查询传感器列表
     */
    @RequestMapping(value = "/querySensorList", method = RequestMethod.POST)
    @ResponseBody
    public String querySensorList(@RequestBody QueryParamVO queryParamVO, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                LOGGER.info("开启db14任务----");
                //开启db14任务
                sensorService.switchThread("01","0");
                LOGGER.info("开启db14任务完成----");
                LOGGER.info("开启xinyuan任务----");
                //开启新元任务
                sensorService.switchThread("01","1");
                LOGGER.info("xinyuan任务完成----");
                //开启数据库任务
                LOGGER.info("Start shujuku 任务");
                sensorService.switchThread("01","2");
                LOGGER.info("shujuk任务完成----");

                //开启密采任务
                LOGGER.info("Start shujuku 任务");
                sensorService.switchThread("01","3");
                LOGGER.info("micai----");

                return sySensorService.querySensorList(queryParamVO);

            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 查询传感器详情
     */
    @RequestMapping(value = "/getCgqList", method = RequestMethod.GET)
    @ResponseBody
    public List<HashMap<String,Object>> getCgqList(HttpServletRequest request) {
//        try {
//            String domain = request.getServerName();
//            Boolean rel = VerifyLicenseUtil.doVerify(domain);
//            if (rel) {
//                return sySensorService.querySensorDetail(sensorId);
//            } else {
//                return "inValid";
//            }
//        } catch (Exception e) {
//            return "inValid";
//        }
        return sySensorService.getCgqList();
    }
    /**
     * 新增修改传感器信息
     */
    @RequestMapping(value = "/addSensorInfo", method = RequestMethod.POST)
    @ResponseBody
    public String addSensorInfo(@RequestBody SySensorInfo sensorInfo, HttpServletRequest request) {
        try {
            LOGGER.info(new ObjectMapper().writeValueAsString(sensorInfo));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                String s = sySensorService.addSensorInfo(sensorInfo);
                cacheService.reloadsySensorInfoList();
                cacheService.reloadSySensorInfoCode();
                cacheService.reloadSySensorInfoName();
                cacheService.reloadSySensorInfoUniqueId();



                return s;
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }

    }

    /**
     * 删除传感器信息
     */
    @RequestMapping(value = "/deleteSensorInfo", method = RequestMethod.GET)
    @ResponseBody
    public String deleteSensorInfo(@RequestParam(value = "sensorId") int sensorId, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                String s = sySensorService.deleteSensorInfo(sensorId);
                cacheService.reloadsySensorInfoList();
                cacheService.reloadSySensorInfoCode();
                cacheService.reloadSySensorInfoName();
                cacheService.reloadSySensorInfoUniqueId();
                return s;
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 查询传感器详情
     */
    @RequestMapping(value = "/querySensorDetail", method = RequestMethod.GET)
    @ResponseBody
    public String querySensorDetail(@RequestParam(value = "sensorId") int sensorId, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                String s = sySensorService.querySensorDetail(sensorId);



                return s;
            } else {
                return "inValid";
            }
        } catch (Exception e) {
            return "inValid";
        }
    }

    /**
     * 手动控制定时任务的开关
     */
    @RequestMapping(value = "/switchThread", method = RequestMethod.GET)
    @ResponseBody
    public String switchThread(@RequestParam(value = "switchType") String switchType, HttpServletRequest request) {
        try {
            String domain = request.getServerName();
            Boolean rel = VerifyLicenseUtil.doVerify(domain);
            if (rel) {
                return sensorService.switchThread(switchType, "1");
            } else {
                return "inValid";
            }

        } catch (Exception e) {
            return "inValid";
        }
    }


}
