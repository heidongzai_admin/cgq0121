package com.test.demo.util;

import javax.activation.MailcapCommandMap;
import javax.lang.model.element.NestingKind;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhy
 * @Description 项目工具类
 * @ClassName ProjectUtil
 * @Date 2021-1-23 14:03
 **/
public class ProjectUtil {

    /**
     * 计算给定范围内的随机数
     * @param
     * @param
     * @return
     */
    public static Double getRandomNumber(BigDecimal maxAlarm, BigDecimal minAlarm) {
        Double maxDouble = maxAlarm.doubleValue();
        Double minDouble = minAlarm.doubleValue();
        Double doubleRandom=null;
        //最大值和最小值之间的随机数
        doubleRandom=minDouble+(Double)(Math.random()*(maxDouble-minDouble));

        if (doubleRandom.equals(maxDouble)||doubleRandom.equals(minDouble)) {
            return getRandomNumber(maxAlarm,minAlarm);
        }
        return doubleRandom;
    }

    /**
     * 判断目标值是否在范围内
     * @Function: judgmentIsRange
     * @param maxAlarm 最大值
     * @param minAlarm 最小值
     * @param value  目标值
     * @Return boolean true 在  false 不在
     *
     * @author: zhy
     * @date: 2021-01-22 11:19
     */
    public static boolean judgmentIsRange(BigDecimal maxAlarm, BigDecimal minAlarm, BigDecimal value) {
        if (value.compareTo(maxAlarm)<=0 && value.compareTo(minAlarm)>=0) {
            return true;
        }
        return false;
    }

    public static boolean judgmentIsRange(BigDecimal maxAlarm, BigDecimal minAlarm, BigDecimal maxValue,BigDecimal minValue) {
        if (maxValue.compareTo(maxAlarm)<=0 && minValue.compareTo(minAlarm)>=0) {
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        BigDecimal maxAlarm=new BigDecimal(10);

        BigDecimal avgAlarm=new BigDecimal(5);

        Map<String,BigDecimal> staOneMinute=new HashMap<>();
        staOneMinute.put("MaxValue",new BigDecimal(7));


        if (!ProjectUtil.judgmentIsRange(maxAlarm,avgAlarm,(BigDecimal)staOneMinute.get("MaxValue")==null?new BigDecimal(0):(BigDecimal)staOneMinute.get("MaxValue"))) {
           Double maxValue=ProjectUtil.getRandomNumber(maxAlarm,avgAlarm);
            System.out.println(maxValue);
        }else{
            System.out.println("在合理范围内，哈哈哈");
        }



    }

}
