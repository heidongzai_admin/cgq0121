package com.test.demo.util;

import com.test.demo.entity.TConfig;
import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @Author zhy
 * @Description 时间工具类
 * @ClassName DateUtil
 * @Date 2021-1-23 10:30
 **/
@Slf4j
public class DateUtil {

    /**
    * 比较目标时间是否在时间范围内
    * @Function: isEffectiveDate
    * @param nowTimeString 目标时间 'HH:mm:ss'
    * @param startTimeString 开始时间
    * @param endTimeString 结束时间
    * @Return boolean true在范围内，false不在范围内
    *
    * @author: zhy
    * @date: 2021-01-23 10:41
    */
    public static boolean isEffectiveDate(String nowTimeString, String startTimeString, String endTimeString){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date nowTime=null;
        Date startTime=null;
        Date endTime=null ;
        try {
            nowTime= dateFormat.parse(nowTimeString);
            startTime = dateFormat.parse(startTimeString);
            endTime= dateFormat.parse(endTimeString);
        } catch (ParseException e) {
            log.error("============================日期转换失败============================");
            e.printStackTrace();
        }
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }

    }


    public static Map<String,Date> dateFormatBeignEndTime(TConfig tConfig) {
        Map<String,Date> result = new HashMap<>();
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = dateFormat.format(date);
        String beginTimeString = format+" "+tConfig.getFsdBeginTime();
        String endTimeString=format+" "+tConfig.getFsdEndTime();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            result.put("beginTime",dateFormat.parse(beginTimeString));
            result.put("endTime",dateFormat.parse(endTimeString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static void main(String[] args) {
        String nowTime="2021-1-23 10:36:00";
        String[] s = nowTime.split(" ");
        System.out.println(s[1]);

        if (isEffectiveDate(s[1],"06:00:00","15:00:00")) {
            System.out.println("是");
        }else {
            System.out.println("不是");
        }
    }


}
