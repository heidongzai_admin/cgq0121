package com.test.demo.license;



import cn.hutool.core.io.FileUtil;
import cn.hutool.core.net.URLDecoder;
import sun.security.util.SecurityConstants;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class RSATester {


    static String publicKey;
    static String privateKey;

    static {
        try {
            Map<String, Object> keyMap = RSAUtils.genKeyPair();
//             publicKey = RSAUtils.getPublicKey(keyMap);
//             privateKey = RSAUtils.getPrivateKey(keyMap);
            publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCagWXCpLlk/X3Alvr6mDoYyXtIcnd5Bbe9Tvan6+dy5MkDJ5urNVX71Fp3bObKjkDod+fth4cOOu+wEtD8MI5ycnDQZDDB5YylKhl68q6eZnMOZ20u/eG3TfaNmQwjcuSZeCxhBF99qnA+Vn67xYTHqPCBVIxbcRtghIp9EVvV6wIDAQAB";
            privateKey="MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJqBZcKkuWT9fcCW+vqYOhjJe0hyd3kFt71O9qfr53LkyQMnm6s1VfvUWnds5sqOQOh35+2Hhw4677AS0PwwjnJycNBkMMHljKUqGXryrp5mcw5nbS794bdN9o2ZDCNy5Jl4LGEEX32qcD5WfrvFhMeo8IFUjFtxG2CEin0RW9XrAgMBAAECgYBpbcKyfxTDBPa6ZEv+k8GRCGtHlFRSEMTAORG2pBLbI6LGtFBVE6AIp27T70vNDCTefHOWcdSGsVC9JRnV/Iw2G48D6lRGYL8nNP8FJqmfB9i1GN03CrZzFFdhGmx18JnWWDQR8KNe+Ji9ZzkWyaNjBhD5jxk/XTAkqqCSqXN+gQJBANsdT/bUsRIwpw6kdS+HlK+OHNB8HppEUjXBpLYwg1MZYCPx8FZZ29axVlThahpYH84Mqw64SqzsWZ3E3CVjq68CQQC0g8YTRxZK2vxAbLib9UVVHwfWZtofBtSAGpdzUAl40ppnrYNt4IoguwZqRGT6nj/AlIU2m6dOlQzLFMebm5yFAkBrjK+7UBq5+veE0GRUuA/bO74Y91NuFSrlB+95HcbX+xSdkBuPxvuJHfJqJ1qVI7w4vy8XQo4k4r36Gq6UC6AxAkBY4mHP8Drr1PRMjLxCHv1qsoLbcX2JtYVBJtbYMtrXIWxiDLAglGWvRCgef1rxhKaDnx4RC4fQS9gmzy6SUoc5AkA6Gs1wy5fN+RWU63AbFN9sylsFJuYC1QgjzvLkG3188gCHfnOUy5Y4aLw5SgZ+1yM0PbeCY0o11Q0gYVo4S8FO";
            System.err.println("公钥: \n\r" + publicKey);
            System.err.println("私钥： \n\r" + privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
       // test();
       // testSign();
        testAuth();
    }

    static void test() throws Exception {
        System.err.println("公钥加密——私钥解密");
        String source = "这是一行没有任何意义的文字，你看完了等于没看，不是吗？";
        System.out.println("\r加密前文字：\r\n" + source);
        byte[] data = source.getBytes();
        byte[] encodedData = RSAUtils.encryptByPublicKey(data, publicKey);
        System.out.println("加密后文字：\r\n" + new String(encodedData));
        byte[] decodedData = RSAUtils.decryptByPrivateKey(encodedData, privateKey);
        String target = new String(decodedData);
        System.out.println("解密后文字: \r\n" + target);
    }

    static void testSign() throws Exception {
        System.err.println("私钥加密——公钥解密");
        String source = "这是一行测试RSA数字签名的无意义文字";
        System.out.println("原文字：\r\n" + source);
        byte[] data = source.getBytes();
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        System.out.println("加密后：\r\n" + new String(encodedData));
        byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
        String target = new String(decodedData);
        System.out.println("解密后: \r\n" + target);
        System.err.println("私钥签名——公钥验证签名");
        String sign = RSAUtils.sign(encodedData, privateKey);
        System.err.println("签名:\r" + sign);
        boolean status = RSAUtils.verify(encodedData, publicKey, sign);
        System.err.println("验证结果:\r" + status);
    }

    static  void testAuth() throws Exception {
        LicensePropertiesGen licenseProperties=new LicensePropertiesGen();
        licenseProperties.setStartStopTime("2020-03-01~2021-04-30");
        licenseProperties.setDomain("127.0.0.1");
        BeanPropertyBox beanPropertyBox=new BeanPropertyBox(licenseProperties);
        ThinkIterator iterator=beanPropertyBox.iterator(LicenseIterator.class);
        Map<String,Object> map=new LinkedHashMap<>();
        while (iterator.hasNext()){
          Map<String,Object> param= (Map<String,Object>) iterator.next();
          if(!param.isEmpty()){
              map.putAll(param);
          }
        }
        String source=map.toString();
        byte[] data = source.getBytes();
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        System.out.println("加密后：\r\n" + new String(encodedData));

        byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
        String target = new String(decodedData);
        System.out.println("解密后: \r\n" + target);
        String sign = RSAUtils.sign(data, privateKey).replace("\r\n","");
        System.out.println("签名:" + sign);

        data= (source).getBytes();

        boolean status = RSAUtils.verify(data, publicKey, sign);
        System.out.println("签名验证结果:"+status);
        map.put("signaturer",sign);

        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();

        String str="\r\n";
        StringBuffer bytes=new StringBuffer("");
        while(it.hasNext()){
            Map.Entry<String, Object> entry = it.next();
            bytes.append(entry.getKey()+ ":"+entry.getValue()+str);
            System.out.println("key:"+entry.getKey()+"  key:"+entry.getValue());
        }


        //Base64Utils.byteArrayToFile(bytes.toString().getBytes(), "G:/taiyuemng/taiyuemng/license.dat");
        Base64Utils.byteArrayToFile(bytes.toString().getBytes(),"E:/taiyuemng/taiyuemng/license.dat");
        System.err.println("验证结果:\r" + status);
    }

}
