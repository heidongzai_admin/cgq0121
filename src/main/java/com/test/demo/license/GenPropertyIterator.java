package com.test.demo.license;

public interface GenPropertyIterator {

    ThinkIterator iterator(Class cls);
}
