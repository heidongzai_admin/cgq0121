package com.test.demo.license;
import cn.hutool.core.date.DateUtil;
import com.test.demo.properties.Checker;
import com.test.demo.service.SensorService;
import com.test.demo.service.impl.SensorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;

/** */

/**
 * <p> 
 * RSA公钥/私钥/签名工具包 
 * </p> 
 * <p> 
 * 罗纳德·李维斯特（Ron [R]ivest）、阿迪·萨莫尔（Adi [S]hamir）和伦纳德·阿德曼（Leonard [A]dleman） 
 * </p> 
 * <p> 
 * 字符串格式的密钥在未在特殊说明情况下都为BASE64编码格式<br/> 
 * 由于非对称加密速度极其缓慢，一般文件不使用它来加密而是使用对称加密，<br/> 
 * 非对称加密算法可以用来对对称加密的密钥加密，这样保证密钥的安全也就保证了数据的安全 
 * </p> 
 *
 * @author IceWee
 * @date 2012-4-26 
 * @version 1.0
 */
public class VerifyLicenseUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(VerifyLicenseUtil.class);

    /** *//**
     * 加密算法RSA 
     */
    public static final String KEY_ALGORITHM = "RSA";

    /** *//**
     * 签名算法 
     */
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";


    /** *//**
     * <p> 
     * 校验数字签名 
     * </p> 
     *
     * @param data 已加密数据 
     * @param publicKey 公钥(BASE64编码) 
     * @param sign 数字签名 
     *
     * @return
     * @throws Exception
     *
     */
    public static boolean verify(byte[] data, String publicKey, String sign)
            throws Exception {
        byte[] keyBytes = Base64Utils.decode(publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PublicKey publicK = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(publicK);
        signature.update(data);
        return signature.verify(Base64Utils.decode(sign));
    }

    private static boolean verifyDomain( String domian,String domainWhite){
        List<String> domains = Arrays.asList(domainWhite.split("\\|"));
        return domains.contains(domian);
    }

    private static  boolean verifyDate(String dateStr){
        Date now = DateUtil.date(),start=null,end=null;
        try {
            String[] startEnd=dateStr.split("~");
            start = DateUtil.parse(startEnd[0]);
            end = DateUtil.parse(startEnd[1]);
            int isSatrt=DateUtil.compare(now,start);
            int isEnd=DateUtil.compare(now,end);
            if(isSatrt==-1){
//                log.error("license证书授权未生效");
                return false;
            }
            if(isEnd==1){
//                log.error("license证书授权已过期");
                return false;
            }
        }catch (Exception e){
//            log.error("证书日期不合法!");
            return false;
        }
        return true;
    }
    public static boolean doVerify(String domain){
//        Map mapMsg = (Map) msg;
//        String domian = (mapMsg.containsKey(Channel.DOMAIN) && Checker.BeNotNull(mapMsg.get(Channel.DOMAIN))) ?
//                mapMsg.get(Channel.DOMAIN).toString():null;
//        if(Checker.BeNotBlank(domian)){
//            if(licenseProperties.needVerifyDomain()){

                boolean verifyRes= verifyDomain(domain,LicenseProperties.getDomain());
                if(!verifyRes){
                    LOGGER.error("域名校验未通过!");
                    return false;
                }
//            }
//            if(licenseProperties.needVerifyDate()){
                 verifyRes= verifyDate(LicenseProperties.getStartStopTime());
                if(!verifyRes){
                    LOGGER.error("时间校验未通过!");
                    return false;
                }
//            }
            boolean legal = verify(LicenseProperties.getDomain(),LicenseProperties.getStartStopTime(),LicenseProperties.getSignaturer());
            if(!legal){
                LOGGER.error("证书不合法!");
                return legal;
            }
//        }
        return true;
    }
    public static boolean doVerifyDate(){
//        Map mapMsg = (Map) msg;
//        String domian = (mapMsg.containsKey(Channel.DOMAIN) && Checker.BeNotNull(mapMsg.get(Channel.DOMAIN))) ?
//                mapMsg.get(Channel.DOMAIN).toString():null;
//        if(Checker.BeNotBlank(domian)){
//            if(licenseProperties.needVerifyDomain()){

//        boolean verifyRes= verifyDomain(domain,LicenseProperties.getDomain());
//        if(!verifyRes){
//            LOGGER.error("域名校验未通过!");
//            return false;
//        }
//            }
//            if(licenseProperties.needVerifyDate()){
        boolean verifyRes= verifyDate(LicenseProperties.getStartStopTime());
        if(!verifyRes){
            LOGGER.error("时间校验未通过!");
            return false;
        }
//            }
        boolean legal = verify(LicenseProperties.getDomain(),LicenseProperties.getStartStopTime(),LicenseProperties.getSignaturer());
        if(!legal){
            LOGGER.error("证书不合法!");
            return legal;
        }
//        }
        return true;
    }
    public static  boolean verify(String domain,String startStopTime,String signaturer){
        LicensePropertiesGen licenseProperties=new LicensePropertiesGen();
        licenseProperties.setStartStopTime(startStopTime);
        licenseProperties.setDomain(domain);
        BeanPropertyBox beanPropertyBox=new BeanPropertyBox(licenseProperties);
        ThinkIterator iterator=beanPropertyBox.iterator(LicenseIterator.class);
        Map<String,Object> map=new LinkedHashMap<>();
        while (iterator.hasNext()){
            Map<String,Object> param= (Map<String,Object>) iterator.next();
            if(!param.isEmpty()){
                map.putAll(param);
            }
        }
        String source=map.toString();
        try {
//            String str=LicenseProperties.getString();
            return RSAUtils.verify(source.getBytes(), publicKey, signaturer);
        } catch (Exception e) {

            LOGGER.error(e.getMessage(),e);
            return false;
        }

    }


    private static String publicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCagWXCpLlk/X3Alvr6mDoYyXtIcnd5Bbe9Tvan6+dy5MkDJ5urNVX71Fp3bObKjkDod+fth4cOOu+wEtD8MI5ycnDQZDDB5YylKhl68q6eZnMOZ20u/eG3TfaNmQwjcuSZeCxhBF99qnA+Vn67xYTHqPCBVIxbcRtghIp9EVvV6wIDAQAB";

//    public static String getString() {
//        Properties properties = new Properties();
//        // 使用ClassLoader加载properties配置文件生成对应的输入流
//        InputStream in = SensorService.class.getClassLoader().getResourceAsStream("sys-params.properties");
//        readFilePath = properties.getProperty("readFilePath");
//        // 使用properties对象加载输入流
//        try {
//            properties.load(in);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        //获取key对应的value值
//        readFilePath = properties.getProperty("readFilePath");
//        return "version="+version+",organization="+organization+",domain="+domain+",startStopTime="+startStopTime+",authorizeDesc="+authorizeDesc+",copyrightOwner="+copyrightOwner;
//    }

}