package com.test.demo.license;

import cn.hutool.core.io.FileUtil;
import com.test.demo.properties.Checker;
import com.test.demo.properties.LicenseMark;
import com.test.demo.service.SensorService;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;

//import com.test.demo.properties.ThinkItProperties;


public class LicensePropertiesGen {


    public boolean needVerifyDomain(){
        return !("*".equals(domain));
    }

    public boolean needVerifyDate(){
        return !("*".equals(startStopTime));
    }

    public boolean verify(){
        try {
            return RSAUtils.verify(toString().getBytes(), publicKey, signaturer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @PostConstruct
    public void init(){
        Properties properties_ = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = SensorService.class.getClassLoader().getResourceAsStream("sys-params.properties");
        // 使用properties对象加载输入流
        try {
            properties_.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //获取key对应的value值
        String license = properties_.getProperty("license");
//        writeParentFilePath = properties.getProperty("writeParentFilePath");
//        cron = properties.getProperty("cron");
//        readFilePathSy = properties.getProperty("readFilePathSy");
//        writeParentFilePathSy = properties.getProperty("writeParentFilePathSy");
//        cronSy = properties.getProperty("cronSy");


        boolean licenseExist = FileUtil.exist(license);
        if(licenseExist){
            File file = new File(license);
            List<String> properties=new ArrayList();
            properties=FileUtil.readUtf8Lines(file,properties);
            if(Checker.BeNotEmpty(properties)){
                Map<String,String> propertieMap = new HashMap<>();
                for(String propertie:properties){
                    String[] parr = propertie.split("\\:");
                    propertieMap.put(parr[0],parr[1]);
                }
                buildProperties(propertieMap);
            }
        }
    }

    private void buildProperties(Map<String,String> propertieMap){
        Field[] fields=this.getClass().getDeclaredFields();
        if(Checker.BeNotEmpty(fields)){
            for(Field field:fields){
                field.setAccessible(true);
                if(field.getAnnotation(LicenseMark.class)!=null){
                    String value =propertieMap.get(field.getName());
                    try {
                        field.set(this,value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Getter
    @LicenseMark
    private String version;

    @Getter
    @LicenseMark
    private String  organization;

    @Getter
    @Setter
    @LicenseMark
    private static String domain;

    @Getter
    @Setter
    @LicenseMark
    private static String startStopTime;

    @Getter
    @LicenseMark
    private String authorizeDesc ;

    @Getter
    @LicenseMark
    private String copyrightOwner;

    @Getter
    @LicenseMark
    private String signaturer;

    private String publicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCkD7vzjM6IFj9dBftSbf0piS9A9mLW8YnGAV4Rdy1Z3P0wJ+lGnBDWNWlKX7cRtHsGsodar4vJelgOjQWNMDqlT1ngJEbz3EbJlwem7d0e59Bww1HF+DuPBM1/+jpspjk5zy1bgLmSrH8tJ5LX/O0t4A+BtnvZPUZKCBytAbxJlwIDAQAB";

    public static String getString() {
//        return "version="+version+",organization="+organization+",domain="+domain+",startStopTime="+startStopTime+",authorizeDesc="+authorizeDesc+",copyrightOwner="+copyrightOwner;

        return "domain="+domain+",startStopTime="+startStopTime;
    }
}
