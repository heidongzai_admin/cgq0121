package com.test.demo.listener;

import com.test.demo.service.SensorService;
import com.test.demo.service.impl.SySensorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartSensorEditTask implements ServletContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(StartSensorEditTask.class);
    @Autowired
    private SensorService sensorService;

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub

    }





    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOGGER.info("开启db14任务----");
        //开启db14任务
        sensorService.switchThread("01","0");
        LOGGER.info("开启db14任务完成----");
        LOGGER.info("开启开启新元任务----");
        //开启新元任务
        sensorService.switchThread("01","1");
        LOGGER.info("开启开启新元任务完成----");

    }
}
