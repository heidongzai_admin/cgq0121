package com.test.demo.listener;

import com.test.demo.service.SensorService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import java.io.IOException;

public class InitFilter implements Filter {
    @Autowired
    private SensorService sensorService;
    @Override  
    public void destroy() {  
  
    }  
  
    @Override  
    public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException,
            ServletException {
  
    }  
  
    @Override  
    public void init(FilterConfig config) throws ServletException {
        //开启db14任务
        sensorService.switchThread("01","0");
        //开启新元任务
        sensorService.switchThread("01","1");
    }  
} 