package com.test.demo.listener;

import com.test.demo.service.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class InitServlet  extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(StartSensorEditTask.class);
    @Autowired
    private SensorService sensorService;
    /**
     */
    private static final long serialVersionUID = 1L;

    @Override
    public void init(ServletConfig config) {
        try {
            super.init();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        LOGGER.info("开启db14任务----------");
        //开启db14任务
        sensorService.switchThread("01","0");
        LOGGER.info("开启db14任务完成----------");
        LOGGER.info("开启开启新元任务----------");
        //开启新元任务
        sensorService.switchThread("01","1");
        LOGGER.info("开启开启新元任务完成----------");
    }
}

