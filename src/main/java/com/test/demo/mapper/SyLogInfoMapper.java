package com.test.demo.mapper;

import com.test.demo.entity.LogInfo;
import com.test.demo.entity.SyLogInfo;
import com.test.demo.util.MyMapper;
import org.apache.ibatis.annotations.Param;

public interface SyLogInfoMapper extends MyMapper<SyLogInfo> {
    SyLogInfo querySyLogInfoByName(@Param("fileName") String fileName);
}