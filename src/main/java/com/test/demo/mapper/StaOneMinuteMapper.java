package com.test.demo.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author zhy
 * @Description
 * @ClassName StaOneMinuteMapper
 * @Date 2021-1-21 18:22
 **/
@Repository
public interface StaOneMinuteMapper {

    /**
     * 根据条件查询数量
     *
     * @param uniqueIds
     * @param statID
     * @param format    但前日期
     * @Function: selectCountByUniquesAndStatId
     * @Return java.lang.Integer
     * @author: zhy
     * @date: 2021-01-22 0:02
     */
    Integer selectCountByUniquesAndStatId(@Param("uniqueIds") List<Long> uniqueIds, @Param("statID") List<Integer> statID, @Param("format") String format);

    List<Map> selectByuniquesAndStatIdLimit(@Param("uniqueIds") List<Long> uniqueIds, @Param("statID") List<Integer> statID, @Param("format") String format, @Param("size") int size, @Param("beginTime") Date beginTime, @Param("endTime") Date endTime, @Param("TMPTableName") String TMPTableName);

    /**
     * 根据id更新
     *
     * @param maxValue
     * @param minValue
     * @param avgValue
     * @param formStatId
     * @param id
     * @param format
     * @Function: updateByUnique
     * @Return void
     * @author: zhy
     * @date: 2021-01-22 12:29
     */
    void updateByUnique(@Param("maxValue") Double maxValue, @Param("minValue") Double minValue, @Param("avgValue") Double avgValue, @Param("formStatId") int formStatId, @Param("id") Long id, @Param("format") String format);

    /**
     * 创建临时表名称
     *
     * @param tableName
     * @param format
     * @Function: createTable
     * @Return void
     * @author: zhy
     * @date: 2021-01-22 21:49
     */
    void createTable(@Param("tableName") String tableName, @Param("format") String format);

    /**
     * 添加已修改临时表数据
     *
     * @param id
     * @param tmpTableName
     * @Function: insertTmpTbale
     * @Return void
     * @author: zhy
     * @date: 2021-01-22 21:49
     */
    void insertTmpTbale(@Param("id") Long id, @Param("tmpTableName") String tmpTableName);
}
