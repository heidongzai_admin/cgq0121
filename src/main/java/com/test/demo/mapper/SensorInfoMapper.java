package com.test.demo.mapper;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SensorInfo;
import com.test.demo.util.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorInfoMapper extends MyMapper<SensorInfo> {

    List<SensorInfo> selectSensorList(QueryParamVO queryParamVO);

    int getSensorCount(QueryParamVO queryParamVO);

    SensorInfo querySensorDetailByXMLCode(@Param("ssTransducerCode") String ssTransducerCode);
}