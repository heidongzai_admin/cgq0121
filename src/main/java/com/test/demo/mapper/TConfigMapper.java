package com.test.demo.mapper;

import com.test.demo.entity.TConfig;
import com.test.demo.util.MyMapper;

public interface TConfigMapper extends MyMapper<TConfig> {

    TConfig queryDetail();


}