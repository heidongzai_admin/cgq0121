package com.test.demo.mapper;

import com.test.demo.entity.LogInfo;
import com.test.demo.util.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LogInfoMapper extends MyMapper<LogInfo> {

    LogInfo queryLogInfoByName(@Param("fileName") String fileName);
}