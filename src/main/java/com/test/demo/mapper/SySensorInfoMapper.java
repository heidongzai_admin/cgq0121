package com.test.demo.mapper;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SensorInfo;
import com.test.demo.entity.SySensorInfo;
import com.test.demo.util.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SySensorInfoMapper extends MyMapper<SySensorInfo> {
    List<SySensorInfo> selectSensorList(QueryParamVO queryParamVO);

    int getSensorCount(QueryParamVO queryParamVO);

    List<HashMap<String, Object>> getCgqList();

    SySensorInfo querySySensorDetail(
            @Param("name") String name);

    SySensorInfo querySySensorDetailByCode(
            @Param("code") String code);

    List<String> selectUniqueId();

    /**
     * 根据uniqueId查询全时段
     *
     * @param uniqueId
     * @Function: selectByUniqueId
     * @Return com.test.demo.entity.SySensorInfo
     * @author: zhy
     * @date: 2021-01-22
     */
    SySensorInfo selectByUniqueId(Long uniqueId);

    /**
     * 查询uniqueId
     *
     * @Function: selectUniqueIdWithString
     * @Return java.util.List<java.lang.String>
     * @author: zhy
     * @date: 2021-01-22 13:40
     */
    List<String> selectUniqueIdWithString();

    /**
     * 查询全时段uniqueId 以及未 被分时段操作的uniqueId
     *
     * @param fsdUniqueIdSet
     * @Function: selectUnusedUniqueId
     * @Return java.util.List<java.lang.Long>
     * @author: zhy
     * @date: 2021-01-22 21:57
     */
    List<Long> selectUnusedUniqueId(@Param("fsdUniqueIdSet") Set fsdUniqueIdSet);

    SySensorInfo selectByCode(String ssTransducerCode);

    List<Map<String, Object>> selectCodeAndUniqueId();

    void updateUnqiueIdByCode(@Param("name") String name, @Param("uniqueId") Long uniqueId);
}