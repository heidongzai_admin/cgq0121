package com.test.demo.mapper;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SySensorInfoFsd;
import com.test.demo.util.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SySensorInfoFsdMapper extends MyMapper<SySensorInfoFsd> {
    List<SySensorInfoFsd> selectSensorList(QueryParamVO queryParamVO);

    int getSensorCount(QueryParamVO queryParamVO);

    SySensorInfoFsd querySySensorDetail(@Param("fenzhan") String fenzhan,
                                        @Param("kuangming") String kuangming,
                                        @Param("code") String code,
                                        @Param("type") String type,
                                        @Param("name") String name);

    List<HashMap<String, Object>> getCgqList();

    List<HashMap<String, Object>> updateStatus(@Param("statusCd") String statusCd);


    List<String> selectUniqueIdList();

    /**
     * 根据uniqueId查询分时段
     *
     * @param uniqueId
     * @Function: selectByUniqueId
     * @Return com.test.demo.entity.SySensorInfo
     * @author: zhy
     * @date: 2021-01-22
     */
    SySensorInfoFsd selectByUniqueId(Long uniqueId);

    /**
     * 根据name查找目标
     *
     * @param name
     * @Function: selectSySensorFsdDetailByName
     * @Return com.test.demo.entity.SySensorInfoFsd
     * @author: zhy
     * @date: 2021-01-23 10:48
     */
    SySensorInfoFsd selectSySensorFsdDetailByName(String name);

    SySensorInfoFsd selectByCode(String ssTransducerCode);

    List<Map<String, Object>> selectCodeAndUniqueId();


    void updateUnqiueIdByCode(@Param("name") String name, @Param("uniqueId") Long uniqueId);
}