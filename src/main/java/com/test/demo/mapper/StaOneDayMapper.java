package com.test.demo.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface StaOneDayMapper {
    void updateByUnique(@Param("maxValue") Double maxValue, @Param("minValue") Double minValue, @Param("avgValue") Double avgValue, @Param("id") Long id, @Param("format") String format);

    List<Map> selectByuniquesAndStatIdLimit(@Param("uniqueIds") List<Long> uniqueIds, @Param("statID") List<Integer> statID, @Param("format") String format, @Param("size") int size, @Param("beginTime") Date beginTime, @Param("endTime") Date endTime, @Param("TMPTableName") String TMPTableName);


}
