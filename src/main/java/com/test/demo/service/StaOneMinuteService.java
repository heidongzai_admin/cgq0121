package com.test.demo.service;

import com.test.demo.entity.TConfig;

import java.util.Date;
import java.util.List;

public interface StaOneMinuteService {
    /**
     *  执行查询并修改StaOneMinute表
     */

    void operationStaOneMinute(List<Long> uniqueList, String statID, Date date, int timeStatus, TConfig tconfig);

}
