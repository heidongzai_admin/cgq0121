package com.test.demo.service;

import com.test.demo.entity.TConfig;


public interface TConfigService {

    String addTConfig(TConfig sensorInfo);

    String queryDetail(String sensorId);
    public TConfig queryDetailObj(String sensorId);

}
