package com.test.demo.service;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SyLogInfo;

import com.test.demo.entity.SySensorInfoFsd;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface SySensorFsdService {

    String querySensorList(QueryParamVO queryParamVO);

    String addSensorInfo(SySensorInfoFsd sensorInfo);

    String deleteSensorInfo(int sensorId);

    String querySensorDetail(int sensorId);

    SySensorInfoFsd querySensorDetailByXMLCode(String ssTransducerCode);
    /**
     * 沈阳新元，读取源文件夹所有文件，修改其中某文件，然后整体拷贝到另外一个文件夹中
     */
    public void readfileSy(String filepath) throws FileNotFoundException, IOException;
    public void readProperties()throws IOException;
    boolean queryLogInfoByName(String fileName);

    boolean insertIntoFileLog(SyLogInfo logInfo);

    String switchThread(String switchType);
    List<HashMap<String,Object>> getCgqList();
}
