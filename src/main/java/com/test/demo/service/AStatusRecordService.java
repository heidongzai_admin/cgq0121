package com.test.demo.service;

import com.test.demo.entity.TConfig;

import java.util.Date;
import java.util.List;

public interface AStatusRecordService {
    void operationStaOneMinute(List<Long> uniqueList, String statID, Date date, int timeStatus, TConfig tconfig);

}
