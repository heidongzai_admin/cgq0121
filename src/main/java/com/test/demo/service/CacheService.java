package com.test.demo.service;

import com.test.demo.entity.SySensorInfo;
import com.test.demo.entity.SySensorInfoFsd;
import com.test.demo.entity.TConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sun.corba.se.impl.util.RepositoryId.cache;

public interface CacheService {
    /**
    * 删除缓存
    * @Function: removeCache
    * @param key
    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:55
    */
    void removeCache(String key);

    /**
    * 获取tconfig配置
    * @Function: getConfig

    * @Return com.test.demo.entity.TConfig
    *
    * @author: zhy
    * @date: 2021-01-28 23:55
    */
    TConfig getConfig();

    /**
    * 获取分时段uniqueIdList
    * @Function: getSySensorInfoFsd

    * @Return java.util.List<java.lang.Long>
    *
    * @author: zhy
    * @date: 2021-01-28 23:55
    */
    List<Long> getSySensorInfoFsd();
    
    /** 
    * 获取全时段unqieIDList
    * @Function: getSySensorInfo 
    
    * @Return java.util.List<java.lang.Long>
    *
    * @author: zhy
    * @date: 2021-01-28 23:57
    */
    
    List<Long> getSySensorInfo();

    /** 
    * 获取全时段时段Map<code,SysensorInfoFsd>
    * @Function: getSySensorInfoCode 
    
    * @Return java.util.Map<java.lang.String,com.test.demo.entity.SySensorInfo>
    *
    * @author: zhy
    * @date: 2021-01-28 23:57
    */
    Map<String , SySensorInfo> getSySensorInfoCode();
    
    /** 
    * 重载全时段Map<code,SysensorInfoFsd>
    * @Function: reloadSySensorInfoCode 
    
    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:57
    */
    void reloadSySensorInfoCode();
    
    /** 
    * 获取全时段Map<name,SysensorInfoFsd>
    * @Function: getSySensorInfoName 
    
    * @Return java.util.Map<java.lang.String,com.test.demo.entity.SySensorInfo>
    *
    * @author: zhy
    * @date: 2021-01-28 23:58
    */
    Map<String , SySensorInfo> getSySensorInfoName();
    
    /** 
    * 重载全时段Map<name,SysensorInfo>
    * @Function: reloadSySensorInfoName 
    
    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:58
    */
    void reloadSySensorInfoName();


    /**
    * 获取全时段Map<unqieuId,SysensorInfo>
    * @Function: getSySensorInfoUniqueId

    * @Return java.util.Map<java.lang.Long,com.test.demo.entity.SySensorInfo>
    *
    * @author: zhy
    * @date: 2021-01-28 23:54
    */
    Map<Long , SySensorInfo> getSySensorInfoUniqueId();
    
    /** 
    * 重载全时段Map<unqieuId,SysensorInfoFsd>
    * @Function: reloadSySensorInfoUniqueId 

    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:54
    */
    void reloadSySensorInfoUniqueId();

    /**
    * 获取分时段Map<unqieuId,SysensorInfoFsd>
    * @Function: getSySensorInfoFsdCode

    * @Return java.util.Map<java.lang.String,com.test.demo.entity.SySensorInfoFsd>
    *
    * @author: zhy
    * @date: 2021-01-28 23:53
    */
    Map<String , SySensorInfoFsd> getSySensorInfoFsdCode();

    /**
    * 重载分时段Map<code,SysensorInfoFsd>
    * @Function: reloadSySensorInfoFsdCode

    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:53
    */
    void reloadSySensorInfoFsdCode();

    /**
    * 获取分时段Map<name,SysensorInfoFsd>
    * @Function: getSySensorInfoFsdName

    * @Return java.util.Map<java.lang.String,com.test.demo.entity.SySensorInfoFsd>
    *
    * @author: zhy
    * @date: 2021-01-28 23:52
    */
    Map<String , SySensorInfoFsd> getSySensorInfoFsdName();


    /**
    * 重载分时段Map<name,SysensorInfoFsd>
    * @Function: reloadSySensorInfoFsdName

    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:51
    */
    void reloadSySensorInfoFsdName();

    /**
    * 获取分时段Map<unqieuId,SysensorInfoFsd>
    * @Function: getSySensorInfoFsdUniqueId 

    * @Return java.util.Map<java.lang.Long,com.test.demo.entity.SySensorInfoFsd>
    *
    * @author: zhy
    * @date: 2021-01-28 23:51
    */
    Map<Long , SySensorInfoFsd> getSySensorInfoFsdUniqueId();


    /**
    * 重载分时段Map<unqieuId,SysensorInfoFsd>
    * @Function: reloadSySensorInfoFsdUniqueId

    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:50
    */
    void reloadSySensorInfoFsdUniqueId();
    
    /** 
    * 重载tconf配置
    * @Function: reloadConfig 
    
    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-28 23:50
    */
    void reloadConfig();

    /**
    * 重载分时段uniqueId List
    * @Function:
    * @param null
    * @Return
    *
    * @author: zhy
    * @date: 2021-01-28 23:49
    */
    void reloadsySensorInfoFsdList();

    /**
    * 重载全时段uniqueId List
    * @Function:
    * @param null
    * @Return
    *
    * @author: zhy
    * @date: 2021-01-28 23:50
    */
    void reloadsySensorInfoList();

    /**
    * 批量更新全时段最新的uniqueId
    * @Function: updateSySensorInfoUniqueIds

    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-29 0:00
    */
    void updateSySensorInfoUniqueIds();

    /**
    * 批量更新分时段最新的uniqueId
    * @Function: updateSySensorInfoFsdUniqueIds

    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-29 0:01
    */

    void updateSySensorInfoFsdUniqueIds();



}
