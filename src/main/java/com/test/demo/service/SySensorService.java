package com.test.demo.service;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SyLogInfo;
import com.test.demo.entity.SySensorInfo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface SySensorService {

    String querySensorList(QueryParamVO queryParamVO);
    List<HashMap<String,Object>> getCgqList();

    String addSensorInfo(SySensorInfo sensorInfo);

    String deleteSensorInfo(int sensorId);

    String querySensorDetail(int sensorId);

    SySensorInfo querySensorDetailByXMLCode(String ssTransducerCode);
    /**
     * 沈阳新元，读取源文件夹所有文件，修改其中某文件，然后整体拷贝到另外一个文件夹中
     */
    void readfileSy(String filepath) throws FileNotFoundException, IOException;
    void readProperties()throws IOException;
    boolean queryLogInfoByName(String fileName);

    boolean insertIntoFileLog(SyLogInfo logInfo);

    String switchThread(String switchType);
}
