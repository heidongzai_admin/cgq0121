package com.test.demo.service;

import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.LogInfo;
import com.test.demo.entity.SensorInfo;
import org.apache.ibatis.annotations.Param;

public interface SensorService {

    String querySensorList(QueryParamVO queryParamVO);

    String addSensorInfo(SensorInfo sensorInfo);

    String deleteSensorInfo(int sensorId);

    String querySensorDetail(int sensorId);

    SensorInfo querySensorDetailByXMLCode(String ssTransducerCode);

    boolean queryLogInfoByName(String fileName);

    boolean insertIntoFileLog(LogInfo logInfo);

    String switchThread(String switchType,String mark);

}
