package com.test.demo.service.impl;

import com.test.demo.entity.SySensorInfo;
import com.test.demo.entity.SySensorInfoFsd;
import com.test.demo.entity.TConfig;
import com.test.demo.mapper.SySensorInfoFsdMapper;
import com.test.demo.mapper.SySensorInfoMapper;
import com.test.demo.mapper.TConfigMapper;
import com.test.demo.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author zhy
 * @Description
 * @ClassName CachServiceImpl
 * @Date 2021-1-24 16:34
 **/
@Service
public class CacheServiceImpl implements CacheService {


    @Autowired
    private SySensorInfoMapper sySensorInfoMapper;

    @Autowired
    private SySensorInfoFsdMapper sySensorInfoFsdMapper;

    @Autowired
    private TConfigMapper tConfigMapper;




    private Map<String,Object> cache = new ConcurrentHashMap<String, Object>();

    public  void setCache(String key, Object obj){
        cache.put(key,obj);
    }

    public  Object getCache(String key){

        return cache.get(key);
    }

    @Override
    public   void removeCache(String key){
        cache.remove(key);
    }
    @Override
    public TConfig getConfig(){
        if(null!=cache.get("config")){
            return (TConfig)cache.get("config");
        }else{
            reloadConfig();
            return (TConfig)cache.get("config");
        }
    }

    @Override
    public List<Long> getSySensorInfoFsd(){
        if (null!=cache.get("sySensorInfoFsdList") && ((List)cache.get("sySensorInfoFsdList")).size()>0) {
            return (List)cache.get("sySensorInfoFsdList");
        }else{
            reloadsySensorInfoFsdList();
            return (List)cache.get("sySensorInfoFsdList");
        }
    }

    @Override
    public List<Long> getSySensorInfo(){
        if (null!=cache.get("sySensorInfoList") && ((List)cache.get("sySensorInfoList")).size()>0) {
            return (List)cache.get("sySensorInfoList");
        }else{
            reloadsySensorInfoList();
            return (List)cache.get("sySensorInfoList");
        }
    }




    //=================================================全时段map配置缓存=============================
    @Override
    public Map<String , SySensorInfo> getSySensorInfoCode(){
        if (null != cache.get("sySensorInfoCode")) {
            return (Map<String, SySensorInfo>) cache.get("sySensorInfoCode");
        }else{
            reloadSySensorInfoCode();
            return (Map<String, SySensorInfo>) cache.get("sySensorInfoCode");
        }
    }
    @Override
    public void reloadSySensorInfoCode(){
        List<SySensorInfo> sySensorInfos = sySensorInfoMapper.selectAll();
        if (null!=sySensorInfos && sySensorInfos.size()>0) {
            Map<String,SySensorInfo> resultMap = new HashMap<>();
            for (SySensorInfo sySensorInfo : sySensorInfos) {
                resultMap.put(sySensorInfo.getCode(),sySensorInfo);
            }
            cache.put("sySensorInfoCode",resultMap);
        }
    }

    @Override
    public Map<String , SySensorInfo> getSySensorInfoName(){
        if (null != cache.get("sySensorInfoName")) {
            return (Map<String, SySensorInfo>) cache.get("sySensorInfoName");
        }else{
            reloadSySensorInfoName();
            return (Map<String, SySensorInfo>) cache.get("sySensorInfoName");
        }
    }
    @Override
    public void reloadSySensorInfoName(){
        List<SySensorInfo> sySensorInfos = sySensorInfoMapper.selectAll();
        if (null!=sySensorInfos && sySensorInfos.size()>0) {
            Map<String,SySensorInfo> resultMap = new HashMap<>();
            for (SySensorInfo sySensorInfo : sySensorInfos) {
                resultMap.put(sySensorInfo.getName(),sySensorInfo);
            }
            cache.put("sySensorInfoName",resultMap);
        }
    }

    @Override
    public Map<Long , SySensorInfo> getSySensorInfoUniqueId(){
        if (null != cache.get("sySensorInfoUniqueId")) {
            return (Map<Long, SySensorInfo>) cache.get("sySensorInfoUniqueId");
        }else{
            reloadSySensorInfoUniqueId();
            return (Map<Long, SySensorInfo>) cache.get("sySensorInfoUniqueId");
        }
    }
    @Override
    public void reloadSySensorInfoUniqueId(){
        List<SySensorInfo> sySensorInfos = sySensorInfoMapper.selectAll();
        if (null!=sySensorInfos && sySensorInfos.size()>0) {
            Map<Long,SySensorInfo> resultMap = new HashMap<>();
            for (SySensorInfo sySensorInfo : sySensorInfos) {
                resultMap.put(Long.parseLong(sySensorInfo.getUniqueId()),sySensorInfo);
            }
            cache.put("sySensorInfoUniqueId",resultMap);
        }
    }
    //=============================================================================================

    //==============================全时段map配置缓存===============================================
    @Override
    public Map<String , SySensorInfoFsd> getSySensorInfoFsdCode(){
        if (null != cache.get("sySensorInfoFsdCode")) {
            return (Map<String, SySensorInfoFsd>) cache.get("sySensorInfoFsdCode");
        }else{
            reloadSySensorInfoFsdCode();
            return (Map<String, SySensorInfoFsd>) cache.get("sySensorInfoFsdCode");
        }
    }
    @Override
    public void reloadSySensorInfoFsdCode(){
        List<SySensorInfoFsd> sySensorInfos = sySensorInfoFsdMapper.selectAll();
        if (null!=sySensorInfos && sySensorInfos.size()>0) {
            Map<String,SySensorInfoFsd> resultMap = new HashMap<>();
            for (SySensorInfoFsd sySensorInfo : sySensorInfos) {
                resultMap.put(sySensorInfo.getCode(),sySensorInfo);
            }
            cache.put("sySensorInfoFsdCode",resultMap);
        }
    }

    @Override
    public Map<String , SySensorInfoFsd> getSySensorInfoFsdName(){
        if (null != cache.get("sySensorInfoFsdName")) {
            return (Map<String, SySensorInfoFsd>) cache.get("sySensorInfoFsdName");
        }else{
            reloadSySensorInfoFsdName();
            return (Map<String, SySensorInfoFsd>) cache.get("sySensorInfoFsdName");
        }
    }
    @Override
    public void reloadSySensorInfoFsdName(){
        List<SySensorInfoFsd> sySensorInfos = sySensorInfoFsdMapper.selectAll();
        if (null!=sySensorInfos && sySensorInfos.size()>0) {
            Map<String,SySensorInfoFsd> resultMap = new HashMap<>();
            for (SySensorInfoFsd sySensorInfo : sySensorInfos) {
                resultMap.put(sySensorInfo.getName(),sySensorInfo);
            }
            cache.put("sySensorInfoFsdName",resultMap);
        }
    }

    @Override
    public Map<Long , SySensorInfoFsd> getSySensorInfoFsdUniqueId(){
        if (null != cache.get("sySensorInfoFsdUniqueId")) {
            return (Map<Long, SySensorInfoFsd>) cache.get("sySensorInfoFsdUniqueId");
        }else{
            reloadSySensorInfoFsdUniqueId();
            return (Map<Long, SySensorInfoFsd>) cache.get("sySensorInfoFsdUniqueId");
        }
    }
    @Override
    public void reloadSySensorInfoFsdUniqueId(){
        List<SySensorInfoFsd> sySensorInfos = sySensorInfoFsdMapper.selectAll();
        if (null!=sySensorInfos && sySensorInfos.size()>0) {
            Map<Long,SySensorInfoFsd> resultMap = new HashMap<>();
            for (SySensorInfoFsd sySensorInfo : sySensorInfos) {
                resultMap.put(Long.parseLong(sySensorInfo.getUniqueId()),sySensorInfo);
            }
            cache.put("sySensorInfoFsdUniqueId",resultMap);
        }
    }
    //========================================================================================




    @Override
    public void reloadConfig(){
        TConfig tConfig=tConfigMapper.queryDetail();
        if(null!=tConfig){
            this.setCache("config",tConfig);
        }
    }

    @Override
    public void reloadsySensorInfoFsdList(){
        List<Long> resultList = new ArrayList();
        for (String uniqueIdString : sySensorInfoFsdMapper.selectUniqueIdList()) {
            resultList.add(Long.parseLong(uniqueIdString));
        }

        if (null!=resultList && resultList.size()>0) {
            this.setCache("sySensorInfoFsdList",resultList);
        }
    }

    @Override
    public void reloadsySensorInfoList(){
        List<Long> resultList = new ArrayList();
        for (String uniqueIdString : sySensorInfoMapper.selectUniqueId()) {
            resultList.add(Long.parseLong(uniqueIdString));
        }

        if (null!=resultList && resultList.size()>0) {
            this.setCache("sySensorInfoList",resultList);
        }
    }

    @Override
    public void updateSySensorInfoUniqueIds() {
        List<HashMap<String, Object>> cgqList = sySensorInfoMapper.getCgqList();
        for (HashMap<String, Object> stringObjectHashMap : cgqList) {

            sySensorInfoMapper.updateUnqiueIdByCode((String) stringObjectHashMap.get("addrname"),(Long)stringObjectHashMap.get("UniqueID"));

        }
        //重载相关
        reloadsySensorInfoList();
        reloadSySensorInfoCode();
        reloadSySensorInfoUniqueId();
        reloadSySensorInfoName();
    }

    @Override
    public void updateSySensorInfoFsdUniqueIds() {
        List<HashMap<String, Object>> cgqList = sySensorInfoMapper.getCgqList();
        for (HashMap<String, Object> stringObjectHashMap : cgqList) {

            sySensorInfoFsdMapper.updateUnqiueIdByCode((String) stringObjectHashMap.get("addrname"),(Long)stringObjectHashMap.get("UniqueID"));

        }
        //重载相关
        reloadsySensorInfoFsdList();
        reloadSySensorInfoFsdCode();
        reloadSySensorInfoFsdUniqueId();
        reloadSySensorInfoFsdName();
    }


}
