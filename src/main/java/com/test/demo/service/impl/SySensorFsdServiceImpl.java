package com.test.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.test.demo.VO.AQSSDataVO;
import com.test.demo.VO.AQSSRootVO;
import com.test.demo.VO.QueryParamVO;
import com.test.demo.entity.SyLogInfo;
import com.test.demo.entity.SySensorInfoFsd;
import com.test.demo.mapper.SyLogInfoMapper;
import com.test.demo.mapper.SySensorInfoFsdMapper;
import com.test.demo.service.SySensorFsdService;
import com.test.demo.util.JaxbUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.ScheduledFuture;

import static java.math.BigDecimal.ROUND_HALF_UP;

//import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Service("sySensorFsdService")
public class SySensorFsdServiceImpl implements SySensorFsdService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SySensorFsdServiceImpl.class);

    @Autowired
    private SySensorInfoFsdMapper sySensorInfoFsdMapper;

    @Autowired
    private SyLogInfoMapper syLogInfoMapper;

    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    private ScheduledFuture<?> future;

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {

        return new ThreadPoolTaskScheduler();
    }

    private String readFilePath = null;

    private String writeParentFilePath = null;

    private String cron = null;

    @Override
    public String querySensorList(QueryParamVO queryParamVO) {
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            int pn = queryParamVO.getPn();
            int ps = queryParamVO.getPs();
            if(pn == 0 || ps == 0){
                pn = 1;
                ps = 10;
            }
            int cn = (pn - 1) * ps;
            queryParamVO.setCn(cn);

            List<SySensorInfoFsd> sensorInfoList = sySensorInfoFsdMapper.selectSensorList(queryParamVO);

            int count = sySensorInfoFsdMapper.getSensorCount(queryParamVO);
            if(count !=  0){
                map.put("code","0000");
                map.put("list",sensorInfoList);
                map.put("total",count);
                map.put("msg","查询成功");
            }else{
                map.put("code","0000");
                map.put("list",sensorInfoList);
                map.put("total",count);
                map.put("msg","暂无数据");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------查询批单列表----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("list",null);
            map.put("total",0);
            map.put("msg",e.getMessage());
        }
        return JSON.toJSONString(map);
    }

    @Override
    @Transactional
    public String addSensorInfo(SySensorInfoFsd sensorInfo) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer id = sensorInfo.getId();
//            String sensorXmlNo = sensorInfo.getSensorXmlNo();
            //根据sensorXmlNo查询当前传感器是否已录入

            if(id == null || "".equals(id)){
                try {
                    LOGGER.info("IDweikong");
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(),e);
                            //e.printStackTrace();
                }
                SySensorInfoFsd si = null;
//                sySensorInfoMapper.querySensorDetailByXMLCode(sensorXmlNo);
                QueryParamVO queryParamVO=new QueryParamVO();
                queryParamVO.setCode(sensorInfo.getCode());
                int pn = queryParamVO.getPn();
                int ps = queryParamVO.getPs();
                if(pn == 0 || ps == 0){
                    pn = 1;
                    ps = 10;
                }
                int cn = (pn - 1) * ps;
                queryParamVO.setCn(cn);
                queryParamVO.setPs(ps);
                queryParamVO.setPn(pn);

                List<SySensorInfoFsd> sensorInfoList = sySensorInfoFsdMapper.selectSensorList(queryParamVO);
                if(sensorInfoList == null||sensorInfoList.size()==0){
                    sensorInfo.setIsValid(1);
                    sensorInfo.setCreateTime(new Date());
                    sySensorInfoFsdMapper.insertUseGeneratedKeys(sensorInfo);
                    map.put("msg","新增成功");
                    map.put("code","0000");
                    map.put("data",null);
                }else{
                    map.put("code","9999");
                    map.put("data",null);
                    map.put("msg","当前传感器已存在，请重新录入！");
                }
            }else{

                QueryParamVO queryParamVO=new QueryParamVO();
                queryParamVO.setCode(sensorInfo.getCode());
                int pn = queryParamVO.getPn();
                int ps = queryParamVO.getPs();
                if(pn == 0 || ps == 0){
                    pn = 1;
                    ps = 10;
                }
                int cn = (pn - 1) * ps;
                queryParamVO.setCn(cn);
                queryParamVO.setPs(ps);
                queryParamVO.setPn(pn);

                List<SySensorInfoFsd> sensorInfoList = sySensorInfoFsdMapper.selectSensorList(queryParamVO);

                if(sensorInfoList == null||sensorInfoList.size()==0){
                    LOGGER.info("修改"+new ObjectMapper().writeValueAsString(sensorInfo));
                    sensorInfo.setIsValid(1);
                    sensorInfo.setUpdateTime(new Date());
                    sySensorInfoFsdMapper.updateByPrimaryKey(sensorInfo);
                    map.put("msg","修改成功");
                    map.put("code","0000");
                    map.put("data",null);
                }else{
                    String existsMark="0";
                    for(SySensorInfoFsd ss:sensorInfoList){
                        if(!ss.getId().equals(sensorInfo.getId())){
                            existsMark="1";
                        }
                    }
                    if("1".equals(existsMark)) {
                        map.put("code", "9999");
                        map.put("data", null);
                        map.put("msg", "当前传感器已存在，请重新录入！");
                    }else{
                        LOGGER.info("修改"+new ObjectMapper().writeValueAsString(sensorInfo));
                        sensorInfo.setIsValid(1);
                        sensorInfo.setUpdateTime(new Date());
                        sySensorInfoFsdMapper.updateByPrimaryKey(sensorInfo);
                        map.put("msg","修改成功");
                        map.put("code","0000");
                        map.put("data",null);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------新增修改传感器----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    @Transactional
    public String deleteSensorInfo(int sensorId) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            SySensorInfoFsd sensorInfo = new SySensorInfoFsd();
            sensorInfo.setId(sensorId);
            sensorInfo.setIsValid(0);
            sensorInfo.setUpdateTime(new Date());
            int size = sySensorInfoFsdMapper.deleteByPrimaryKey(sensorInfo);
            if(size >0){
                map.put("code","0000");
                map.put("data",null);
                map.put("msg","删除成功");
            }else{
                map.put("code","9999");
                map.put("data",null);
                map.put("msg","删除失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------新增修改传感器----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    public String querySensorDetail(int sensorId) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            SySensorInfoFsd sensorInfo = new SySensorInfoFsd();
            sensorInfo.setId(sensorId);
            sensorInfo.setIsValid(1);
            sensorInfo = sySensorInfoFsdMapper.selectByPrimaryKey(sensorInfo);

            if(sensorInfo != null ){
                map.put("code","0000");
                map.put("data",sensorInfo);
                map.put("msg","查询成功");
            }else{
                map.put("code","0000");
                map.put("data",null);
                map.put("msg","查询数据为空");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------新增修改传感器----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    public SySensorInfoFsd querySensorDetailByXMLCode(String ssTransducerCode) {

        return null;
//        sySensorInfoMapper.querySensorDetailByXMLCode(ssTransducerCode);
    }

    @Override
    public boolean queryLogInfoByName(String fileName) {
        boolean flag = false;
        SyLogInfo logInfo = null;
//        syLogInfoMapper.queryLogInfoByName(fileName);
        if(logInfo != null){
            flag = true;
        }
        return flag;
    }

    @Override
    @Transactional
    public boolean insertIntoFileLog(SyLogInfo logInfo) {
        int size = syLogInfoMapper.insertUseGeneratedKeys(logInfo);
        if(size > 0){
            return true;
        }
        return false;
    }
    @Override
    public List<HashMap<String,Object>> getCgqList(){
        List<HashMap<String,Object>> rel;
        rel=sySensorInfoFsdMapper.getCgqList();
        return rel;
    }
    @Override

    public String switchThread(String switchType) {
        /**/
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            if(StringUtils.isEmpty(switchType)){
                map.put("code","9999");
                map.put("msg","线程转换类型不能为空");
            }
            // switchType 01-开启线程  00-关闭线程
            boolean flag = taskCycle(switchType);
            if(flag){
                map.put("code","0000");
                map.put("msg","设置成功");
            }else{
                map.put("code","9999");
                map.put("msg","设置失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","9999");
            map.put("msg",e.getMessage());
        }
        return JSON.toJSONString(map);
//        return null;
    }

    @Override
    public void readProperties() throws IOException {
        Properties properties = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = SySensorFsdService.class.getClassLoader().getResourceAsStream("sys-params.properties");
        // 使用properties对象加载输入流
        properties.load(in);
        //获取key对应的value值
        readFilePath = properties.getProperty("readFilePathSy");
        writeParentFilePath = properties.getProperty("writeParentFilePathSy");
        cron = properties.getProperty("cronSy");
    }
    public boolean taskCycle(String switchType) {
        boolean flag = false;
        try {
            readProperties();
            if(switchType.equals("01")){
                future = null;
                threadPoolTaskScheduler.schedule(new FileDealThread(), new CronTrigger(cron));
                if(future != null){
                    flag = true;
                }
            }else if(switchType.equals("00")){
                if (future != null && !future.isDone()) {
                    future.cancel(true);
                    flag = true;
                }else if(future.isDone() || future.isCancelled()){
                    flag = true;
                }
            }
        }catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
                            //e.printStackTrace();
        }
        return flag;
    }

    public class FileDealThread implements Runnable{

        @Override
        public void run() {
            try{
                LOGGER.info("---thread FileDealThread begin ---");
                String filePath = readFilePath;
                readfile(filePath);
                LOGGER.info("---thread FileDealThread end ---");
            }catch (Exception e){
                e.printStackTrace();
                LOGGER.info("---thread FileDealThread end exception---");
                LOGGER.error(e.getMessage());
            }
        }
    }

    /**
     * 读取某个文件夹下的所有文件
     */
    public void readfile(String filepath) throws FileNotFoundException, IOException {
        try {
            File file = new File(filepath);
            // 判断是文件夹还是文件
            if (!file.isDirectory()) {
                LOGGER.info("文件");
                LOGGER.info("path=" + file.getPath());
                LOGGER.info("absolutepath=" + file.getAbsolutePath());
                LOGGER.info("name=" + file.getName());
                if(file !=  null){
                    boolean flag =  updateFileInfo(file);
                    // 删除原目录中的文件
                    if(file.exists() && flag){
                        System.gc();	//加上确保文件能删除，不然可能删不掉
                        boolean deleteflag = file.delete();
                        if(deleteflag){
                            LOGGER.info("-------------文件名为==>"+file.getName()+ "文件已删除----------------");
                        }
                    }
                }
            } else if (file.isDirectory()) {
                LOGGER.info("----------------文件夹------------");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        if(readfile != null){
                            boolean flag = updateFileInfoSy(readfile);
                            // 删除原目录中的文件
                            if(readfile.exists() && flag){
                                System.gc();	//加上确保文件能删除，不然可能删不掉
                                boolean deleteflag = readfile.delete();
                                if(deleteflag){
                                    LOGGER.info("-------------文件名为==>"+readfile.getName()+ "文件已删除----------------");
                                }
                            }
                        }
                    } else if (readfile.isDirectory()) {
                        readfile(filepath + "\\" + filelist[i]);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>FileNotFoundException:" + e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>文件处理异常:" + e.getMessage());
        }
    }

    /**
     * 沈阳新元，读取源文件夹所有文件，修改其中某文件，然后整体拷贝到另外一个文件夹中
     */
    @Override
    public void readfileSy(String filepath) throws FileNotFoundException, IOException {
        try {
            File file = new File(filepath);
            // 判断是文件夹还是文件
            if (!file.isDirectory()) {
                LOGGER.info("文件");
                LOGGER.info("path=" + file.getPath());
                LOGGER.info("absolutepath=" + file.getAbsolutePath());
                LOGGER.info("name=" + file.getName());
                if(file !=  null){
                    boolean flag =  updateFileInfoSy(file);
                    // 删除原目录中的文件
                    if(file.exists() && flag){
                        System.gc();	//加上确保文件能删除，不然可能删不掉
//                        boolean deleteflag = file.delete();
//                        if(deleteflag){
//                            LOGGER.info("-------------文件名为==>"+file.getName()+ "文件已删除----------------");
//                        }
                    }
                }
            } else if (file.isDirectory()) {
                LOGGER.info("----------------文件夹------------");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        if(readfile != null){
                            boolean flag = updateFileInfoSy(readfile);
                            // 删除原目录中的文件
                            if(readfile.exists() && flag){
                                System.gc();	//加上确保文件能删除，不然可能删不掉
//                                boolean deleteflag = readfile.delete();
//                                if(deleteflag){
//                                    LOGGER.info("-------------文件名为==>"+readfile.getName()+ "文件已删除----------------");
//                                }
                            }
                        }
                    } else if (readfile.isDirectory()) {
                        readfileSy(filepath + "\\" + filelist[i]);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>FileNotFoundException:" + e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>文件处理异常:" + e.getMessage());
        }
    }

    public static void main(String[] args) {
//        String str="通洲晋杨,1,501,1,1号总回风甲烷,0.03,1,2020-10-30 16:46:52";
//        String time=str.substring(str.lastIndexOf(",")+1);//采样时间
//        str=str.substring(0,str.lastIndexOf(","));
//        String status=str.substring(str.lastIndexOf(",")+1);//状态
//        str=str.substring(0,str.lastIndexOf(","));
//        String value=str.substring(str.lastIndexOf(",")+1);//监测值
//        str=str.substring(0,str.lastIndexOf(","));
//        String name=str.substring(str.lastIndexOf(",")+1);//名称
//        str=str.substring(0,str.lastIndexOf(","));
//        String type=str.substring(str.lastIndexOf(",")+1);//类型
//        str=str.substring(0,str.lastIndexOf(","));
//        String code=str.substring(str.lastIndexOf(",")+1);//传感器编号
//        str=str.substring(0,str.lastIndexOf(","));
//        String fenzhan=str.substring(str.lastIndexOf(",")+1);//分站
//        str=str.substring(0,str.lastIndexOf(","));
//        String kuangming=str.substring(str.lastIndexOf(",")+1);//监测值
//        System.out.println(kuangming+"|"+fenzhan+"|"+code+"|"+type+"|"+name+"|"+value+"|"+status+"|"+time+"|");


//        LOGGER.info("-------------文件名称----------------:"+ fileName);
//try {
//    StringBuffer sbuf = new StringBuffer();
////                isr = new InputStreamReader(new FileInputStream(readfile), "UTF-8");
//    InputStreamReader isr = new InputStreamReader(new FileInputStream("E://work//shanpeng//传感器//模拟量显示"),"GBK");
//    BufferedReader br = null;
//    br = new BufferedReader(isr);
//    String curStr;
//    StringBuffer newFile = new StringBuffer();
//    // 通过readLine()方法按行读取字符串
//    while ((curStr = br.readLine()) != null) {
////                 String str="通洲晋杨,1,501,1,1号总回风甲烷,0.03,1,2020-10-30 16:46:52";
//        String str = curStr;
//        String time = str.substring(str.lastIndexOf(",") + 1);//采样时间
//        str = str.substring(0, str.lastIndexOf(","));
//        String status = str.substring(str.lastIndexOf(",") + 1);//状态
//        str = str.substring(0, str.lastIndexOf(","));
//        String value = str.substring(str.lastIndexOf(",") + 1);//监测值
//        str = str.substring(0, str.lastIndexOf(","));
//        String name = str.substring(str.lastIndexOf(",") + 1);//名称
//        str = str.substring(0, str.lastIndexOf(","));
//        String type = str.substring(str.lastIndexOf(",") + 1);//类型
//        str = str.substring(0, str.lastIndexOf(","));
//        String code = str.substring(str.lastIndexOf(",") + 1);//传感器编号
//        str = str.substring(0, str.lastIndexOf(","));
//        String fenzhan = str.substring(str.lastIndexOf(",") + 1);//分站
//        str = str.substring(0, str.lastIndexOf(","));
//        String kuangming = str.substring(str.lastIndexOf(",") + 1);//监测值
//                    System.out.println(kuangming+"|"+fenzhan+"|"+code+"|"+type+"|"+name+"|"+value+"|"+status+"|"+time+"|");
//        BigDecimal valueBD = new BigDecimal(value);
//            if("3".equals(status)){
////                SySensorInfo si = sySensorInfoMapper.querySySensorDetail(fenzhan,
////                        kuangming,
////                        code,
////                        type,
////                        name);
////                if(null!=si){
////                    BigDecimal max=si.getMaxAlarm();
////                    BigDecimal min=si.getMinAlarm();
//                    BigDecimal valueBDNew= new BigDecimal("888");
//                    newFile.append(kuangming+","+fenzhan+","+code+","+type+","+name+","+valueBDNew+","+status+","+time);
////                }else{
////                    newFile.append(curStr);
////                }
//            }else
//            {
//                newFile.append(curStr);
//            }
//        newFile.append("\r\n");
//    }
////    writeToFile(newFile.toString(), "模拟量显示");
//}catch (Exception e){
//    e.printStackTrace();
//}
//        String fileName="模拟量显示";
//        File readfile=new File("E://work//shanpeng//传感器//"+fileName);
//
//        Path copied = Paths.get("E://work//shanpeng//传感器//aa//"+fileName);
//        Path originalPath =readfile.toPath();
//        try {
//            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        BigDecimal maxValue=new BigDecimal("0.02");
        BigDecimal minValue=new BigDecimal("0");
        BigDecimal factValue=null;
        BigDecimal tmpValue=maxValue.subtract(minValue);
//        BigDecimal diffValue = tmpValue;
        BigDecimal tmpSubValue=tmpValue.divide(new BigDecimal("2"),5,BigDecimal.ROUND_HALF_UP);
//        BigDecimal diffValue=().setScale(5,ROUND_HALF_UP);
        factValue = (maxValue.subtract(tmpSubValue)).setScale(5,ROUND_HALF_UP);
        System.out.println();

    }
    // 沈阳新元对读取到的文件进行处理
    @Transactional
    public boolean updateFileInfoSy(File readfile){
        InputStreamReader isr = null;
        try {
            String fileName =  readfile.getName();
            // 根绝文件名称查询当前文件是否进行修改
//            boolean fileFlag = queryLogInfoByName(fileName);
            boolean fileFlag=false;
            isr = new InputStreamReader(new FileInputStream(readfile), "GBK");
            if("模拟量显示".equals(fileName)){
                fileFlag=true;
            }
            if(fileFlag){

                LOGGER.info("-------------文件名称----------------:"+ fileName);

                StringBuffer sbuf = new StringBuffer();

//                isr = new InputStreamReader(new FileInputStream("E://work//shanpeng//传感器//模拟量显示"),"GBK");
                BufferedReader br = null;
                br = new BufferedReader(isr);
                String curStr;
                StringBuffer newFile=new StringBuffer();
                // 通过readLine()方法按行读取字符串
                while ((curStr = br.readLine()) != null) {
//                 String str="通洲晋杨,1,501,1,1号总回风甲烷,0.03,1,2020-10-30 16:46:52";
                    String str=curStr;
                    String time=str.substring(str.lastIndexOf(",")+1);//采样时间
                    str=str.substring(0,str.lastIndexOf(","));
                    String status=str.substring(str.lastIndexOf(",")+1);//状态
                    str=str.substring(0,str.lastIndexOf(","));
                    String value=str.substring(str.lastIndexOf(",")+1);//监测值
                    str=str.substring(0,str.lastIndexOf(","));
                    String name=str.substring(str.lastIndexOf(",")+1);//名称
                    str=str.substring(0,str.lastIndexOf(","));
                    String type=str.substring(str.lastIndexOf(",")+1);//类型
                    str=str.substring(0,str.lastIndexOf(","));
                    String code=str.substring(str.lastIndexOf(",")+1);//传感器编号
                    str=str.substring(0,str.lastIndexOf(","));
                    String fenzhan=str.substring(str.lastIndexOf(",")+1);//分站
                    str=str.substring(0,str.lastIndexOf(","));
                    String kuangming=str.substring(str.lastIndexOf(",")+1);//监测值
//                    System.out.println(kuangming+"|"+fenzhan+"|"+code+"|"+type+"|"+name+"|"+value+"|"+status+"|"+time+"|");
                    BigDecimal valueBD=new BigDecimal(value);
                    if("3".equals(status)){
                        SySensorInfoFsd si = sySensorInfoFsdMapper.querySySensorDetail(fenzhan,
                                kuangming,
                                code,
                                type,
                                name);
                        if(null!=si){
                            BigDecimal max=si.getMaxAlarm();
                            BigDecimal min=si.getMinAlarm();
                            BigDecimal valueBDNew= dealAnalogValueSy(min,max,valueBD);
                            Double doubleValue = valueBDNew.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
//                            //保留两位小数且四舍五入
//                            usedM = usedM.setScale(2, BigDecimal.ROUND_HALF_UP);
                            newFile.append(kuangming+","+fenzhan+","+code+","+type+","+name+","+doubleValue+",1,"+time);
                        }else{

//                            //保留两位小数且四舍五入
//                            usedM = usedM.setScale(2, BigDecimal.ROUND_HALF_UP);
                            newFile.append(kuangming+","+fenzhan+","+code+","+type+","+name+","+valueBD+",1,"+time);
//                            newFile.append(curStr);
                        }
                    }else
                    {
                        newFile.append(curStr);
                    }
                    newFile.append("\r\n");
                }
                writeToFile(newFile.toString(),fileName);


                        SyLogInfo logInfo = new SyLogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
//                        boolean flag1 = insertIntoFileLog(logInfo);
//                        if(!flag1){
//                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
//                        }

//                        LOGGER.info("-------------不做处理直接删除----------------{}:"+ fileName);
//                        SyLogInfo logInfo = new SyLogInfo();
//                        logInfo.setFileName(fileName);
//                        logInfo.setStatus("01");
//                        logInfo.setCreateTime(new Date());
//                        boolean flag = insertIntoFileLog(logInfo);
//                        if(!flag){
//                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
//                        }
//                    }
                }else{
                    // 将文件名称中不含 “AQSS”，“AQBJ”的文件直接输出当固定文件夹中
//                    writeToFile(sbuf.toString(),fileName);
//                Files src=""
                Path copied = Paths.get(writeParentFilePath+fileName);
                Path originalPath =readfile.toPath();
                Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
//                    SyLogInfo logInfo = new SyLogInfo();
//                    logInfo.setFileName(fileName);
//                    logInfo.setStatus("01");
//                    logInfo.setCreateTime(new Date());
//                    boolean flag = insertIntoFileLog(logInfo);
//                    if(!flag){
//                        LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
//                    }
                }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
                  }finally {
            try {
                if(isr != null){
                    isr.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return false;
    }


    // 对读取到的文件进行处理
    @Transactional
    public boolean updateFileInfo(File readfile){
        InputStreamReader isr = null;
        try {
            String fileName =  readfile.getName();
            // 根绝文件名称查询当前文件是否进行修改
            boolean fileFlag = queryLogInfoByName(fileName);
            if(!fileFlag){

                LOGGER.info("-------------文件名称----------------:"+ fileName);

                StringBuffer sbuf = new StringBuffer();
                isr = new InputStreamReader(new FileInputStream(readfile), "UTF-8");

                Long filelength = readfile.length();

                char[] ch=new char[filelength.intValue()];

                int len=isr.read(ch);

                String line = new String(ch,0,len);

                sbuf.append(line);

                LOGGER.info("-------------读取到的XML数据----------------:"+ sbuf.toString());

                if(fileName.indexOf("AQSS") > 1 || fileName.indexOf("AQBJ") > 1){

                    if(fileName.indexOf("AQSS") > 1){
                        AQSSRootVO aqssRootVO = (AQSSRootVO) JaxbUtil.xmlStrToOject(AQSSRootVO.class,sbuf.toString());
                        LOGGER.info("-------------XML --> AQSSRootVO实体----------------:"+ JSON.toJSONString(aqssRootVO));

                        List<AQSSDataVO> aqssDataVOList = aqssRootVO.getDataVOList();

                        for(int i = 0; i < aqssDataVOList.size(); i++){
                            AQSSDataVO aqssDataVO = aqssDataVOList.get(i);
                            String  ssTransducerCode = aqssDataVO.getSs_transducer_code();
                            BigDecimal ssAnalogValue = aqssDataVO.getSs_analog_value();
                            // 修改报警值
                            BigDecimal diffValue = dealAnalogValue(ssTransducerCode,ssAnalogValue);
                            if(diffValue.compareTo(new BigDecimal(-1)) != 0) {
                                aqssDataVO.setSs_analog_value(diffValue);
                                aqssDataVO.setSs_transducer_state("0");
                            }
                        }
                        // 将Java对象转成XML写出到相应的文件夹中
                        String xmlData = JaxbUtil.beanToXml(aqssRootVO,AQSSRootVO.class);
                        writeToFile(xmlData,fileName);

                        SyLogInfo logInfo = new SyLogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
                        boolean flag1 = insertIntoFileLog(logInfo);
                        if(!flag1){
                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                        }
                    }else if(fileName.indexOf("AQBJ") > 1){
//                        AQBJRootVO aqbjRootVO = (AQBJRootVO)JaxbUtil.xmlStrToOject(AQBJRootVO.class,sbuf.toString());
//                        LOGGER.info("-------------XML --> AQBJRootVO实体----------------:"+ JSON.toJSONString(aqbjRootVO));
//
//                        AQBJDataVO aqbjDataVO = aqbjRootVO.getData();
//
//                        String  ssTransducerCode = aqbjDataVO.getSs_transducer_code();
//                        BigDecimal ssAnalogValue = aqbjDataVO.getSs_analog_value();
//
//                        // 修改报警值
//                        BigDecimal diffValue = dealAnalogValue(ssTransducerCode,ssAnalogValue);
//
//                        if(diffValue.compareTo(new BigDecimal(-1)) != 0) {
//                            aqbjDataVO.setSs_analog_value(diffValue);
//                        }
//                        // 将Java对象转成XML写出到相应的文件夹中
//                        String xmlData = JaxbUtil.beanToXml(aqbjRootVO,AQBJRootVO.class);
//                        LOGGER.info("-------------需要写入文件的数据----------------{}:"+ JSON.toJSONString(xmlData));
//                        writeToFile(xmlData,fileName);
                        LOGGER.info("-------------不做处理直接删除----------------{}:"+ fileName);
                        SyLogInfo logInfo = new SyLogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
                        boolean flag = insertIntoFileLog(logInfo);
                        if(!flag){
                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                        }
                    }
                }else{
                    // 将文件名称中不含 “AQSS”，“AQBJ”的文件直接输出当固定文件夹中
                    writeToFile(sbuf.toString(),fileName);
                    SyLogInfo logInfo = new SyLogInfo();
                    logInfo.setFileName(fileName);
                    logInfo.setStatus("01");
                    logInfo.setCreateTime(new Date());
                    boolean flag = insertIntoFileLog(logInfo);
                    if(!flag){
                        LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                    }
                }
            }
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>FileNotFoundException:" + e.getMessage());
        } catch (IOException e){
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>IOException:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>Exception:" + e.getMessage());
        }finally {
            try {
                if(isr != null){
                    isr.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return false;
    }
    /**
     * 查询当前编号对应的报警值是否大于报警值的最大值
     *
     */
    public BigDecimal dealAnalogValueSy(BigDecimal minValue,BigDecimal maxValue, BigDecimal ssAnalogValue){
        BigDecimal factValue = new BigDecimal(-1);
        // 根据XML编码查询报警值
//        SySensorInfo sensorInfo = querySensorDetailByXMLCode(ssTransducerCode);
//        if(sensorInfo != null){
//            BigDecimal minValue = sensorInfo.getMinAlarm();
//            BigDecimal maxValue = sensorInfo.getMaxAlarm();
            if(ssAnalogValue != null && minValue!= null && maxValue != null){
                if(minValue.compareTo(maxValue) == 0){

                    factValue = minValue;

                }else if(ssAnalogValue.compareTo(minValue) ==  -1){

                    BigDecimal tmpValue=maxValue.subtract(minValue);
                    BigDecimal tmpSubValue=tmpValue.divide(new BigDecimal("2"),5,BigDecimal.ROUND_HALF_UP);
                    factValue = (maxValue.subtract(tmpSubValue)).setScale(5,ROUND_HALF_UP);

                }else if(ssAnalogValue.compareTo(maxValue) ==  1){


                    BigDecimal tmpValue=maxValue.subtract(minValue);
                    BigDecimal tmpSubValue=tmpValue.divide(new BigDecimal("2"),5,BigDecimal.ROUND_HALF_UP);
                    factValue = (maxValue.subtract(tmpSubValue)).setScale(5,ROUND_HALF_UP);

                }else if(ssAnalogValue.compareTo(maxValue) == 0){

                    BigDecimal tmpValue=maxValue.subtract(minValue);
                    BigDecimal tmpSubValue=tmpValue.divide(new BigDecimal("2"),5,BigDecimal.ROUND_HALF_UP);
                    factValue = (maxValue.subtract(tmpSubValue)).setScale(5,ROUND_HALF_UP);

                }else if(ssAnalogValue.compareTo(minValue) == 0){

                    BigDecimal tmpValue=maxValue.subtract(minValue);
                    BigDecimal tmpSubValue=tmpValue.divide(new BigDecimal("2"),5,BigDecimal.ROUND_HALF_UP);
                    factValue = (maxValue.subtract(tmpSubValue)).setScale(5,ROUND_HALF_UP);

                }
            }
//        }
        return factValue;
    }


    /**
     * 查询当前编号对应的报警值是否大于报警值的最大值
     *
     */
    public BigDecimal dealAnalogValue(String  ssTransducerCode, BigDecimal ssAnalogValue){
        BigDecimal factValue = new BigDecimal(-1);
        // 根据XML编码查询报警值
        SySensorInfoFsd sensorInfo = querySensorDetailByXMLCode(ssTransducerCode);
        if(sensorInfo != null){
            BigDecimal minValue = sensorInfo.getMinAlarm();
            BigDecimal maxValue = sensorInfo.getMaxAlarm();
            if(ssAnalogValue != null && minValue!= null && maxValue != null){
                if(minValue.compareTo(maxValue) == 0){

                    factValue = minValue;

                }else if(ssAnalogValue.compareTo(minValue) ==  -1){

                    BigDecimal diffValue = minValue.subtract(ssAnalogValue);
                    factValue = ssAnalogValue.add(diffValue).add(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);
                    if(factValue.compareTo(maxValue) == 1){
                        factValue = factValue.subtract(factValue.subtract(maxValue)).setScale(5,ROUND_HALF_UP);
                    }

                }else if(ssAnalogValue.compareTo(maxValue) ==  1){

                    BigDecimal diffValue = ssAnalogValue.subtract(maxValue);
                    factValue = ssAnalogValue.subtract(diffValue).subtract(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);
                    if(factValue.compareTo(minValue) == -1){
                        factValue = factValue.add(minValue.subtract(factValue)).setScale(5,ROUND_HALF_UP);
                    }

                }else if(ssAnalogValue.compareTo(maxValue) == 0){

                    factValue = ssAnalogValue.subtract(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);
                    if(factValue.compareTo(maxValue) == 1){
                        factValue = factValue.subtract(factValue.subtract(maxValue)).setScale(5,ROUND_HALF_UP);
                    }

                }else if(ssAnalogValue.compareTo(minValue) == 0){

                    factValue = ssAnalogValue.add(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);
                    if(factValue.compareTo(minValue) == -1){
                        factValue = factValue.add(minValue.subtract(factValue)).setScale(5,ROUND_HALF_UP);
                    }

                }
            }
        }
        return factValue;
    }

    public  void writeToFile(String data,String fileName){
//        byte[] sourceByte = data.getBytes();
        String path = writeParentFilePath;
//        String path = "E://work//shanpeng//传感器//aa//";
        if(null != data && !"".equals(data)){
            OutputStreamWriter osw = null;
            FileOutputStream fos = null;
            try {
                File file = new File(path+fileName);//文件路径（路径+文件名）
                if (!file.exists()) {   //文件不存在则创建文件，先创建目录
                    File dir = new File(file.getParent());
                    dir.mkdirs();
                    file.createNewFile();
                }
                fos = new FileOutputStream(file); //文件输出流将数据写入文件
                osw = new OutputStreamWriter(fos,"UTF-8");

                osw.write(data);
                osw.flush();

            } catch (Exception e) {
                LOGGER.info("------------- 文件生成异常 Exception:" + e.getMessage());
                e.printStackTrace();
            }finally {
                try {
                    fos.close();
                    osw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
