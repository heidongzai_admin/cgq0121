package com.test.demo.service.impl;

import com.test.demo.entity.SySensorInfo;
import com.test.demo.entity.SySensorInfoFsd;
import com.test.demo.entity.TConfig;
import com.test.demo.mapper.AStatusRecordMapper;
import com.test.demo.mapper.StaOneMinuteMapper;
import com.test.demo.mapper.SySensorInfoFsdMapper;
import com.test.demo.mapper.SySensorInfoMapper;
import com.test.demo.service.AStatusRecordService;
import com.test.demo.service.CacheService;
import com.test.demo.util.DateUtil;
import com.test.demo.util.ProjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author zhy
 * @Description
 * @ClassName AStatusRecordServiceImpl
 * @Date 2021-1-23 13:53
 **/
@Service
@Slf4j
public class AStatusRecordServiceImpl implements AStatusRecordService {

    @Autowired
    private StaOneMinuteMapper staOneMinuteMapper;

    @Autowired
    private SySensorInfoMapper sySensorInfoMapper;

    @Autowired
    private SySensorInfoFsdMapper sySensorInfoFsdMapper;

    @Autowired
    private AStatusRecordMapper aStatusRecordMapper;

    @Autowired
    private CacheService cacheService;



    @Override
    public void operationStaOneMinute(List<Long> uniqueList, String statID, Date date, int timeStatus, TConfig tconfig) {
        if (uniqueList!=null&&uniqueList.size()>0) {
            //todo 格式化日期
            DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            String format = dateFormat.format(date);
            String tableName="AStatusRecord";
            String tmpTableName=tableName+"_TMP"+format;
            staOneMinuteMapper.createTable(tableName,format);

            //查询符合条件
            List<Integer> statIds = new ArrayList<>();
            for (String statIdString : statID.split(",")) {
                statIds.add(Integer.parseInt(statIdString));
            }
            //分时段开始时和结束时间
            Date beginTime=null;
            Date endTime=null;
            //分时段所处理的uniqueIds
            if (timeStatus!=1) {
                //格式化处理分段开始和结束时间
                Map<String, Date> resultMap =  DateUtil.dateFormatBeignEndTime(tconfig);
                beginTime=resultMap.get("beginTime");
                endTime=resultMap.get("endTime");
            }
            do{
                //按条件查找
                //todo 修改mapper
                List<Map> staOneMinutes = aStatusRecordMapper.selectByuniquesAndStatIdLimit(uniqueList, statIds, format ,1000,beginTime,endTime,tmpTableName);
                if (staOneMinutes==null || staOneMinutes.size()==0) {
                    break;
                }
                for (Map staOneMinute: staOneMinutes) {
                    try {
                        operationTable(staOneMinute,timeStatus,tconfig,tmpTableName,format);
                    } catch (Exception e) {
                        log.error(e.getMessage(),e);
                            //e.printStackTrace();
                    }finally {
                        try {
                            staOneMinuteMapper.insertTmpTbale((Long) staOneMinute.get("ID"),tmpTableName);
                        } catch (Exception e) {
                            log.error(e.getMessage(),e);
                            //e.printStackTrace();
                        }
                    }
                }
            }while (true);
            //如果是分时段和全时段进行分时段处理后进行全时段处理
            if (timeStatus==3) {
                uniqueList= cacheService.getSySensorInfo();
                statID=tconfig.getStatusIds();
                timeStatus=1;
                operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
            }
        }
    }

    /**
     * 修改数据库
     * @param staOneMinute
     * @param timeStatus
     * @param tconfig
     * @param tableName
     * @param tmpTableName
     */
    @Transactional(rollbackFor = Exception.class)
    public void operationTable(Map staOneMinute, Integer timeStatus, TConfig tconfig, String tmpTableName, String format) {
        Long uniqueId = (Long) staOneMinute.get("UniqueID");
        BigDecimal maxAlarm = null;
        BigDecimal minAlarm = null;
        switch (timeStatus) {
            case 1:
                SySensorInfo sySensorInfo = sySensorInfoMapper.selectByUniqueId(uniqueId);
                maxAlarm = sySensorInfo.getMaxAlarm();
                minAlarm = sySensorInfo.getMinAlarm();
                break;
            default:
                //先进行分时段处理
                SySensorInfoFsd sySensorInfoFsd = sySensorInfoFsdMapper.selectByUniqueId(uniqueId);
                maxAlarm = sySensorInfoFsd.getMaxAlarm();
                minAlarm = sySensorInfoFsd.getMinAlarm();
                break;
        }
        //报警平均值
        BigDecimal avgAlarm = (maxAlarm.add(minAlarm)).divide(new BigDecimal(2));
        //计算随机数
        Double maxValue = null;
        Double minValue = null;
        Double avgValue = null;


        if (!ProjectUtil.judgmentIsRange(maxAlarm, minAlarm,
                (BigDecimal) staOneMinute.get("MaxValue") == null ? new BigDecimal(0) : (BigDecimal) staOneMinute.get("MaxValue"),
                (BigDecimal) staOneMinute.get("MinValue") == null ? new BigDecimal(0) : (BigDecimal) staOneMinute.get("MinValue")
        )) {
            //判断MaxValue是否在范围内
            if (!ProjectUtil.judgmentIsRange(maxAlarm, avgAlarm, (BigDecimal) staOneMinute.get("MaxValue") == null ? new BigDecimal(0) : (BigDecimal) staOneMinute.get("MaxValue"))) {
                maxValue = ProjectUtil.getRandomNumber(maxAlarm, avgAlarm);
            }
            //判断MinValue是否在范围内
            if (!ProjectUtil.judgmentIsRange(avgAlarm, minAlarm, (BigDecimal) staOneMinute.get("MinValue") == null ? new BigDecimal(0) : (BigDecimal) staOneMinute.get("MinValue"))) {
                minValue = ProjectUtil.getRandomNumber(avgAlarm, minAlarm);
            }
            //判断avgValue是否在范围内
            if (maxValue == null) {
                maxValue = ((BigDecimal) staOneMinute.get("MaxValue")).doubleValue();
            }
            if (minValue == null) {
                minValue = ((BigDecimal) staOneMinute.get("MinValue")).doubleValue();
            }
            avgValue = (((new BigDecimal(maxValue)).add(new BigDecimal(minValue))).divide(new BigDecimal(2))).doubleValue();
        }

            //修改statId
            int formStatId = Integer.parseInt(tconfig.getTargetStatus());
            //todo 修改mapper
            aStatusRecordMapper.updateByUnique(maxValue, minValue, avgValue, formStatId, (Long) staOneMinute.get("ID"), format);
    }



}
