package com.test.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.test.demo.entity.TConfig;
//import com.test.demo.mapper.SySensorInfoFsdMapper;
import com.test.demo.mapper.SySensorInfoMapper;
import com.test.demo.mapper.TConfigMapper;
import com.test.demo.service.TConfigService;
import org.apache.ibatis.jdbc.Null;
import org.apache.taglibs.standard.lang.jstl.NullLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

//import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Service("tConfigService")
public class TConfigServiceImpl implements TConfigService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TConfigServiceImpl.class);

    @Autowired
    private TConfigMapper tConfigMapper;




//    @Autowired
//    private SySensorInfoFsdMapper sySensorInfoFsdMapper;
    @Autowired
    private SySensorInfoMapper sySensorInfoMapper;

    @Override
    public String addTConfig(TConfig sensorInfo) {
        TConfig configOrigin=sensorInfo;
        if ("".equals(sensorInfo.getFsdBeginTime())) {
            sensorInfo.setFsdBeginTime(null);
        }
        if ("".equals(sensorInfo.getFsdEndTime())) {
            sensorInfo.setFsdEndTime(null);
        }
        tConfigMapper.updateByPrimaryKeySelective(sensorInfo);
//        if(null!=sensorInfo&&"1".equals(sensorInfo.getFsdSwitch())||"0".equals(sensorInfo.getFsdSwitch())){
//            sySensorInfoFsdMapper.updateStatus(sensorInfo.getFsdSwitch());
//        }
//        if("1".equals(configOrigin.getQsdSwitch())&&"1".equals(configOrigin.getFsdSwitch())){
//            sySensorInfoMapper.updateInnerStatus("0");
//            sySensorInfoMapper.updateOuterStatus("1");
//        }else if("1".equals(configOrigin.getQsdSwitch())&&"0".equals(configOrigin.getFsdSwitch())){
//            sySensorInfoMapper.updateInnerStatus("1");
//            sySensorInfoMapper.updateOuterStatus("1");
//        }else if("0".equals(configOrigin.getQsdSwitch())&&"1".equals(configOrigin.getFsdSwitch())){
//            sySensorInfoMapper.updateInnerStatus("0");
//            sySensorInfoMapper.updateOuterStatus("0");
//        }else if("0".equals(configOrigin.getQsdSwitch())&&"0".equals(configOrigin.getFsdSwitch())){
//            sySensorInfoMapper.updateInnerStatus("0");
//            sySensorInfoMapper.updateOuterStatus("0");
//        }
//        sySensorFsdService.getCgqList()


        return null;
    }


    @Override
    public String queryDetail(String sensorId) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            TConfig TConfig = new TConfig();
            TConfig.setId(sensorId);

            TConfig = tConfigMapper.selectByPrimaryKey(TConfig);

            if(TConfig != null ){
                map.put("code","0000");
                map.put("data",TConfig);
                map.put("msg","查询成功");
            }else{
                map.put("code","0000");
                map.put("data",null);
                map.put("msg","查询数据为空");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------查询配置信息----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    public TConfig queryDetailObj(String sensorId) {
        Map<String,Object> map = new HashMap<String,Object>();
        TConfig TConfig = new TConfig();
        try{
//            TConfig TConfig = new TConfig();
            TConfig.setId(sensorId);

            TConfig = tConfigMapper.selectByPrimaryKey(TConfig);

//            if(TConfig != null ){
//                map.put("code","0000");
//                map.put("data",TConfig);
//                map.put("msg","查询成功");
//            }else{
//                map.put("code","0000");
//                map.put("data",null);
//                map.put("msg","查询数据为空");
//            }
        }catch (Exception e){
//            e.printStackTrace();
//            LOGGER.info("--------------查询配置信息----异常信息：", e.getMessage());
//            map.put("code","9999");
//            map.put("data",null);
//            map.put("msg",e.getMessage());
        }

        return TConfig;
    }



}
