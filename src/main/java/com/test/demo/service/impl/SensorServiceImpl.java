package com.test.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.sun.javafx.scene.control.skin.BehaviorSkinBase;
import com.test.demo.VO.*;
import com.test.demo.dbUtil.JDBCSqlServerUtils;
import com.test.demo.entity.*;
import com.test.demo.license.VerifyLicenseUtil;
import com.test.demo.mapper.*;
import com.test.demo.service.*;
import com.test.demo.task.FileDealTask;
import com.test.demo.util.DateUtil;
import com.test.demo.util.JaxbUtil;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import javax.persistence.criteria.From;
import javax.xml.crypto.Data;
import java.io.*;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

import static java.math.BigDecimal.ROUND_HALF_UP;

@Service("sensorService")
@Slf4j
public class SensorServiceImpl implements SensorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorServiceImpl.class);
    @Autowired
    private SySensorService sySensorService;

    @Autowired
    private SensorInfoMapper sensorInfoMapper;

    @Autowired
    private SySensorInfoMapper sySensorInfoMapper;

    @Autowired
    private SySensorInfoFsdMapper sySensorInfoFsdMapper;

    @Resource
    private StaOneMinuteService staOneMinuteService;

    @Resource
    private AnalogRunRecordService analogRunRecordService;

    @Resource
    private AStatusRecordService aStatusRecordService;

    @Resource
    private RunRecordService runRecordService;

    @Resource
    private StaOneDayService staOneDayService;

    @Resource
    private StaOneHourService staOneHourService;
    @Autowired
    private TConfigService tConfigService;

    @Autowired
    private LogInfoMapper logInfoMapper;

    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    private ScheduledFuture<?> future;
    private ScheduledFuture<?> futureSy;

    private ScheduledFuture<?> futureDb;
    private ScheduledFuture<?> futureMc;

    @Autowired
    private CacheService cacheService;


    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    private String readFilePath = null;

    private String writeParentFilePath = null;

    private String cron = null;
    private String readFilePathSy = null;

    private String writeParentFilePathSy = null;

    private String cronSy = null;
    /**
     * 数据库修改定时任务时间
     */
    private String cronDb=null;

    /**
     * 密采定时任务时间
     */
    private String cronMc=null;
    @Override
    public String querySensorList(QueryParamVO queryParamVO) {
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            int pn = queryParamVO.getPn();
            int ps = queryParamVO.getPs();
            if(pn == 0 || ps == 0){
                pn = 1;
                ps = 10;
            }
            int cn = (pn - 1) * ps;
            queryParamVO.setCn(cn);

            List<SensorInfo> sensorInfoList = sensorInfoMapper.selectSensorList(queryParamVO);

            int count = sensorInfoMapper.getSensorCount(queryParamVO);
            if(count !=  0){
                map.put("code","0000");
                map.put("list",sensorInfoList);
                map.put("total",count);
                map.put("msg","查询成功");
            }else{
                map.put("code","0000");
                map.put("list",sensorInfoList);
                map.put("total",count);
                map.put("msg","暂无数据");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------查询批单列表----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("list",null);
            map.put("total",0);
            map.put("msg",e.getMessage());
        }
        return JSON.toJSONString(map);
    }

    @Override
    @Transactional
    public String addSensorInfo(SensorInfo sensorInfo) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            Integer id = sensorInfo.getId();
            String sensorXmlNo = sensorInfo.getSensorXmlNo();
            //根据sensorXmlNo查询当前传感器是否已录入

            if(id == null || "".equals(id)){
                SensorInfo si = sensorInfoMapper.querySensorDetailByXMLCode(sensorXmlNo);
                if(si == null){
                    sensorInfo.setIsValid(1);
                    sensorInfo.setCreateTime(new Date());
                    sensorInfoMapper.insertUseGeneratedKeys(sensorInfo);
                    map.put("msg","新增成功");
                    map.put("code","0000");
                    map.put("data",null);
                }else{
                    map.put("code","9999");
                    map.put("data",null);
                    map.put("msg","当前传感器已存在，请重新录入！");
                }
            }else{
                LOGGER.info("修改"+new ObjectMapper().writeValueAsString(sensorInfo));
                sensorInfo.setIsValid(1);
                sensorInfo.setUpdateTime(new Date());
                sensorInfoMapper.updateByPrimaryKey(sensorInfo);
                map.put("msg","修改成功");
                map.put("code","0000");
                map.put("data",null);
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------新增修改传感器----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    @Transactional
    public String deleteSensorInfo(int sensorId) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            SensorInfo sensorInfo = new SensorInfo();
            sensorInfo.setId(sensorId);
            sensorInfo.setIsValid(0);
            sensorInfo.setUpdateTime(new Date());
            int size = sensorInfoMapper.deleteByPrimaryKey(sensorInfo);
            if(size >0){
                map.put("code","0000");
                map.put("data",null);
                map.put("msg","删除成功");
            }else{
                map.put("code","9999");
                map.put("data",null);
                map.put("msg","删除失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------新增修改传感器----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    public String querySensorDetail(int sensorId) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            SensorInfo sensorInfo = new SensorInfo();
            sensorInfo.setId(sensorId);
            sensorInfo.setIsValid(1);
            sensorInfo = sensorInfoMapper.selectByPrimaryKey(sensorInfo);

            if(sensorInfo != null ){
                map.put("code","0000");
                map.put("data",sensorInfo);
                map.put("msg","查询成功");
            }else{
                map.put("code","0000");
                map.put("data",null);
                map.put("msg","查询数据为空");
            }
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.info("--------------新增修改传感器----异常信息：", e.getMessage());
            map.put("code","9999");
            map.put("data",null);
            map.put("msg",e.getMessage());
        }

        return JSON.toJSONString(map);
    }

    @Override
    public SensorInfo querySensorDetailByXMLCode(String ssTransducerCode) {

        return sensorInfoMapper.querySensorDetailByXMLCode(ssTransducerCode);
    }

    @Override
    public boolean queryLogInfoByName(String fileName) {
        boolean flag = false;
        LogInfo logInfo = logInfoMapper.queryLogInfoByName(fileName);
        if(logInfo != null){
            flag = true;
        }
        return flag;
    }

    @Override
    @Transactional
    public boolean insertIntoFileLog(LogInfo logInfo) {
        int size = logInfoMapper.insertUseGeneratedKeys(logInfo);
        if(size > 0){
            return true;
        }
        return false;
    }

    @Override
    public String switchThread(String switchType,String mark) {
        Map<String,Object> map = new HashMap<String,Object>();
        try{
            if(StringUtils.isEmpty(switchType)){
                map.put("code","9999");
                map.put("msg","线程转换类型不能为空");
            }
            // switchType 01-开启线程  00-关闭线程
            boolean flag = taskCycle(switchType,mark);
            if(flag){
                map.put("code","0000");
                map.put("msg","设置成功");
            }else{
                map.put("code","9999");
                map.put("msg","设置失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","9999");
            map.put("msg",e.getMessage());
        }
        return JSON.toJSONString(map);
    }

    public void readProperties() throws IOException {
        Properties properties = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = SensorService.class.getClassLoader().getResourceAsStream("sys-params.properties");
        // 使用properties对象加载输入流
        properties.load(in);
        //获取key对应的value值
        readFilePath = properties.getProperty("readFilePath");
        writeParentFilePath = properties.getProperty("writeParentFilePath");
        cron = properties.getProperty("cron");
        readFilePathSy = properties.getProperty("readFilePathSy");
        writeParentFilePathSy = properties.getProperty("writeParentFilePathSy");
        cronSy = properties.getProperty("cronSy");
        cronDb = properties.getProperty("cronDb");
        cronMc = properties.getProperty("cronMc");
    }


    public boolean taskCycle(String switchType,String mark) {
        boolean flag = false;
        if(4!=threadPoolTaskScheduler.getPoolSize()) {
            threadPoolTaskScheduler.setPoolSize(4);
        }
        try {
                readProperties();
            if(switchType.equals("01")){
                if("1".equals(mark)){
                    if(null==futureSy||futureSy.isCancelled()||futureSy.isCancelled()) {
                        futureSy = threadPoolTaskScheduler.schedule(new FileDealThreadSy(), new CronTrigger(cronSy));
                    }if (futureSy != null) {
                        flag = true;
                    }
                }else if("0".equals(mark)){
                    if(null==future||future.isCancelled()||future.isCancelled()) {
                        future = threadPoolTaskScheduler.schedule(new FileDealThread(), new CronTrigger(cron));
                    }
                    if (future != null) {
                        flag = true;
                    }
                }else if("2".equals(mark)){
                    if(null==futureDb||futureDb.isCancelled()||futureDb.isCancelled()) {
                        futureDb = threadPoolTaskScheduler.schedule(new FileDealThreadDb(), new CronTrigger(cronDb));
                    }
                    if (futureDb != null) {
                        flag = true;
                    }
                }else if("3".equals(mark)){
                    if(null==futureMc||futureMc.isCancelled()||futureMc.isCancelled()) {
                        futureMc = threadPoolTaskScheduler.schedule(new FileDealThreadMc(), new CronTrigger(cronMc));
                    }
                    if (futureMc != null) {
                        flag = true;
                    }
                }
            }else if(switchType.equals("00")){
                if("1".equals(mark)){
                    if (futureSy != null && !futureSy.isDone()) {
                        boolean cancel = futureSy.cancel(true);
                        LOGGER.info("是否已经取消"+cancel);
                        flag = true;
                    }else if(futureSy.isDone() || futureSy.isCancelled()){
                        flag = true;
                    }
                }else if("0".equals(mark)){
                    if (future != null && !future.isDone()) {
                        future.cancel(true);
                        flag = true;
                    }else if(future.isDone() || future.isCancelled()){
                        flag = true;
                    }
                }else if("2".equals(mark)){
                    if (futureDb != null && !futureDb.isDone()) {
                        futureDb.cancel(true);
                        flag = true;
                    }else if(futureDb.isDone() || future.isCancelled()){
                        flag = true;
                    }
                }else if("3".equals(mark)){
                    if (futureMc != null && !future.isDone()) {
                        futureMc.cancel(true);
                        flag = true;
                    }else if(futureMc.isDone() || futureMc.isCancelled()){
                        flag = true;
                    }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public class FileDealThread implements Runnable{

        @Override
        public void run() {

            try{
                TConfig tconfig=cacheService.getConfig();
                if(null!=tconfig&& StringUtil.isNotEmpty(tconfig.getId())) {
                    if ("1".equals(tconfig.getQsdSwitch())) {
                        Date cur = new Date();
                        int i = cur.getHours();
                        int m = cur.getMinutes();
                        if (13 == i && m == 13) {
                            boolean rel = VerifyLicenseUtil.doVerifyDate();
                            if (!rel) {
                                taskCycle("00", null);
                            }
                        }
                        LOGGER.info("---thread FileDealThread begin ---");
                        String filePath = readFilePath;
                        readfile(filePath);
                        LOGGER.info("---thread FileDealThread end ---");
                    } else {
                        LOGGER.info("---DB14开关已关闭 ---");
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                LOGGER.info("---thread FileDealThread end exception---");
                LOGGER.error(e.getMessage());
            }
        }
    }
    public class FileDealThreadSy implements Runnable{

        @Override
        public void run() {
            //首先判断本协议开关开启没有；
            //判断全时段还是分时段生效，分时段优先级高，拿到生效的传感器列表,本次增加的unique字段，所有jsp dto都要加进去
            //密采和DBy哦那个的是uniqueId，心愿3和db14用的是code

            //拿到要修改的传感器状态
            //db就是6张表挨个检索计算修改，注意每次只处理100条
            //密采，jdbc删除目标记录即可（根据传感器列表in条件，状态列表intia偶见直接delete，注意关闭连接，这是第三方库)
            try{
                TConfig tconfig = cacheService.getConfig();
                if(null!=tconfig&& StringUtil.isNotEmpty(tconfig.getId())) {
                    if ("1".equals(tconfig.getFsdSwitch())) {
                        Date cur = new Date();
                        int i = cur.getHours();
                        int m = cur.getMinutes();

                        if (13 == i && 13 == m) {
                            boolean rel = VerifyLicenseUtil.doVerifyDate();
                            if (!rel) {
                                taskCycle("00", "1");
                            }
                        }

                        LOGGER.info("---thread FileDealThreadSy begin ---");
                        String filePath = readFilePathSy;
                        sySensorService.readProperties();
                        sySensorService.readfileSy(filePath);

                        LOGGER.info("---thread FileDealThreadSy end ---");
                    } else {
                        LOGGER.info("---新元开关已关闭 ---");
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                LOGGER.info("---thread FileDealThreadSy end exception---");
                LOGGER.error(e.getMessage());
            }
        }
    }
    /**  修改数据库线程类
    *
    * @Function:
    * @param
    * @Return
    *
    * @author: zhy
    * @date: 2021-01-20 15:23
    */
    public class FileDealThreadDb implements Runnable{

        @Override
        public void run() {
            //更新缓存
            cacheService.updateSySensorInfoFsdUniqueIds();
            cacheService.updateSySensorInfoUniqueIds();
            TConfig tconfig = cacheService.getConfig();
            if(null!=tconfig&& StringUtil.isNotEmpty(tconfig.getId())) {
                if ("1".equals(tconfig.getDbSwitch())) {
                    //判断是否在分时段
                    Date date = new Date();
                    List<Long> uniqueList = new ArrayList<>();
                    String statID=null;
                    //全/分时段开关是否开启
                    int timeStatus = judgmentTime(tconfig);
                    //查询uniqueid
                    switch (timeStatus){
                        case 0:
                            LOGGER.info("======数据库修改，全时段分时都均未开启");
                            return;
                        case 1:
                            uniqueList=cacheService.getSySensorInfo();
                            statID=tconfig.getStatusIds();
                            LOGGER.info("======数据库修改,只有全时段开启");
                            break;
                        case 2:
                            uniqueList = cacheService.getSySensorInfoFsd();
                            statID=tconfig.getParttimeStatusIds();
                            LOGGER.info("====数据库修改，只有分时段开启");
                            break;
                        case 3:
                            uniqueList = cacheService.getSySensorInfoFsd();
                            statID=tconfig.getParttimeStatusIds();
                            LOGGER.info("====数据库修改，分时段全时段均开启");
                            break;

                    }
                    //执行查询并修改StaOneMinute表
                    try {
                        staOneMinuteService.operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(),e);
                            //e.printStackTrace();
                    }
                    //执行查询并修改StaOneHour表
                    try {
                        staOneHourService.operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(),e);
                            //e.printStackTrace();
                    }
                    //执行查询并修改StaOneDay表
                    try {
                        staOneDayService.operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(),e);
                            //e.printStackTrace();
                    }
                    //执行查询并修改RunRecord表
                    try {
                        runRecordService.operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(),e);
                        e.printStackTrace();
                    }
                    //执行查询并修改AStatusRecord表
                    try {
                        aStatusRecordService.operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(),e);
                        e.printStackTrace();
                    }
                    //执行查询并修改AnalogRunRecord表
                    try {
                        analogRunRecordService.operationStaOneMinute(uniqueList,statID,date,timeStatus,tconfig);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(),e);
                        e.printStackTrace();
                    }
                }else{
                    LOGGER.info("---数据库修改开关已关闭 ---");
                }
            }
        }
    }






    /**
    * 判断当前全/分时段状态
    * @Function: judgmentTime
* @param date
    * @Return int 0：都不开启 1:全时段，2：分时段，3：全时段和分时段都共存
    *
    * @author: zhy
    * @date: 2021-01-21 16:49
    */
    private int judgmentTime(TConfig tconfig) {
        if ("1".equals(tconfig.getParttimeSwitch())) {
            //分时段开启
            if ("1".equals(tconfig.getAlltimeSwitch())) {
                //分时段全时段都开启
                return 3;
            }else{
                //只有分时段
                return 2;
            }
        }else if ("1".equals(tconfig.getAlltimeSwitch())) {
            //分段式关闭，全时段开启
            return 1;
        }else{
                //都关闭
                return 0;
        }
    }



    /** 
    * 修改密采线程类
    * @Function:  
    * @param null 
    * @Return
    *
    * @author: zhy
    * @date: 2021-01-20 15:30
    */
    public class FileDealThreadMc implements Runnable{
        @Override
        public void run() {
            try{
                TConfig tconfig = cacheService.getConfig();
                cacheService.updateSySensorInfoFsdUniqueIds();
                cacheService.updateSySensorInfoUniqueIds();
                if(null!=tconfig&& StringUtil.isNotEmpty(tconfig.getId())) {
                    //全时段开启并且密采开启
                    if ("1".equals(tconfig.getMcSwitch())&& "1".equals(tconfig.getAlltimeSwitch())) {
                        LOGGER.info("密采开始执行");
                        LOGGER.info("连接数据库");
                        Connection connection = JDBCSqlServerUtils.getConnection();
                        if (connection==null) {
                            LOGGER.error("第三方数据库连接失败");
                            return ;
                        }
                        //查询全时段的uniqueId
                        List<String> uniqueIdList = sySensorInfoMapper.selectUniqueIdWithString();
                        String uniqueIds = String.join(",", uniqueIdList);
                        Date date = new Date();
                        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                        String format = dateFormat.format(date);
                        String sql ="delete from "
                                +JDBCSqlServerUtils.getForm()+format+
                                " where UniqueId in ("+uniqueIds+")and StatusID not in ("+tconfig.getStatusIds()+")";
                        Statement statement = connection.createStatement();
                        int i = statement.executeUpdate(sql);
                        //关闭连接
                        JDBCSqlServerUtils.close(null,statement,connection);
                        LOGGER.info("---thread FileDealThread end ---");
                    } else {
                        LOGGER.info("---密采开关已关闭 ---");
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                LOGGER.info("---thread FileDealThread end exception---");
                LOGGER.error(e.getMessage());
            }
        }
    }




    /**
     * 读取某个文件夹下的所有文件
     */
    public void readfile(String filepath) throws FileNotFoundException, IOException {
        try {
            File file = new File(filepath);
            // 判断是文件夹还是文件
            if (!file.isDirectory()) {
                LOGGER.info("文件");
                LOGGER.info("path=" + file.getPath());
                LOGGER.info("absolutepath=" + file.getAbsolutePath());
                LOGGER.info("name=" + file.getName());
                if(file !=  null){
                    boolean flag =  updateFileInfo(file);
                    // 删除原目录中的文件
                    if(file.exists() && flag){
                        System.gc();	//加上确保文件能删除，不然可能删不掉
                        boolean deleteflag = file.delete();
                        if(deleteflag){
                            LOGGER.info("-------------文件名为==>"+file.getName()+ "文件已删除----------------");
                        }
                    }
                }
            } else if (file.isDirectory()) {
                LOGGER.info("----------------文件夹------------");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        if(readfile != null){
                            boolean flag = updateFileInfo(readfile);
                            // 删除原目录中的文件
                            if(readfile.exists() && flag){
                                System.gc();	//加上确保文件能删除，不然可能删不掉
                                boolean deleteflag = readfile.delete();
                                if(deleteflag){
                                    LOGGER.info("-------------文件名为==>"+readfile.getName()+ "文件已删除----------------");
                                }
                            }
                        }
                    } else if (readfile.isDirectory()) {
                        readfile(filepath + "\\" + filelist[i]);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>FileNotFoundException:" + e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>文件处理异常:" + e.getMessage());
        }
    }

    // 对读取到的文件进行处理
    @Transactional(rollbackFor = Exception.class)
    public boolean updateFileInfo(File readfile){
        InputStreamReader isr = null;
        try {
            String fileName =  readfile.getName();
            // 根绝文件名称查询当前文件是否进行修改
            boolean fileFlag = queryLogInfoByName(fileName);
            if(!fileFlag){

                LOGGER.info("-------------文件名称----------------:"+ fileName);

                StringBuffer sbuf = new StringBuffer();
                isr = new InputStreamReader(new FileInputStream(readfile), "UTF-8");

                Long filelength = readfile.length();

                char[] ch=new char[filelength.intValue()];

                int len=isr.read(ch);

                String line = new String(ch,0,len);

                sbuf.append(line);

//                LOGGER.info("-------------读取到的XML数据----------------:"+ sbuf.toString());

                if(fileName.indexOf("AQSS") > 1 || fileName.indexOf("AQBJ") > 1){

                    if(fileName.indexOf("AQSS") > 1){
                        String sbufStr=sbuf.toString().substring(1);
//                        LOGGER.info("-------------加工后的XML数据----------------:"+ sbufStr);
                        AQSSRootVO aqssRootVO = (AQSSRootVO) JaxbUtil.xmlStrToOject(AQSSRootVO.class,sbufStr);
//                        LOGGER.info("-------------XML --> AQSSRootVO实体----------------:"+ JSON.toJSONString(aqssRootVO));

                        List<AQSSDataVO> aqssDataVOList = aqssRootVO.getDataVOList();
                        String timeString=aqssRootVO.getHead().getCs_data_time();
                        for(int i = 0; i < aqssDataVOList.size(); i++){
                            AQSSDataVO aqssDataVO = aqssDataVOList.get(i);
                            String  ssTransducerCode = aqssDataVO.getSs_transducer_code();
                            BigDecimal ssAnalogValue = aqssDataVO.getSs_analog_value();
                            // 修改报警值
                            BigDecimal diffValue = dealAnalogValue(ssTransducerCode,ssAnalogValue,timeString);
                            if(diffValue.compareTo(new BigDecimal(-1)) != 0) {
                                aqssDataVO.setSs_analog_value(diffValue);
                                aqssDataVO.setSs_transducer_state("0");
                            }
                        }
                        // 将Java对象转成XML写出到相应的文件夹中
                        String xmlData = JaxbUtil.beanToXml(aqssRootVO,AQSSRootVO.class);
                        writeToFile(xmlData,fileName);

                        LogInfo logInfo = new LogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
                        boolean flag1 = insertIntoFileLog(logInfo);
                        if(!flag1){
                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                        }
                    }else if(fileName.indexOf("AQBJ") > 1){
                        String tmp=sbuf.toString();
                        if(StringUtil.isNotEmpty(tmp)&&tmp.length()>0){
                            String targetTmp=tmp.substring(1);
                            if(StringUtil.isNotEmpty(targetTmp)&&targetTmp.length()>0&&targetTmp.indexOf("<")==0){
                                tmp=targetTmp;
                            }
                        }
                        AQBJRootVO aqbjRootVO = (AQBJRootVO)JaxbUtil.xmlStrToOject(AQBJRootVO.class,tmp);
                        LOGGER.info("-------------XML --> AQBJRootVO实体----------------:"+ JSON.toJSONString(aqbjRootVO));

                        AQBJDataVO aqbjDataVO = aqbjRootVO.getData();

                        String  ssTransducerCode = aqbjDataVO.getSs_transducer_code();
                        BigDecimal ssAnalogValue = aqbjDataVO.getSs_analog_value();
                        String ss_transducer_state=aqbjDataVO.getSs_transducer_state();
                        String copyMark="1";
                        if("1".equals(ss_transducer_state)&& StringUtil.isNotEmpty(ssTransducerCode)){
                            TConfig tconfig = cacheService.getConfig();
                            SySensorInfo sySensorInfo=null ;
                            if ("1".equals(tconfig.getAlltimeSwitch())) {
                                Map<String, SySensorInfo> sySensorInfoCode = cacheService.getSySensorInfoCode();
                                sySensorInfo=sySensorInfoCode.get(ssTransducerCode);
                                //sySensorInfo=sySensorInfoMapper.querySySensorDetailByCode(ssTransducerCode);
                            }
                            if(null!=sySensorInfo){
                                copyMark="0";
                            }
                        }
                        if("1".equals(copyMark)){
                            String xmlData = JaxbUtil.beanToXml(aqbjRootVO,AQBJRootVO.class);
                            writeToFile(xmlData,fileName);
                            LOGGER.info("-------------拷贝文件----------------{}:"+ fileName);
                        }else{
                            LOGGER.info("-------------补考呗----------------{}:"+ fileName);
                        }
//
//                        // 修改报警值
//                        BigDecimal diffValue = dealAnalogValue(ssTransducerCode,ssAnalogValue);
//
//                        if(diffValue.compareTo(new BigDecimal(-1)) != 0) {
//                            aqbjDataVO.setSs_analog_value(diffValue);
//                        }
//                        // 将Java对象转成XML写出到相应的文件夹中
//                        String xmlData = JaxbUtil.beanToXml(aqbjRootVO,AQBJRootVO.class);
//                        LOGGER.info("-------------需要写入文件的数据----------------{}:"+ JSON.toJSONString(xmlData));
//                        writeToFile(xmlData,fileName);
                        LOGGER.info("-------------删除----------------{}:"+ fileName);
                        LogInfo logInfo = new LogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
                        boolean flag = insertIntoFileLog(logInfo);
                        if(!flag){
                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                        }
                    }
                }else{
                    // 将文件名称中不含 “AQSS”，“AQBJ”的文件直接输出当固定文件夹中
                    writeToFile(sbuf.toString(),fileName);
                    LogInfo logInfo = new LogInfo();
                    logInfo.setFileName(fileName);
                    logInfo.setStatus("01");
                    logInfo.setCreateTime(new Date());
                    boolean flag = insertIntoFileLog(logInfo);
                    if(!flag){
                        LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                    }
                }
            }
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>FileNotFoundException:" + e.getMessage());
        } catch (IOException e){
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>IOException:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>Exception:" + e.getMessage());
        }finally {
            try {
                if(isr != null){
                    isr.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 查询当前编号对应的报警值是否大于报警值的最大值
     *
     */
    public BigDecimal dealAnalogValue(String ssTransducerCode, BigDecimal ssAnalogValue, String timeString){
        BigDecimal factValue = new BigDecimal(-1);
        // 根据XML编码查询报警值
        TConfig tconfig = cacheService.getConfig();
        BigDecimal minValue=null;
        BigDecimal maxValue=null;
        int timeStatus = judgmentTime(tconfig);
        switch (timeStatus){
            case 1:
                Map<String, SySensorInfo> sySensorInfoCode = cacheService.getSySensorInfoCode();
                SySensorInfo sySensorInfo = sySensorInfoCode.get(ssTransducerCode);
                //SySensorInfo sySensorInfo = sySensorInfoMapper.querySySensorDetailByCode(ssTransducerCode);
                if (sySensorInfo!=null) {
                    minValue=sySensorInfo.getMinAlarm();
                    maxValue=sySensorInfo.getMaxAlarm();
                }
                break;
            case 2:
                if (DateUtil.isEffectiveDate(timeString.split(" ")[1],tconfig.getFsdBeginTime(),tconfig.getFsdEndTime())) {
                    Map<String, SySensorInfoFsd> sySensorInfoFsdCode = cacheService.getSySensorInfoFsdCode();
                    SySensorInfoFsd sySensorInfoFsd = sySensorInfoFsdCode.get(ssTransducerCode);
                    if (sySensorInfoFsd!=null) {
                        minValue=sySensorInfoFsd.getMinAlarm();
                        maxValue=sySensorInfoFsd.getMaxAlarm();
                    }
                }
                break;
            case 3:
                if (DateUtil.isEffectiveDate(timeString.split(" ")[1],tconfig.getFsdBeginTime(),tconfig.getFsdEndTime())) {
                    Map<String, SySensorInfoFsd> sySensorInfoFsdCode = cacheService.getSySensorInfoFsdCode();
                    SySensorInfoFsd sySensorInfoFsd = sySensorInfoFsdCode.get(ssTransducerCode);
                    if (sySensorInfoFsd!=null) {
                        minValue=sySensorInfoFsd.getMinAlarm();
                        maxValue=sySensorInfoFsd.getMaxAlarm();
                        break;
                    }
                }
                Map<String, SySensorInfo> newSySensorInfoCode = cacheService.getSySensorInfoCode();
                SySensorInfo newSySensorInfo = newSySensorInfoCode.get(ssTransducerCode);
                if (newSySensorInfo!=null) {
                    minValue=newSySensorInfo.getMinAlarm();
                    maxValue=newSySensorInfo.getMaxAlarm();
                }

                break;
        }

            if(ssAnalogValue != null && minValue!= null && maxValue != null){
                if(minValue.compareTo(maxValue) == 0){

                    factValue = maxValue;

                }else if(ssAnalogValue.compareTo(minValue) ==  -1){

//                    BigDecimal tmpValue=maxValue.subtract(minValue);
//                    BigDecimal tmpSubValue=tmpValue.divide(new BigDecimal("2"),5,BigDecimal.ROUND_HALF_UP);
//                    factValue = (maxValue.subtract(tmpSubValue)).setScale(5,ROUND_HALF_UP);

                    float minF = minValue.floatValue();
                    float maxF = maxValue.floatValue();
                    factValue = new BigDecimal(Math.random() * (maxF - minF) + minF).setScale(2,ROUND_HALF_UP);;

                }else if(ssAnalogValue.compareTo(maxValue) ==  1){
                    float minF = minValue.floatValue();
                    float maxF = maxValue.floatValue();
                    factValue = new BigDecimal(Math.random() * (maxF - minF) + minF).setScale(2,ROUND_HALF_UP);;;

                }else if(ssAnalogValue.compareTo(minValue) == 0){
                    factValue = minValue;
                }
            }

        return factValue;
    }

    public void writeToFile(String data,String fileName){
//        byte[] sourceByte = data.getBytes();
        String path = writeParentFilePath;
        if(null != data && !"".equals(data)){
            OutputStreamWriter osw = null;
            FileOutputStream fos = null;
            try {
                File file = new File(path+File.separator+fileName);//文件路径（路径+文件名）
                if (!file.exists()) {   //文件不存在则创建文件，先创建目录
                    File dir = new File(file.getParent());
                    dir.mkdirs();
                    file.createNewFile();
                }
                fos = new FileOutputStream(file); //文件输出流将数据写入文件
                osw = new OutputStreamWriter(fos,"UTF-8");

                osw.write(data);
                osw.flush();

            } catch (Exception e) {
                LOGGER.info("------------- 文件生成异常 Exception:" + e.getMessage());
                e.printStackTrace();
            }finally {
                try {
                    fos.close();
                    osw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



}
