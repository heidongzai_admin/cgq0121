package com.test.demo.com.test.demo;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    public ReadFile() {
    }


    /**
     * 读取某个文件夹下的所有文件
     * test
     */
    public static boolean readfile(String filepath) throws FileNotFoundException, IOException {
        try {
            File file = new File(filepath);

            if (!file.isDirectory()) {
                System.out.println("文件");
                System.out.println("path=" + file.getPath());
                System.out.println("absolutepath=" + file.getAbsolutePath());
                System.out.println("name=" + file.getName());

            } else if (file.isDirectory()) {
                System.out.println("文件夹");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        String fileName =  readfile.getName();
                        List<String> listStr = new ArrayList<String>();
                        String str = null;
                        if(fileName.indexOf("AQSS") > 1 || fileName.indexOf("AQMC") > 1 || fileName.indexOf(" AQBJ") > 1){
                            BufferedReader br = new BufferedReader(new FileReader(readfile));
                            while ((str = br.readLine())!= null) {
                                System.out.println("-------------文件输出内容------>>>>"+str);
                                listStr.add(str);
                            }
                        }

                    } else if (readfile.isDirectory()) {
                        readfile(filepath + "\\" + filelist[i]);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("readfile()   Exception:" + e.getMessage());
        }
        return true;
    }
    public static void main(String[] args) {
        try {
            readfile("E:/Work/MyDoc/MX/KJ83_data");
            // deletefile("D:/file");
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        }
        System.out.println("ok");
    }

}
