package com.test.demo.properties;

import lombok.Data;

import java.util.List;

@Data
public class Application {
    public String appName;
    public List<String> authc;
    public List<String> ignore;
}
