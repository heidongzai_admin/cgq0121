package com.test.demo.properties;
import lombok.Data;
//import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;

@Data
@Component
//@ConfigurationProperties(prefix = "thinkit")
public class ThinkItProperties {

    public ThinkItProperties.path path = new path();

    private String gatewayApi;

    private Boolean keepOnOfError = true;

    private Boolean startSolr = false;

    private String license;

    public String getRootPath(){
        return this.path.getRootPath();
    }

    public String getSitePath(){
        return getRootPath()+File.separator+this.path.getSite().getPathName();
    }

    public String getSourcePath(){
        return getRootPath()+File.separator+this.path.getSource().getPathName();
    }

    public String getTemplate(){
        return getSourcePath()+File.separator+this.path.getSource().getTemplates();
    }

    public String getFileServerIp(){
        return this.path.getSource().getFileServerIp();
    }

    public String getPlugins(){
        return getSourcePath()+File.separator+this.path.getSource().getPlugins();
    }

    @Data
    public static class path{


        public ThinkItProperties.path.site site = new site();

        public ThinkItProperties.path.source source = new source();

        private String rootPath;

        @Data
        public static class site{
            /**
             * 网站跟路径名称
             */
            private String pathName;

        }

        @Data
        public static class source{

            /**
             * 资源跟路径名称
             */
            private String pathName;

            /**
             * 网站模板跟目录
             */
            private String templates;

            /**
             * 文件服务器 地址（可以为域名）
             */
            private String fileServerIp;


            /**
             * 文件服务器类型 ： oss,local fastdfs
             */
            private String fileServer;


            /**
             * 网站插件目录
             */
            private String plugins;
        }
    }
}
