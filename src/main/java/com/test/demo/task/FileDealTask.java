package com.test.demo.task;

import com.alibaba.fastjson.JSON;
import com.test.demo.VO.AQBJDataVO;
import com.test.demo.VO.AQBJRootVO;
import com.test.demo.VO.AQSSDataVO;
import com.test.demo.VO.AQSSRootVO;
import com.test.demo.entity.LogInfo;
import com.test.demo.entity.SensorInfo;
import com.test.demo.service.SensorService;
import com.test.demo.util.JaxbUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static java.math.BigDecimal.ROUND_HALF_UP;

@Component
public class FileDealTask {

    @Resource(name = "sensorService")
    private SensorService sensorService;

    private String writeParentFilePath = null;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileDealTask.class);

    public void readProperties() throws IOException {
        Properties properties = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = FileDealTask.class.getClassLoader().getResourceAsStream("sys-params.properties");
        // 使用properties对象加载输入流
        properties.load(in);
        //获取key对应的value值
        writeParentFilePath = properties.getProperty("writeParentFilePath");
        LOGGER.info("writeParentFilePath===>>{}",writeParentFilePath);
    }
    /**
     * 读取某个文件夹下的所有文件
     */
    public void readfile(String filepath) throws FileNotFoundException, IOException {
        try {
            LOGGER.info("进入读取文件。。。。。。。");
            File file = new File(filepath);
            // 判断是文件夹还是文件
            if (!file.isDirectory()) {
                LOGGER.info("文件");
                LOGGER.info("path=" + file.getPath());
                LOGGER.info("absolutepath=" + file.getAbsolutePath());
                LOGGER.info("name=" + file.getName());
                if(file !=  null){
                    boolean flag =  updateFileInfo(file);
                    // 删除原目录中的文件
                    if(file.exists() && flag){
                        System.gc();	//加上确保文件能删除，不然可能删不掉
                        boolean deleteflag = file.delete();
                        if(deleteflag){
                            LOGGER.info("-------------文件名为==>"+file.getName()+ "文件已删除----------------");
                        }
                    }
                }
            } else if (file.isDirectory()) {
                LOGGER.info("----------------文件夹------------");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        if(readfile != null){
                            boolean flag = updateFileInfo(readfile);
                            // 删除原目录中的文件
                            if(readfile.exists() && flag){
                                System.gc();	//加上确保文件能删除，不然可能删不掉
                                boolean deleteflag = readfile.delete();
                                if(deleteflag){
                                    LOGGER.info("-------------文件名为==>"+readfile.getName()+ "文件已删除----------------");
                                }
                            }
                        }
                    } else if (readfile.isDirectory()) {
                        readfile(filepath + "\\" + filelist[i]);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>FileNotFoundException:" + e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            LOGGER.info("-------------readfile()——>文件处理异常:" + e.getMessage());
        }
    }

    // 对读取到的文件进行处理
    @Transactional
    public boolean updateFileInfo(File readfile){
        InputStreamReader isr = null;
        try {
            String fileName =  readfile.getName();
            // 根绝文件名称查询当前文件是否进行修改
            boolean fileFlag = sensorService.queryLogInfoByName(fileName);
            if(!fileFlag){

                LOGGER.info("-------------文件名称----------------:"+ fileName);

                StringBuffer sbuf = new StringBuffer();
                isr = new InputStreamReader(new FileInputStream(readfile), "UTF-8");

                char[] ch=new char[2014];

                int len=isr.read(ch);

                String line = new String(ch,0,len);

                sbuf.append(line);

                LOGGER.info("-------------读取到的XML数据----------------:"+ sbuf.toString());

                if(fileName.indexOf("AQSS") > 1 || fileName.indexOf("AQBJ") > 1){

                    if(fileName.indexOf("AQSS") > 1){
                        AQSSRootVO aqssRootVO = (AQSSRootVO)JaxbUtil.xmlStrToOject(AQSSRootVO.class,sbuf.toString());
                        LOGGER.info("-------------XML --> AQSSRootVO实体----------------:"+ JSON.toJSONString(aqssRootVO));

                        List<AQSSDataVO> aqssDataVOList = aqssRootVO.getDataVOList();

                        for(int i = 0; i < aqssDataVOList.size(); i++){
                            AQSSDataVO aqssDataVO = aqssDataVOList.get(i);
                            String  ssTransducerCode = aqssDataVO.getSs_transducer_code();
                            BigDecimal ssAnalogValue = aqssDataVO.getSs_analog_value();
                            // 修改报警值
                            BigDecimal diffValue = dealAnalogValue(ssTransducerCode,ssAnalogValue);
                            LOGGER.info("需要修改的差异值是===>>>{}",diffValue);
                            if(diffValue.compareTo(new BigDecimal(-1)) != 0) {
                                aqssDataVO.setSs_analog_value(diffValue);
                                aqssDataVO.setSs_transducer_state("0");
                            }
                        }
                        // 将Java对象转成XML写出到相应的文件夹中
                        String xmlData = JaxbUtil.beanToXml(aqssRootVO,AQSSRootVO.class);
                        writeToFile(xmlData,fileName);

                        LogInfo logInfo = new LogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
                        boolean flag1 = sensorService.insertIntoFileLog(logInfo);
                        if(!flag1){
                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                        }
                    }else if(fileName.indexOf("AQBJ") > 1){
                        AQBJRootVO aqbjRootVO = (AQBJRootVO)JaxbUtil.xmlStrToOject(AQBJRootVO.class,sbuf.toString());
                        LOGGER.info("-------------XML --> AQBJRootVO实体----------------:"+ JSON.toJSONString(aqbjRootVO));

                        AQBJDataVO aqbjDataVO = aqbjRootVO.getData();

                        String  ssTransducerCode = aqbjDataVO.getSs_transducer_code();
                        BigDecimal ssAnalogValue = aqbjDataVO.getSs_analog_value();

                        // 修改报警值
                        BigDecimal diffValue = dealAnalogValue(ssTransducerCode,ssAnalogValue);

                        if(diffValue.compareTo(new BigDecimal(-1)) != 0) {
                            aqbjDataVO.setSs_analog_value(diffValue);
                            aqbjDataVO.setSs_transducer_state("0");
                        }
                        // 将Java对象转成XML写出到相应的文件夹中
                        String xmlData = JaxbUtil.beanToXml(aqbjRootVO,AQBJRootVO.class);
                        LOGGER.info("-------------需要写入文件的数据----------------{}:"+ JSON.toJSONString(xmlData));
                        writeToFile(xmlData,fileName);
                        LogInfo logInfo = new LogInfo();
                        logInfo.setFileName(fileName);
                        logInfo.setStatus("01");
                        logInfo.setCreateTime(new Date());
                        boolean flag = sensorService.insertIntoFileLog(logInfo);
                        if(!flag){
                            LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                        }
                    }
                }else{
                    // 将文件名称中不含 “AQSS”，“AQMC”，“AQBJ”的文件直接输出当固定文件夹中
                    writeToFile(sbuf.toString(),fileName);
                    LogInfo logInfo = new LogInfo();
                    logInfo.setFileName(fileName);
                    logInfo.setStatus("01");
                    logInfo.setCreateTime(new Date());
                    boolean flag = sensorService.insertIntoFileLog(logInfo);
                    if(!flag){
                        LOGGER.info("-------------文件日志入库失败----------------:"+ JSON.toJSONString(logInfo));
                    }
                }
            }
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>FileNotFoundException:" + e.getMessage());
        } catch (IOException e){
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>IOException:" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("-------------updateFileInfo()——>Exception:" + e.getMessage());
        }finally {
            try {
                if(isr != null){
                    isr.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * 查询当前编号对应的报警值是否大于报警值的最大值
     *
     */
    public BigDecimal dealAnalogValue(String  ssTransducerCode, BigDecimal ssAnalogValue){
        LOGGER.info("-------------ssTransducerCode:" + ssTransducerCode);
        BigDecimal factValue = new BigDecimal(-1);
        // 根据XML编码查询报警值
        SensorInfo sensorInfo = sensorService.querySensorDetailByXMLCode(ssTransducerCode);
        if(sensorInfo != null){
            BigDecimal minValue = sensorInfo.getMinAlarm();
            BigDecimal maxValue = sensorInfo.getMaxAlarm();
            if(ssAnalogValue != null && minValue!= null && maxValue != null){
                if(ssAnalogValue.compareTo(minValue) ==  -1){

                     BigDecimal diffValue = minValue.subtract(ssAnalogValue);
                     factValue = ssAnalogValue.add(diffValue).add(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);

                }else if(ssAnalogValue.compareTo(maxValue) ==  1){

                    BigDecimal diffValue = ssAnalogValue.subtract(maxValue);
                    factValue = ssAnalogValue.subtract(diffValue).subtract(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);

                }else if(ssAnalogValue.compareTo(maxValue) == 0){

                    factValue = ssAnalogValue.subtract(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);

                }else if(ssAnalogValue.compareTo(minValue) == 0){

                    factValue = ssAnalogValue.add(new BigDecimal(0.05)).setScale(5,ROUND_HALF_UP);
                }
            }
        }
        return factValue;
    }

    public void writeToFile(String data,String fileName){
//        byte[] sourceByte = data.getBytes();
        String path = writeParentFilePath;
        if(null != data && !"".equals(data)){
            OutputStreamWriter osw = null;
            FileOutputStream fos = null;
            try {
                File file = new File(path+fileName);//文件路径（路径+文件名）
                if (!file.exists()) {   //文件不存在则创建文件，先创建目录
                    File dir = new File(file.getParent());
                    dir.mkdirs();
                    file.createNewFile();
                }
                fos = new FileOutputStream(file); //文件输出流将数据写入文件
                osw = new OutputStreamWriter(fos,"UTF-8");

                osw.write(data);
                osw.flush();

            } catch (Exception e) {
                LOGGER.info("------------- 文件生成异常 Exception:" + e.getMessage());
                e.printStackTrace();
            }finally {
                try {
                    fos.close();
                    osw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
