package com.test.demo.dbUtil;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestPreparedStatement {
    public  static  List<Map<String,String>> list = new ArrayList<Map<String,String>>();//存学生对象的集合
    public int a;
    public void flag(){
        a = 10;
        System.out.println(a);
        //驱动类
        String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        //url地址
        String url = "jdbc:sqlserver://localhost:1433;databaseName=test";
        String userName = "sa";
        String password = "123456";
        Connection conn = null;//用于应用程序和数据的连接
        Statement stmt = null;//已经创建连接的基础上，向数据库发送SQL语句
        ResultSet rs = null;//暂时存放数据库查询操作所获得的结果
         
        try {
            //加载驱动类
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            //得到一个连接
            conn = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=test",  "sa","123456");//得到一个连接
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from Info");//执行SQL语句，并存在ResultSet对象中
            while (rs.next()) {
                //存对象到集合
                Map student = new HashMap();
                student.put("id",rs.getString("id"));
                student.put("name",rs.getString("name"));
                student.put("sex",rs.getString("sex"));
//                rs.getString("id"), rs.getString("name"), rs.getString("sex"));
                TestPreparedStatement.list.add(student);

                System.out.print(rs.getString("id")); //读取数据
                System.out.print(rs.getString("name"));
                System.out.print(rs.getString("sex"));
                System.out.println();
            }
 
            //   创建PreparedStatement对象
            //PreparedStatement pstmt = conn.prepareStatement("insert  into  Info values(?, ? ,?)");//添加
            //删除
            PreparedStatement pstmt1 = conn.prepareStatement("delete from Info where id = ? and name = ?");
            //使用set方法设置参数
            PreparedStatement pstmt2 = conn.prepareStatement("update Info set name=? where name ='star' ");
            pstmt1.setInt(1, 4);
            pstmt1.setString(2, "the");//根据id和name删除数据库的内容
             
            /*pstmt.setInt(1, 1);
            pstmt.setString(2, "star");
            pstmt.setString(3, "nan");//添加数值对到数据库
            */
            pstmt2.setString(1, "super");//更新数据
            pstmt2.executeUpdate();
            //pstmt.executeUpdate();
            pstmt1.executeUpdate();//执行PreparedStatement语句
        } catch (SQLException e) {
            e.printStackTrace();
            }finally{
                        //关闭连接
                        try {
                            rs.close();
                            stmt.close();
                            conn.close();
                        } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                 
        }
            System.out.println("----------------");
        for (int i = 0; i < TestPreparedStatement.list.size(); i++) {
            System.out.println(TestPreparedStatement.list.get(i));
        }
    }
     
    public static void main(String[] args) {
        new TestPreparedStatement().flag();
         
    }
}