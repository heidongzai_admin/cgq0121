package com.test.demo.dbUtil;

import com.sun.media.jfxmedia.logging.Logger;
import com.test.demo.properties.LicenseMark;
import com.test.demo.service.SensorService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.Null;
import org.springframework.context.support.StaticApplicationContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * @Author zhy
 * @Description sqlserver连接工具类
 * @ClassName JDBCSqlServerUtils
 * @Date 2021-1-21 9:56
 **/
@Slf4j
public class JDBCSqlServerUtils {
    @Getter
    @Setter
    @LicenseMark
    private static String JDBCUrl;

    @Getter
    @Setter
    @LicenseMark
    private static String username;

    @Getter
    @Setter
    @LicenseMark
    private static String password;

    @Getter
    @Setter
    @LicenseMark
    private static String form;

    @Getter
    @Setter
    @LicenseMark
    private static String driver;
    static {
        InputStream in = SensorService.class.getClassLoader().getResourceAsStream("sys-params.properties");
        // 使用properties对象加载输入流
        try {
            Properties properties = new Properties();
            properties.load(in);
            JDBCUrl=properties.getProperty("jdbcurl");
            username=properties.getProperty("username");
            password=properties.getProperty("password");
            form=properties.getProperty("form");
            driver=properties.getProperty("driver");
        } catch (IOException e) {
            log.error(e.getMessage(),e);
        }
    }
    /**
    * 创建sqlserver数据库连接
    * @Function: getConnection

    * @Return java.sql.Connection
    *
    * @author: zhy
    * @date: 2021-01-21 10:33
    */
    public static Connection getConnection(){
        //加载驱动
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(),e);
        }
        //创建连接

        try {
           return DriverManager.getConnection(JDBCUrl,username,password);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }
    
    /** 
    * 关闭连接
    * @Function: close 
    * @param rs
* @param stmt
* @param con 
    * @Return void
    *
    * @author: zhy
    * @date: 2021-01-21 10:34
    */
    public static void close(ResultSet rs, Statement stmt, Connection con) throws SQLException {
        if(rs!=null)
            rs.close();
        if(stmt!=null)
            stmt.close();
        if(con!=null)
            con.close();
    }



}
