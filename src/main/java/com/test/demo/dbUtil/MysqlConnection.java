package com.test.demo.dbUtil;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
public class MysqlConnection {
    public static void main(String[] args) {   
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Connection conn=null;
        PreparedStatement prep = null;
        Statement state = null;
        ResultSet rs  = null;
        try {
            String url = "jdbc:mysql://localhost:3306/mytest";
             conn = DriverManager.getConnection(url, "root", "123456");
             state = conn.createStatement();
             prep = conn.prepareStatement("select * from useinfo");
             rs = prep.executeQuery();
            while (rs.next()) {
                int id= rs.getInt("userId");
                String name= rs.getString("userName");
                String sex = rs.getString("userSex");
                System.out.println("从mysql读取的数据："+id+"---------"+name+"--------"+sex);
            }
             
             
        }  catch (SQLException e) {
            e.printStackTrace();
        }finally{
             
                try {
                    if(rs==null)  rs.close();
                    if(prep==null) prep.close();
                    if(state==null)  state.close();
                    if(conn==null)  conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
             
        }
         
         
         
    }

    //crud: create read update delete
    //插入语句
    public static void insert(String username,String password) throws SQLException {
        //注册驱动    使用驱动连接数据库
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = JDBCUtils.getConnection();
            String sql = "insert into garytb(username,password) values(?,?)";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            int result =stmt.executeUpdate();// 返回值代表收到影响的行数
            System.out.println("插入成功"+username);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            JDBCUtils.close(rs, stmt, con);
        }
    }
    //删除语句
    public static void delete(int id) throws SQLException {
        //注册驱动    使用驱动连接数据库
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = JDBCUtils.getConnection();

            String sql = "delete from garytb where id = ?";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            int result =stmt.executeUpdate();// 返回值代表收到影响的行数
            if(result>0) {
                System.out.println("删除成功");
            }else {
                System.out.println("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.close(rs, stmt, con);
        }
    }
    //修改语句
    public static void update(int id,String newPassword) throws SQLException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = JDBCUtils.getConnection();

            String sql = "update garytb set password = ? where id = ?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, newPassword);
            stmt.setInt(2, id);
            int result =stmt.executeUpdate();// 返回值代表收到影响的行数
            if(result>0) {
                System.out.println("修改成功");
            }else {
                System.out.println("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.close(rs, stmt, con);
        }
    }
}