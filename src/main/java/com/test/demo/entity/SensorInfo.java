package com.test.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "t_sensor_info")
public class SensorInfo {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 编号
     */
    @Column(name = "sensor_no")
    private String sensorNo;

    /**
     * xml中的编码
     */
    @Column(name = "sensor_xml_no")
    private String sensorXmlNo;

    /**
     * 位置
     */
    @Column(name = "sensor_location")
    private String sensorLocation;

    /**
     * 传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     */
    @Column(name = "sensor_type")
    private String sensorType;

    /**
     * 最小实际报警值
     */
    @Column(name = "min_alarm")
    private BigDecimal minAlarm;

    /**
     * 最大实际报警值
     */
    @Column(name = "max_alarm")
    private BigDecimal maxAlarm;

    /**
     * 调教报警范围
     */
    @Column(name = "alarm_scope")
    private String alarmScope;

    /**
     * 备注
     */
    private String note;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 修改时间
     */
    @Column(name = "is_valid")
    private int isValid;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取编号
     *
     * @return sensor_no - 编号
     */
    public String getSensorNo() {
        return sensorNo;
    }

    /**
     * 设置编号
     *
     * @param sensorNo 编号
     */
    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    /**
     * 获取xml中的编码
     *
     * @return sensor_xml_no - xml中的编码
     */
    public String getSensorXmlNo() {
        return sensorXmlNo;
    }

    /**
     * 设置xml中的编码
     *
     * @param sensorXmlNo xml中的编码
     */
    public void setSensorXmlNo(String sensorXmlNo) {
        this.sensorXmlNo = sensorXmlNo;
    }

    /**
     * 获取位置
     *
     * @return sensor_location - 位置
     */
    public String getSensorLocation() {
        return sensorLocation;
    }

    /**
     * 设置位置
     *
     * @param sensorLocation 位置
     */
    public void setSensorLocation(String sensorLocation) {
        this.sensorLocation = sensorLocation;
    }

    /**
     * 获取传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     *
     * @return sensor_type - 传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     */
    public String getSensorType() {
        return sensorType;
    }

    /**
     * 设置传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     *
     * @param sensorType 传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     */
    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    /**
     * 获取最小实际报警值
     *
     * @return min_alarm - 最小实际报警值
     */
    public BigDecimal getMinAlarm() {
        return minAlarm;
    }

    /**
     * 设置最小实际报警值
     *
     * @param minAlarm 最小实际报警值
     */
    public void setMinAlarm(BigDecimal minAlarm) {
        this.minAlarm = minAlarm;
    }

    /**
     * 获取最大实际报警值
     *
     * @return max_alarm - 最大实际报警值
     */
    public BigDecimal getMaxAlarm() {
        return maxAlarm;
    }

    /**
     * 设置最大实际报警值
     *
     * @param maxAlarm 最大实际报警值
     */
    public void setMaxAlarm(BigDecimal maxAlarm) {
        this.maxAlarm = maxAlarm;
    }

    /**
     * 获取调教报警范围
     *
     * @return alarm_scope - 调教报警范围
     */
    public String getAlarmScope() {
        return alarmScope;
    }

    /**
     * 设置调教报警范围
     *
     * @param alarmScope 调教报警范围
     */
    public void setAlarmScope(String alarmScope) {
        this.alarmScope = alarmScope;
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsValid() {
        return isValid;
    }

    public void setIsValid(int isValid) {
        this.isValid = isValid;
    }
}