package com.test.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.groups.ConvertGroup;

@Getter
@Setter
@Table(name = "t_config")
public class TConfig {
    /**
     * 主键
     */
    @Column(name = "id")
    @Id
    private String id;

    /**
     * db14开关
     */
    @Column(name = "qsd_switch")
    private String qsdSwitch;

    /**
     * 新元开关
     */
    @Column(name = "fsd_switch")
    private String fsdSwitch;

    /**
     * 分时段开始时间
     */
    @Column(name = "fsd_begin_time")
    private String fsdBeginTime;

    /**
     * 分时段结束时间
     */
    @Column(name = "fsd_end_time")
    private String fsdEndTime;

    /**
     * 注释
     */
    @Column(name = "mark")
    private String mark;

    /**
     * 全时段开关
     */
    @Column(name="alltime_switch")
    private String alltimeSwitch;

    /**
     * 分时段开关
     */
    @Column(name = "parttime_switch")
    private String parttimeSwitch;

    /**
     * 密采开关
     */
    @Column(name = "mc_switch")
    private String mcSwitch;

    /**
     * 数据库修改开关
     */
    @Column(name = "db_switch")
    private String dbSwitch;

    /**
     * 分时段菜单开关
     */
    @Column(name = "parttime_menu_switch")
    private String parttimeMenuSwitch;

    /**
     * 配置菜单开关
     */
    @Column(name = "config_menu_switch")
    private String configMenuSwitch;

    /**
     * 密采菜单开关
     */
    @Column(name = "mc_switch_on")
    private String mcSwitchOn;

    /**
     * db菜单开关
     */
    @Column(name = "db_switch_on")
    private String dbSwitchOn;

    /**
     * 新元菜单开关
     */
    @Column(name = "sy_switch_on")
    private String sySwitchOn;

    /**
     * db14开关
     */
    @Column(name = "db14_switch_on")
    private String db14SwitchOn;


    /**
     * 全时修改的状态id“,”隔开
     */
    @Column(name = "statusIds")
    private String statusIds;

    /**
     * 分时修改的状态
     */
    @Column(name = "parttime_statusIds")
    private String parttimeStatusIds;

    /**
     * 目标状态
     */
    @Column(name = "target_status")
    private String targetStatus;

}