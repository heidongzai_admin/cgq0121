package com.test.demo.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "t_sy_sensor_info_fsd")
public class SySensorInfoFsd {
    /**
     * 主键
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Id
    private Integer id;

    /**
     * 编号
     */
    @Column(name = "kuangming")
    private String kuangming;

    /**
     * xml中的编码
     */
    @Column(name = "fenzhan")
    private String fenzhan;

    /**
     * 状态
     */
    @Column(name = "status_cd")
    private String statusCd;
    /**
     * 位置
     */
    @Column(name = "code")
    private String code;

    /**
     * 传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     */
    @Column(name = "type")
    private String type;

    /**
     * 最小实际报警值
     */
    @Column(name = "name")
    private String name;

    /**
     * 最大实际报警值
     */
    @Column(name = "max_alarm")
    private BigDecimal maxAlarm;

    /**
     * 调教报警范围
     */
    @Column(name = "min_alarm")
    private BigDecimal minAlarm;

    @Column(name = "alarm_scope")
    private String alarmScope;

    /**
     * 备注
     */
    private String note;

    /**
     * 是否已删除 0 -删除 1 未删除
     */
    @Column(name = "is_valid")
    private Integer isValid;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * uniqueId
     */
    @Column(name = "unique_id")
    private String uniqueId;

    public String getUniqueId() {
        return uniqueId;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取编号
     *
     * @return kuangming - 编号
     */
    public String getKuangming() {
        return kuangming;
    }

    /**
     * 设置编号
     *
     * @param kuangming 编号
     */
    public void setKuangming(String kuangming) {
        this.kuangming = kuangming;
    }

    /**
     * 获取xml中的编码
     *
     * @return fenzhan - xml中的编码
     */
    public String getFenzhan() {
        return fenzhan;
    }

    /**
     * 设置xml中的编码
     *
     * @param fenzhan xml中的编码
     */
    public void setFenzhan(String fenzhan) {
        this.fenzhan = fenzhan;
    }

    /**
     * 获取位置
     *
     * @return code - 位置
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置位置
     *
     * @param code 位置
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     *
     * @return type - 传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     */
    public String getType() {
        return type;
    }

    /**
     * 设置传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     *
     * @param type 传感器类型 01 -  甲烷传感器，02-二氧化碳传感器，03-一氧化碳传感器，04-风速传感器，05-风压传感器，06-温度传感器， 07-氧气传感器
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取最小实际报警值
     *
     * @return name - 最小实际报警值
     */
    public String getName() {
        return name;
    }

    /**
     * 设置最小实际报警值
     *
     * @param name 最小实际报警值
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取最大实际报警值
     *
     * @return max_alarm - 最大实际报警值
     */
    public BigDecimal getMaxAlarm() {
        return maxAlarm;
    }

    /**
     * 设置最大实际报警值
     *
     * @param maxAlarm 最大实际报警值
     */
    public void setMaxAlarm(BigDecimal maxAlarm) {
        this.maxAlarm = maxAlarm;
    }

    /**
     * 获取调教报警范围
     *
     * @return min_alarm - 调教报警范围
     */
    public BigDecimal getMinAlarm() {
        return minAlarm;
    }

    /**
     * 设置调教报警范围
     *
     * @param minAlarm 调教报警范围
     */
    public void setMinAlarm(BigDecimal minAlarm) {
        this.minAlarm = minAlarm;
    }

    /**
     * @return alarm_scope
     */
    public String getAlarmScope() {
        return alarmScope;
    }

    /**
     * @param alarmScope
     */
    public void setAlarmScope(String alarmScope) {
        this.alarmScope = alarmScope;
    }

    /**
     * 获取备注
     *
     * @return note - 备注
     */
    public String getNote() {
        return note;
    }

    /**
     * 设置备注
     *
     * @param note 备注
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * 获取是否已删除 0 -删除 1 未删除
     *
     * @return is_valid - 是否已删除 0 -删除 1 未删除
     */
    public Integer getIsValid() {
        return isValid;
    }

    /**
     * 设置是否已删除 0 -删除 1 未删除
     *
     * @param isValid 是否已删除 0 -删除 1 未删除
     */
    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}