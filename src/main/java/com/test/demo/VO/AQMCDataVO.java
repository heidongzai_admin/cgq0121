package com.test.demo.VO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.FIELD)
public class AQMCDataVO {

    @XmlAttribute(name = "ss_station_code")
    private String ss_station_code;

    @XmlAttribute(name = "ss_transducer_code")
    private String ss_transducer_code;

    @XmlAttribute(name = "ss_transducer_type")
    private String ss_transducer_type;

    @XmlAttribute(name = "ss_transducer_name")
    private String ss_transducer_name;

    @XmlAttribute(name = "ss_analog_unit")
    private String ss_analog_unit;

    @XmlAttribute(name = "ss_transducer_point")
    private String ss_transducer_point;

    @XmlAttribute(name = "ss_gisnode_code")
    private String ss_gisnode_code;

    @XmlAttribute(name = "ss_analog_lower")
    private BigDecimal ss_analog_lower;

    @XmlAttribute(name = "ss_analog_high")
    private BigDecimal ss_analog_high;

    @XmlAttribute(name = "ss_analog_alarmlow")
    private BigDecimal ss_analog_alarmlow;

    @XmlAttribute(name = "ss_analog_alarmhigh")
    private BigDecimal ss_analog_alarmhigh;

    @XmlAttribute(name = "ss_analog_pofflow")
    private BigDecimal ss_analog_pofflow;

    @XmlAttribute(name = "ss_analog_poffhigh")
    private BigDecimal ss_analog_poffhigh;

    @XmlAttribute(name = "ss_analog_ponlow")
    private BigDecimal ss_analog_ponlow;

    @XmlAttribute(name = "ss_analog_ponhigh")
    private BigDecimal ss_analog_ponhigh;

    @XmlAttribute(name = "ss_poffarea_code")
    private String ss_poffarea_code;

    public String getSs_station_code() {
        return ss_station_code;
    }

    public void setSs_station_code(String ss_station_code) {
        this.ss_station_code = ss_station_code;
    }

    public String getSs_transducer_code() {
        return ss_transducer_code;
    }

    public void setSs_transducer_code(String ss_transducer_code) {
        this.ss_transducer_code = ss_transducer_code;
    }

    public String getSs_transducer_type() {
        return ss_transducer_type;
    }

    public void setSs_transducer_type(String ss_transducer_type) {
        this.ss_transducer_type = ss_transducer_type;
    }

    public String getSs_transducer_name() {
        return ss_transducer_name;
    }

    public void setSs_transducer_name(String ss_transducer_name) {
        this.ss_transducer_name = ss_transducer_name;
    }

    public String getSs_analog_unit() {
        return ss_analog_unit;
    }

    public void setSs_analog_unit(String ss_analog_unit) {
        this.ss_analog_unit = ss_analog_unit;
    }

    public String getSs_transducer_point() {
        return ss_transducer_point;
    }

    public void setSs_transducer_point(String ss_transducer_point) {
        this.ss_transducer_point = ss_transducer_point;
    }

    public String getSs_gisnode_code() {
        return ss_gisnode_code;
    }

    public void setSs_gisnode_code(String ss_gisnode_code) {
        this.ss_gisnode_code = ss_gisnode_code;
    }

    public BigDecimal getSs_analog_lower() {
        return ss_analog_lower;
    }

    public void setSs_analog_lower(BigDecimal ss_analog_lower) {
        this.ss_analog_lower = ss_analog_lower;
    }

    public BigDecimal getSs_analog_high() {
        return ss_analog_high;
    }

    public void setSs_analog_high(BigDecimal ss_analog_high) {
        this.ss_analog_high = ss_analog_high;
    }

    public BigDecimal getSs_analog_alarmlow() {
        return ss_analog_alarmlow;
    }

    public void setSs_analog_alarmlow(BigDecimal ss_analog_alarmlow) {
        this.ss_analog_alarmlow = ss_analog_alarmlow;
    }

    public BigDecimal getSs_analog_alarmhigh() {
        return ss_analog_alarmhigh;
    }

    public void setSs_analog_alarmhigh(BigDecimal ss_analog_alarmhigh) {
        this.ss_analog_alarmhigh = ss_analog_alarmhigh;
    }

    public BigDecimal getSs_analog_pofflow() {
        return ss_analog_pofflow;
    }

    public void setSs_analog_pofflow(BigDecimal ss_analog_pofflow) {
        this.ss_analog_pofflow = ss_analog_pofflow;
    }

    public BigDecimal getSs_analog_poffhigh() {
        return ss_analog_poffhigh;
    }

    public void setSs_analog_poffhigh(BigDecimal ss_analog_poffhigh) {
        this.ss_analog_poffhigh = ss_analog_poffhigh;
    }

    public BigDecimal getSs_analog_ponlow() {
        return ss_analog_ponlow;
    }

    public void setSs_analog_ponlow(BigDecimal ss_analog_ponlow) {
        this.ss_analog_ponlow = ss_analog_ponlow;
    }

    public BigDecimal getSs_analog_ponhigh() {
        return ss_analog_ponhigh;
    }

    public void setSs_analog_ponhigh(BigDecimal ss_analog_ponhigh) {
        this.ss_analog_ponhigh = ss_analog_ponhigh;
    }

    public String getSs_poffarea_code() {
        return ss_poffarea_code;
    }

    public void setSs_poffarea_code(String ss_poffarea_code) {
        this.ss_poffarea_code = ss_poffarea_code;
    }

    @Override
    public String toString() {
        return "AQMCDataVO{" +
                "ss_station_code='" + ss_station_code + '\'' +
                ", ss_transducer_code='" + ss_transducer_code + '\'' +
                ", ss_transducer_type='" + ss_transducer_type + '\'' +
                ", ss_transducer_name='" + ss_transducer_name + '\'' +
                ", ss_analog_unit='" + ss_analog_unit + '\'' +
                ", ss_transducer_point='" + ss_transducer_point + '\'' +
                ", ss_gisnode_code='" + ss_gisnode_code + '\'' +
                ", ss_analog_lower=" + ss_analog_lower +
                ", ss_analog_high=" + ss_analog_high +
                ", ss_analog_alarmlow=" + ss_analog_alarmlow +
                ", ss_analog_alarmhigh=" + ss_analog_alarmhigh +
                ", ss_analog_pofflow=" + ss_analog_pofflow +
                ", ss_analog_poffhigh=" + ss_analog_poffhigh +
                ", ss_analog_ponlow=" + ss_analog_ponlow +
                ", ss_analog_ponhigh=" + ss_analog_ponhigh +
                ", ss_poffarea_code='" + ss_poffarea_code + '\'' +
                '}';
    }
}


