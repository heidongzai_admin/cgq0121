package com.test.demo.VO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.FIELD)
public class AQBJDataVO {

    @XmlAttribute(name = "ss_station_code")
    private String ss_station_code;

    @XmlAttribute(name = "ss_transducer_code")
    private String ss_transducer_code;

    @XmlAttribute(name = "ss_transducer_state")
    private String ss_transducer_state;

    @XmlAttribute(name = "ss_analog_value")
    private BigDecimal ss_analog_value;

    @XmlAttribute(name = "ss_alarm_stime")
    private String ss_alarm_stime;

    @XmlAttribute(name = "ss_alarm_etime")
    private String ss_alarm_etime;

    @XmlAttribute(name = "ss_deal_measure")
    private String ss_deal_measure;

    public String getSs_station_code() {
        return ss_station_code;
    }

    public void setSs_station_code(String ss_station_code) {
        this.ss_station_code = ss_station_code;
    }

    public String getSs_transducer_code() {
        return ss_transducer_code;
    }

    public void setSs_transducer_code(String ss_transducer_code) {
        this.ss_transducer_code = ss_transducer_code;
    }

    public String getSs_transducer_state() {
        return ss_transducer_state;
    }

    public void setSs_transducer_state(String ss_transducer_state) {
        this.ss_transducer_state = ss_transducer_state;
    }

    public BigDecimal getSs_analog_value() {
        return ss_analog_value;
    }

    public void setSs_analog_value(BigDecimal ss_analog_value) {
        this.ss_analog_value = ss_analog_value;
    }

    public String getSs_alarm_stime() {
        return ss_alarm_stime;
    }

    public void setSs_alarm_stime(String ss_alarm_stime) {
        this.ss_alarm_stime = ss_alarm_stime;
    }

    public String getSs_alarm_etime() {
        return ss_alarm_etime;
    }

    public void setSs_alarm_etime(String ss_alarm_etime) {
        this.ss_alarm_etime = ss_alarm_etime;
    }

    public String getSs_deal_measure() {
        return ss_deal_measure;
    }

    public void setSs_deal_measure(String ss_deal_measure) {
        this.ss_deal_measure = ss_deal_measure;
    }

    @Override
    public String toString() {
        return "AQBJDataVO{" +
                "ss_station_code='" + ss_station_code + '\'' +
                ", ss_transducer_code='" + ss_transducer_code + '\'' +
                ", ss_transducer_state='" + ss_transducer_state + '\'' +
                ", ss_analog_value='" + ss_analog_value + '\'' +
                ", ss_alarm_stime=" + ss_alarm_stime +
                ", ss_alarm_etime=" + ss_alarm_etime +
                ", ss_deal_measure='" + ss_deal_measure + '\'' +
                '}';
    }
}
