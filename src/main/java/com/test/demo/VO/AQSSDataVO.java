package com.test.demo.VO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.FIELD)
public class AQSSDataVO {

    @XmlAttribute(name = "ss_station_code")
    private String ss_station_code;

    @XmlAttribute(name = "ss_transducer_code")
    private String ss_transducer_code;

    @XmlAttribute(name = "ss_transducer_state")
    private String ss_transducer_state;

    @XmlAttribute(name = "ss_analog_value")
    private BigDecimal ss_analog_value;

    @XmlAttribute(name = "ss_switching_value")
    private BigDecimal ss_switching_value;

    public String getSs_station_code() {
        return ss_station_code;
    }

    public void setSs_station_code(String ss_station_code) {
        this.ss_station_code = ss_station_code;
    }

    public String getSs_transducer_code() {
        return ss_transducer_code;
    }

    public void setSs_transducer_code(String ss_transducer_code) {
        this.ss_transducer_code = ss_transducer_code;
    }

    public String getSs_transducer_state() {
        return ss_transducer_state;
    }

    public void setSs_transducer_state(String ss_transducer_state) {
        this.ss_transducer_state = ss_transducer_state;
    }

    public BigDecimal getSs_analog_value() {
        return ss_analog_value;
    }

    public void setSs_analog_value(BigDecimal ss_analog_value) {
        this.ss_analog_value = ss_analog_value;
    }

    public BigDecimal getSs_switching_value() {
        return ss_switching_value;
    }

    public void setSs_switching_value(BigDecimal ss_switching_value) {
        this.ss_switching_value = ss_switching_value;
    }

    @Override
    public String toString() {
        return "AQSSDataVO{" +
                "ss_station_code='" + ss_station_code + '\'' +
                ", ss_transducer_code='" + ss_transducer_code + '\'' +
                ", ss_transducer_state='" + ss_transducer_state + '\'' +
                ", ss_analog_value=" + ss_analog_value +
                ", ss_switching_value=" + ss_switching_value +
                '}';
    }
}
