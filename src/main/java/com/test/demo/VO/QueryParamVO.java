package com.test.demo.VO;

public class QueryParamVO {

    private String sensorNo;
    private String sensorXmlNo;
    private String sensorLocation;
    private String sensorType;
    private String kuangming;

    private String fenzhan;

    private String code;

    private String type;

    private String name;
    private int pn;
    private int ps;
    private int cn;

    public String getSensorNo() {
        return sensorNo;
    }

    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    public String getSensorXmlNo() {
        return sensorXmlNo;
    }

    public void setSensorXmlNo(String sensorXmlNo) {
        this.sensorXmlNo = sensorXmlNo;
    }

    public String getSensorLocation() {
        return sensorLocation;
    }

    public void setSensorLocation(String sensorLocation) {
        this.sensorLocation = sensorLocation;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public int getPn() {
        return pn;
    }

    public void setPn(int pn) {
        this.pn = pn;
    }

    public int getPs() {
        return ps;
    }

    public void setPs(int ps) {
        this.ps = ps;
    }

    public int getCn() {
        return cn;
    }

    public void setCn(int cn) {
        this.cn = cn;
    }

    public String getKuangming() {
        return kuangming;
    }

    public void setKuangming(String kuangming) {
        this.kuangming = kuangming;
    }

    public String getFenzhan() {
        return fenzhan;
    }

    public void setFenzhan(String fenzhan) {
        this.fenzhan = fenzhan;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "QueryParamVO{" +
                "sensorNo='" + sensorNo + '\'' +
                ", sensorXmlNo='" + sensorXmlNo + '\'' +
                ", sensorLocation='" + sensorLocation + '\'' +
                ", sensorType='" + sensorType + '\'' +
                ", pn=" + pn +
                ", ps=" + ps +
                ", cn=" + cn +
                '}';
    }
}
