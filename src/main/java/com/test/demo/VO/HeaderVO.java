package com.test.demo.VO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "head")
@XmlAccessorType(XmlAccessType.FIELD)
public class HeaderVO {

    @XmlAttribute(name = "cs_mine_code")
    private String cs_mine_code;

    @XmlAttribute(name = "cs_data_time")
    private String cs_data_time;

    public String getCs_mine_code() {
        return cs_mine_code;
    }

    public void setCs_mine_code(String cs_mine_code) {
        this.cs_mine_code = cs_mine_code;
    }

    public String getCs_data_time() {
        return cs_data_time;
    }

    public void setCs_data_time(String cs_data_time) {
        this.cs_data_time = cs_data_time;
    }

    @Override
    public String toString() {
        return "HeaderVO{" +
                "cs_mine_code='" + cs_mine_code + '\'' +
                ", cs_data_time=" + cs_data_time +
                '}';
    }
}


