package com.test.demo.VO;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class AQSSRootVO {

    @XmlAttribute(name = "code")
    private String code;

    @XmlElement(name = "head")
    private HeaderVO head;

    @XmlElement(name = "data")
    private List<AQSSDataVO> dataVOList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HeaderVO getHead() {
        return head;
    }

    public void setHead(HeaderVO head) {
        this.head = head;
    }

    public List<AQSSDataVO> getDataVOList() {
        return dataVOList;
    }

    public void setDataVOList(List<AQSSDataVO> dataVOList) {
        this.dataVOList = dataVOList;
    }

    @Override
    public String toString() {
        return "AQSSRootVO{" +
                "code='" + code + '\'' +
                ", head=" + head +
                ", dataVOList=" + dataVOList +
                '}';
    }
}
