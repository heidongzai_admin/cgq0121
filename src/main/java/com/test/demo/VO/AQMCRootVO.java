package com.test.demo.VO;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class AQMCRootVO {
    @XmlAttribute(name = "code")
    private String code;

    @XmlElement(name = "head")
    private HeaderVO head;

    @XmlElement(name = "data")
    private AQMCDataVO data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HeaderVO getHead() {
        return head;
    }

    public void setHead(HeaderVO head) {
        this.head = head;
    }

    public AQMCDataVO getData() {
        return data;
    }

    public void setData(AQMCDataVO data) {
        this.data = data;
    }

}
