package com.test.demo.VO;
import javax.xml.bind.annotation.*;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class AQBJRootVO {

    @XmlAttribute(name = "code")
    private String code;

    @XmlElement(name = "head")
    private HeaderVO head;

    @XmlElement(name = "data")
    private AQBJDataVO data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HeaderVO getHead() {
        return head;
    }

    public void setHead(HeaderVO head) {
        this.head = head;
    }

    public AQBJDataVO getData() {
        return data;
    }

    public void setData(AQBJDataVO data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "AJBQRootVO{" +
                "code='" + code + '\'' +
                ", head=" + head +
                ", data=" + data +
                '}';
    }
}
