<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">  
<html>
	<head>
		<meta charset="utf-8">
		<title>传感器管理平台</title>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/style.css">
		<!-- <link rel="stylesheet" href=".<%=basePath%>/resources/static/main.css" type="text/css"> -->
		<!-- 引入样式 -->
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/element-index.css">
		<%--<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">--%>
	</head>
	<style>
		.cgq_box{
			position: absolute;
			width: 570px;
			background: #fff;
			margin-top: -3px;
			padding-left: 140px;
			z-index: 20;
			height: 250px;
			overflow: auto;
		}
		.cle{margin:0; padding:0; list-sytle:none!important;}
	</style>
	<body>
		<div id="app" style="width: 100%;display: flex;height: 100%;">
			<el-container>
				<el-header style=" width: 100%;height: 70px;font-size: 22px;">
					<div>传感器管理系统</div>
				</el-header>
				<el-container>
					<el-aside style="width: 202px;height: 100%;">
						<el-menu
								style="max-width: 300px!important;height: 100%;"
								class="el-menu-vertical-demo"
								theme="dark"
								default-active="1"
								unique-opened>
							<!--<li style="color:#999999" class="el-menu-item">
								<a href="<%=basePath%>data/getData" style="color:#999999"><span style="padding-left: 5px;padding-right: 5px;">
								<image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>传感器管理</span></a>
							</li>-->
							<li style="color:#999999" class="el-menu-item">
								<a href="<%=basePath%>data/getSyData" style="color:#999999">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>全时段</span></a>
							</li>
							<li v-if="parttimeMenuSwitch==1" style="color:#409eef" class="el-menu-item">
								<a  href="<%=basePath%>data/getSyDataFsd" style="color:#409eef">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/blue.png"></image></span>
									<span>分时段</span></a>
							</li>
							<li v-if="configMenuSwitch==1" style="color:#999999" class="el-menu-item">
								<a href="<%=basePath%>data/config" style="color:#999999">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>配置管理</span></a>
							</li>
						</el-menu>
					</el-aside>
					<el-container style="width: 100%!important;height: 100%;">
						<el-main style="color: red;">
							<div class="crumbs">
							   <el-breadcrumb>
								   <el-breadcrumb-item>传感器管理</el-breadcrumb-item>
							   </el-breadcrumb>
						   </div>
							<!-- 查询表单 开始 -->
							<div class="background-panel" style="margin-bottom: 10px;">
								<!-- <div class="box-card-title">查询条件</div> -->
                                <el-form
                                    size="mini"
                                    ref="queryForm"
                                    :model="queryForm"
                                    label-width="80px">
                                    <el-row>
                                        <!--<el-col :span="8" style="padding-right: 10px;" class="form-col">
											<el-form-item size="mini" label="类别 " prop="type">
												<el-select
                                                    style="width: 100%;"
                                                    size="mini"
                                                    v-model="queryForm.type">
													<el-option
                                                        v-for="(item, index) in sensorTypeSelectData"
                                                        :label="item.label"
                                                        :value="item.value"
                                                        :key="index">
													</el-option>
												</el-select>
											</el-form-item>
										</el-col>
										<el-col :span="8" style="padding-right: 10px;" class="form-col">
											<el-form-item size="mini" label="矿名" prop="kuangming">
												<el-input

														style="width: 100%;"
														size="mini"
														v-model="queryForm.kuangming"
														placeholder="请输入矿名"
														clearable></el-input>
											</el-form-item>
										</el-col>
										<el-col :span="8" style="padding-right: 0px;" class="form-col">
											<el-form-item size="mini" label="分站" prop="fenzhan">
												<el-input
														style="width: 100%;"
														size="mini"
														v-model="queryForm.fenzhan"
														placeholder="请输入名称"
														clearable></el-input>
											</el-form-item>
										</el-col>-->
										<el-col :span="8" style="padding-right: 10px;" class="form-col">
											<el-form-item size="mini" label="编号" prop="code">
												<el-input


														style="width: 100%;"
													size="mini"
													v-model="queryForm.code"
													placeholder="请输入传感器编号"
													></el-input>
											</el-form-item>
										</el-col>
										<el-col :span="8" style="padding-right: 0px;" class="form-col">
											<el-form-item size="mini" label="名称" prop="name">
												<el-input
													style="width: 100%;"
													size="mini"
													v-model="queryForm.name"
													placeholder="请输入名称"
													clearable></el-input>
											</el-form-item>
										</el-col>

										<el-col :span="8" class="form-col">
											<el-form-item size="mini" style="text-align: right; margin-bottom: 0;">
												<el-button size="mini" type="primary" @click="clear('queryForm')">清空</el-button>
												<el-button size="mini" type="primary" @click="query">查询</el-button>
											</el-form-item>
										</el-col>
									</el-row>
								</el-form>
							</div>

							<div class="background-panel">
                                <div class="box-card-title"
                                    style=" text-align: right;
                                        width: calc(100% - 0px);
                                        height: 28px;
                                        font-size: 14px;
                                        line-height: 28px;
                                        color: #606266;
                                        margin-bottom: 10px;
                                        line-height: 28px;">
									<span style="float: left;">查询列表</span>
<%--									<el-switch--%>
<%--											v-if="db14SwitchOn==1"--%>
<%--											v-model="qsdSwitch"--%>
<%--											active-text=""--%>
<%--											active-value="1"--%>
<%--											inactive-text="DB14开关"--%>
<%--											inactive-value="0"--%>
<%--											active-color="#13ce66"--%>
<%--											inactive-color="#ff4949"--%>
<%--											@change="editSwitch($event)">--%>
<%--									</el-switch>--%>

<%--									<el-switch style="margin-left:30px"--%>
<%--											   v-if="sySwitchOn==1"--%>
<%--											   v-model="fsdSwitch"--%>
<%--											   active-text=""--%>
<%--											   active-value="1"--%>
<%--											   inactive-text="新元开关"--%>
<%--											   inactive-value="0"--%>
<%--											   active-color="#13ce66"--%>
<%--											   inactive-color="#ff4949"--%>
<%--											   @change="editSwitch($event)">--%>
<%--									</el-switch>--%>
<%--									<el-switch style="margin-left:30px"--%>
<%--											   v-if="dbSwitchOn==1"--%>
<%--											   v-model="dbSwitch"--%>
<%--											   active-text=""--%>
<%--											   active-value="1"--%>
<%--											   inactive-text="数据库修改开关"--%>
<%--											   inactive-value="0"--%>
<%--											   active-color="#13ce66"--%>
<%--											   inactive-color="#ff4949"--%>
<%--											   @change="editSwitch($event)">--%>
<%--									</el-switch>--%>
<%--									<el-switch style="margin-left:30px"--%>
<%--											   v-if="mcSwitchOn==1"--%>
<%--											   v-model="mcSwitch"--%>
<%--											   active-text=""--%>
<%--											   active-value="1"--%>
<%--											   inactive-text="密采开关"--%>
<%--											   inactive-value="0"--%>
<%--											   active-color="#13ce66"--%>
<%--											   inactive-color="#ff4949"--%>
<%--											   @change="editSwitch($event)">--%>
<%--									</el-switch>--%>

									<el-switch style="margin-left:30px"
											   v-model="parttimeSwitch"
											   active-text=""
											   active-value="1"
											   inactive-text="分时段开关"
											   inactive-value="0"
											   active-color="#13ce66"
											   inactive-color="#ff4949"
											   @change="editSwitch($event)">
									</el-switch>
									<el-button size="mini" type="primary" @click="addConfig">设置分时段时间</el-button>
									<el-button size="mini" type="primary" @click="add">新增</el-button>
								</div>
								<el-table
                                    size="mini"
                                    ref="singleTable"
                                    v-loading="tableLoading"
                                    :data="tableData"
                                    style="width: 100%;"
                                    @current-change="handleCurrentChange"
                                    :header-cell-style="getRowClass">
                                        
									<el-table-column
											label="序号"
                                            width="80"
                                            show-overflow-tooltip>
										<template slot-scope="scope">
											{{(scope.$index+1) + (currentPage-1)*pagesize}}
										</template>
                                    </el-table-column>
									<!--<el-table-column
											prop="kuangming"
											label="矿名"
											min-width="90"
											show-overflow-tooltip>
									</el-table-column>
									<el-table-column
											prop="fenzhan"
											label="分站"
											min-width="90"
											show-overflow-tooltip>
									</el-table-column>-->
									<el-table-column
											prop="code"
											label="编号"
											min-width="90"
											show-overflow-tooltip>
									</el-table-column>
									<!--<el-table-column
											prop="sensorType"
											label="类型"
                                            min-width="90"
                                            show-overflow-tooltip>
                                        <template slot-scope="scope">
                                            {{formatSensorTypes(scope.row.type)}}
                                        </template>
                                    </el-table-column>-->
                                    
									<el-table-column
                                        prop="name"
                                        label="名称"
                                        min-width="90"
                                        show-overflow-tooltip>
                                    </el-table-column>



									<el-table-column
                                        label="实际报警值"
                                        min-width="90"
                                        show-overflow-tooltip>
                                        <template slot-scope="scope">
                                            {{scope.row.minAlarm}}
                                            ～
                                            {{scope.row.maxAlarm}}
                                        </template>
                                    </el-table-column>
                                    
									<!--<el-table-column
                                        prop="alarmScope"
                                        label="调教报警范围"
                                        min-width="90"
                                        show-overflow-tooltip>
                                    </el-table-column>-->

									<el-table-column
											fixed="right"
											label="操作"
											width="100">
										<template slot-scope="scope">

                                            <el-button size="mini" type="text"
                                                @click="edit(scope.row)">
                                                修改</el-button>

                                            <el-button size="mini" type="text"
                                                style="color: #F56C6C;"
                                                @click="del(scope.row)">
                                                删除</el-button>

										</template>
                                    </el-table-column>
                                    
								</el-table>
								<div style="text-align: right;width: 100%;">
									<el-pagination
										layout="total, prev, pager, next"
										:current-page="currentPage"
										@current-change="paginationCurrentChange"
										:page-size="pagesize"
										:total="totalPage">
									</el-pagination>
								</div>
							</div>
						</el-main>
					</el-container>
				</el-container>
			</el-container>
			<el-dialog
					:title="addTitleConfig"
					:visible.sync="dialogVisibleConfig"
					width="50%"
					:before-close="closeMsg">
				<el-form :model="formConfig"
						 :rules="rules"
						 ref="addForm"
						 size="mini"
						 label-width="140px">

					<!--<el-form-item label="矿名" prop="kuangming">
						<el-input v-model="form.kuangming"></el-input>
					</el-form-item>
					<el-form-item label="分站" prop="fenzhan">
						<el-input v-model="form.fenzhan"></el-input>
					</el-form-item>-->
					<el-form-item label="分时段开始时间" prop="fsdBeginTime" >
						<el-time-picker
								v-model="formConfig.fsdBeginTime"
								type="datetime"
								:editable="true"
								placeholder="分时段开始时间"
								value-format="HH:mm:ss"
								format="HH:mm:ss"

						></el-time-picker>
						<!--<el-input v-model="form.fsdBeginTime" @change="hideBox" :disabled="true"></el-input>-->
					</el-form-item>
					<el-form-item label="分时段结束时间" prop="fsdEndTime" >
						<el-time-picker
								v-model="formConfig.fsdEndTime"
								type="datetime"
								:editable="true"
								placeholder="分时段结束时间"
								value-format="HH:mm:ss"
								format="HH:mm:ss"
						></el-time-picker>
						<!--<el-input v-model="form.fsdEndTime" @change="hideBox" :disabled="true"></el-input>-->
					</el-form-item>
					<!--<el-form-item label="类型" prop="type">
						<el-select
								v-model="form.type"
								placeholder="请选择"
								style="width: 100%;">
							<el-option
									v-for="item in sensorTypeSelectData"
									:key="item.value"
									:label="item.label"
									:value="item.value">
							</el-option>
						</el-select>
					</el-form-item>-->



					<!--<el-form-item label="调教报警范围" prop="alarmScope">
						<el-input v-model="form.alarmScope"></el-input>
					</el-form-item>
					<el-form-item label="备注" prop="note">
						<el-input v-model="form.note" type="textarea"></el-input>
					</el-form-item>-->
				</el-form>

				<span slot="footer" class="dialog-footer">
					<el-button @click="closeMsg">取 消</el-button>
					<el-button type="primary" @click="editFsdTime">确 定</el-button>
				</span>
			</el-dialog>
            <el-dialog
                :title="addTitle"
                :visible.sync="dialogVisible"
                width="50%"
                :before-close="closeMsg">
				<el-form :model="form"
					:rules="rules"
                    ref="addForm"
					size="mini"
					label-width="140px">

					<!--<el-form-item label="矿名" prop="kuangming">
						<el-input v-model="form.kuangming"></el-input>
					</el-form-item>
					<el-form-item label="分站" prop="fenzhan">
						<el-input v-model="form.fenzhan"></el-input>
					</el-form-item>-->
					<el-form-item label="编号" prop="code" >
						<el-input v-model="form.code" @change="hideBox" :disabled="true"></el-input>
					</el-form-item>

					<!--<el-form-item label="类型" prop="type">
						<el-select
								v-model="form.type"
								placeholder="请选择"
								style="width: 100%;">
							<el-option
									v-for="item in sensorTypeSelectData"
									:key="item.value"
									:label="item.label"
									:value="item.value">
							</el-option>
						</el-select>
					</el-form-item>-->


						<el-form-item label="名称" prop="keyword_">
							<el-input v-model="form.keyword_" @focus="showBoxM" @input="fSearchM" ></el-input>
						</el-form-item>

<%--						<ul class="cgq_box" v-show="showBox">--%>
<%--							<li v-for="value in fSearchList" @click="getMenu(value)"  class="cle">--%>
<%--								<div ><div style="width:200px;margin:auto;padding-left: 20px;display:inline-block">{{value.addrname}}</div>--%>
<%--									<div style="width:200px;margin:auto;padding-left: 20px;display:inline-block">{{value.UniqueID}}</div></div>--%>
<%--							</li>--%>
<%--						</ul>--%>


					<ul class="cgq_box" v-show="showBox">
						<li   class="cle">
							<div ><div style="width:200px;margin:auto;padding-left: 20px;display:inline-block">地址</div>
								<div style="width:100px;margin:auto;padding-left: 20px;display:inline-block">点号</div>
								<div style="width:150px;margin:auto;padding-left: 20px;display:inline-block">编号</div></div>
						</li>
						<li v-for="value in fSearchList" @click="getMenu(value)"  class="cle">
							<div ><div style="width:200px;margin:auto;padding-left: 20px;display:inline-block">{{value.addrname}}</div>
								<div style="width:100px;margin:auto;padding-left: 20px;display:inline-block">{{value.db14Code}}</div>
								<div style="width:150px;margin:auto;padding-left: 20px;display:inline-block">{{value.UniqueID}}</div>
							</div>
						</li>
					</ul>


					<el-form-item label="最小实际报警值" prop="minAlarm">
						<el-input v-model="form.minAlarm" min="0" type="number"></el-input>
					</el-form-item>
					<el-form-item label="最大实际报警值" prop="maxAlarm">
						<el-input v-model="form.maxAlarm" type="number"></el-input>
					</el-form-item>
					<!--<el-form-item label="调教报警范围" prop="alarmScope">
						<el-input v-model="form.alarmScope"></el-input>
					</el-form-item>
					<el-form-item label="备注" prop="note">
						<el-input v-model="form.note" type="textarea"></el-input>
					</el-form-item>-->
				</el-form>

				<span slot="footer" class="dialog-footer">
					<el-button @click="closeMsg">取 消</el-button>
					<el-button type="primary" @click="save">确 定</el-button>
				</span>
			</el-dialog>


		</div>
	</body>

  	<script  type="text/javascript"  src="<%=basePath%>/resources/static/js/vue.js"></script>
	<!-- 引入组件库 -->
	<script src="<%=basePath%>/resources/static/js/axios.min.js"></script>
	<script src="<%=basePath%>/resources/static/js/element-index.js"></script>

	<script>
        var vue = new Vue({
            el: '#app',
            data() {
                return {
					fSearchList: [],
					showBox : false,
					cgqList : ['百度', '百度翻译', '百度地图', '百度网盘', '百度新闻'],
					meun:[
						{
							index: "1",
							icon: "el-icon-menu",
							name: "传感器管理",
							href: "<%=basePath%>data/getData",
						},
						{
							index: "2",
							icon: "el-icon-menu",
							name: "传感器管理2",
							href: "<%=basePath%>data/getSyData",
						}
					],
					formConfig: {

						fsdBeginTime: '',
						fsdEndTime: '',
					},
					tConfig: {},
					fsdSwitch: '',
					qsdSwitch: '',
					fsdBeginTime: '',
					fsdEndTime: '',
					mcSwitchOn: '',
					dbSwitchOn: '',
					sySwitchOn: '',
					db14SwitchOn: '',
					mcSwitch: '',
					dbSwitch: '',
					alltimeSwitch: '',
					parttimeMenuSwitch: '',
                    tableLoading: false, // 表单loding
                    tableData: [],
                    currentPage: 1, // 当前页码
                    pagesize:10,    // 每页的数据
                    totalPage: 0, // 总条数
                    getSelectRow: "", // 获取表格中选中的数据
                    getIndex: "", // 获取选中的index
                    queryForm: { // 查询表单
                        code: "",
                        name: "",
						fenzhan: "",
                        kuangming: "",
						type: "",
					},
                    dialogVisible: false,
                    addTitle: '添加',
					dialogVisibleConfig: false,
					addTitleConfig: '设置',
                    sensorTypeSelectData: [
                        { label: '低浓度甲烷', value: '1' },
                        { label: '高低浓度甲烷', value: '2' },
                        { label: '高浓度甲烷', value: '7' },
                        { label: '风速', value: '3' },
                        { label: 'CO', value: '4' },
                        { label: '温度', value: '5' },
                        { label: '水位', value: '23' },
						{ label: '粉尘', value: '37' },
						{ label: '负压', value: '39' },
                    ],
                    form: {
                        id: '',
                        code: '',
                        fenzhan: '',
                        kuangming: '',
						name: '',
                        type: '',
                        minAlarm: '',
                        maxAlarm: '',
                        alarmScope: '',
                        note: '',
						keyword_: '',
						uniqueId: '',
                    },
                    rules: {
                        type: [
                            {required: true, message: '请选择传感器类型', trigger: 'change'},
                        ],
                        code: [
                            {required: true, message: '请输入编号', trigger: 'blur'},
                        ],
						fsdBeginTime: [
							{required: true, message: '请选择开始时间', trigger: 'blur'},
						],
						fsdEndTime: [
							{required: true, message: '请选择结束时间', trigger: 'blur'},
						],
						keyword_: [
                            {required: true, message: '请输入名称1', trigger: 'change'},
                        ],
                        kuangming: [
                            {required: true, message: '请输入矿名', trigger: 'blur'},
                        ],
						fenzhan: [
							{required: true, message: '请输入分站', trigger: 'blur'},
						],
                        minAlarm: [
                            {required: true, message: '请输入最小值', trigger: 'blur'},
                        ],
                        maxAlarm: [
                            {required: true, message: '请输入最大值', trigger: 'blur'},
                        ],
                    },
					focusMark: false,
                    fileUpdate: '00',
					fileUpdateFsd: ''
            	}
            },
            created() {

                this.query();
				this.getCgqList();
				this.getConfig();
            },
			computed : {  //设置计算属性
				// fSearch(){
				// 	let that=this;
				//
				// 	if(that.form.keyword_){
				// 		// that.showBox=true;
				//
				// 		this.fSearchList=this.cgqList.filter((value)=>{  //过滤数组元素
				// 				 return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
				// 			});
				// 		// return this.cgqList.filter((value)=>{  //过滤数组元素
				// 		// 	 return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
				// 		// });
				// 	}
				// }
			},
            methods: {
				fSearchM(){
					let that=this;

					if(that.form.keyword_){
						// that.showBox=true;

						that.fSearchList=that.cgqList.filter((value)=>{  //过滤数组元素
							return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
						});
						// return this.cgqList.filter((value)=>{  //过滤数组元素
						// 	 return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
						// });
					}else{
						that.fSearchList=that.cgqList;
					}
				},
				hideBox: function(){

				this.showBox=false;
				},
				showBoxM: function(){
					
					// if(this.showBox==true){
					// 	this.showBox=false;
					// }
					// if(this.showBox==false){
					// 	this.showBox=true;
					// }
					this.showBox=true;
					this.focusMark=true;
					let that=this;
					if(that.form.keyword_){
						that.fSearchList=that.cgqList.filter((value)=>{  //过滤数组元素
							return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
						});
					}

				},
				hideBoxM: function(){
					this.showBox=false;

				},
				getConfig: function(){
					let that=this;

					axios.get('<%=basePath%>/tConfig/queryDetail').then(res=> {
						return new Promise((success, error)=> {

							// var newRes = res.data
							// if(!newRes || !newRes.code) { error('响应错误'); return; }
							// if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							that.tConfig=res.data.data;
							that.qsdSwitch=that.tConfig.qsdSwitch;
							that.fsdSwitch=that.tConfig.fsdSwitch;
							that.fsdEndTime=that.tConfig.fsdEndTime;
							that.fsdBeginTime=that.tConfig.fsdBeginTime;
							that.dbSwitch=that.tConfig.dbSwitch;
							that.mcSwitch=that.tConfig.mcSwitch;
							that.mcSwitchOn=that.tConfig.mcSwitchOn;
							that.dbSwitchOn=that.tConfig.dbSwitchOn;
							that.sySwitchOn=that.tConfig.sySwitchOn;
							that.db14SwitchOn=that.tConfig.db14SwitchOn;
							that.parttimeSwitch=that.tConfig.parttimeSwitch;
							that.parttimeMenuSwitch=that.tConfig.parttimeMenuSwitch;
							that.configMenuSwitch=that.tConfig.configMenuSwitch;

							that.formConfig.fsdEndTime=that.tConfig.fsdEndTime;
							that.formConfig.fsdBeginTime=that.tConfig.fsdBeginTime;
							// this.$message.success('操作成功');
						});
					}).catch(error=>{

						console.error(error);
						this.$message.error(error);
					});
				},
				editFsdTime: function(){
					let that=this;
					var params = {
						fsdBeginTime: this.formConfig.fsdBeginTime,
						fsdEndTime: this.formConfig.fsdEndTime,
						fsdSwitch: this.fsdSwitch,
						qsdSwitch: this.qsdSwitch,
						id: '1'
					}
					
					axios.post('<%=basePath%>/tConfig/addConfig',params).then(res=> {
						return new Promise((success, error)=> {
							if(res.status==200){
								this.$message.success('操作成功');
								this.close();
								this.query();
								this.getConfig();
							}
							//var newRes = res.data
							//if(!newRes || !newRes.code) { error('响应错误'); return; }
							//if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							// if(newRes.data){ error('查询结果为空'); return; }
							// that.tConfig=res.data.data;
							// that.qsdSwitch=that.tConfig.qsdSwitch;
							// that.fsdSwitch=that.tConfig.fsdSwitch;
							// that.formConfig.fsdEndTime=that.tConfig.fsdEndTime;
							// that.formConfig.fsdBeginTime=that.tConfig.fsdBeginTime;


						});
					}).catch(error=>{
						if(this.fileUpdate == '0') {
							this.fileUpdate = '1';
						}else if(this.fileUpdate == '1') {
							this.fileUpdate = '0';
						}
						console.error(error);
						this.$message.error(error);
					});
				},
				editSwitch: function($event){
					
					let that=this;
					var params = {
						fsdSwitch: this.fsdSwitch,
						fsdBeginTime: this.formConfig.fsdBeginTime,
						fsdEndTime: this.formConfig.fsdEndTime,
						qsdSwitch: this.qsdSwitch,
						parttimeSwitch: this.parttimeSwitch,
						id: '1'
					}
					axios.post('<%=basePath%>/tConfig/addConfig',params).then(res=> {
						return new Promise((success, error)=> {
							if(res.status==200){
								this.$message.success('操作成功');
								this.close();
								this.query();
								this.getConfig();
							}
							// var newRes = res.data
							// if(!newRes || !newRes.code) { error('响应错误'); return; }
							// if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							// that.tConfig=res.data;
							// that.qsdSwitch=that.tConfig.qsdSwitch;
							// that.fsdSwitch=that.tConfig.fsdSwitch;
							// that.formConfig.fsdEndTime=that.tConfig.fsdEndTime;
							// that.formConfig.fsdBeginTime=that.tConfig.fsdBeginTime;

							// this.$message.success('操作成功');
						});
					}).catch(error=>{
						// if(this.fileUpdate == '00') {
						// 	this.fileUpdate = '01';
						// }else if(this.fileUpdate == '01') {
						// 	this.fileUpdate = '00';
						// }
						console.error(error);
						this.$message.error(error);
					});
				},
				getCgqList: function(){
					let that=this;

					axios.get('<%=basePath%>/sySensor/getCgqList').then(res=> {
						return new Promise((success, error)=> {

							// var newRes = res.data
							// if(!newRes || !newRes.code) { error('响应错误'); return; }
							// if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							that.cgqList=res.data;
							that.fSearchList=res.data;
							// this.$message.success('操作成功');
						});
					}).catch(error=>{
						if(this.fileUpdate == '00') {
							this.fileUpdate = '01';
						}else if(this.fileUpdate == '01') {
							this.fileUpdate = '00';
						}
						console.error(error);
						this.$message.error(error);
					});
				},
				getMenu: function(value){
					
					this.form.keyword_=value.addrname;
					this.form.code=value.db14Code;
					this.form.name=value.addrname;
					this.form.uniqueId=value.UniqueID;
					this.showBox=false;

				},


            	fileUpdateChange(val) {
            		axios.get('<%=basePath%>/sySensor/switchThread?switchType='
                    +val).then(res=> {
                        return new Promise((success, error)=> {
                            var newRes = res.data
                            if(!newRes || !newRes.code) { error('响应错误'); return; }
                            if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                            this.$message.success('操作成功');
                        });
                    }).catch(error=>{
                        if(this.fileUpdate == '00') {
                            this.fileUpdate = '01';
						}else if(this.fileUpdate == '01') {
                            this.fileUpdate = '00';
						}
                        console.error(error);
                        this.$message.error(error);
                    });
            	},
                // 清空条件查询
                clear(queryForm) {
                    this.$refs[queryForm].resetFields();
                },
                // 查询
                query() {
                    var queryFrom =this.queryForm;

                    var params = {
                        pn: this.currentPage,
                        ps: this.pagesize,
						type: queryFrom.type,
                        code: queryFrom.code,
                        name: queryFrom.name,
                        kuangming: queryFrom.kuangming,
						fenzhan: queryFrom.fenzhan,
                    }
                    axios.post('<%=basePath%>/sySensorFsd/querySensorList',
                    params).then(res=> {
                        return new Promise((success, error)=> {
                            var newRes = res.data
if("inValid"==newRes){
								window.location.replace("<%=basePath%>data/inValid")
							}
                            if(!newRes || !newRes.code) { error('响应错误'); return; }
                            if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }

                            this.tableData = newRes.list;
                            this.totalPage = newRes.total;
                        });
                    }).catch(error=>{ console.error(error); this.$message.error(error); });
				},
                // 分页
                handleCurrentChange(val) {
                    this.getSelectRow = val;
                    this.query();
                },
                add(){
                    this.addTitle = '添加';
                    this.dialogVisible = true;
                },
				addConfig(row) {
					this.addTitleConfig = '修改';
					// this.form = JSON.parse(JSON.stringify(row));
					// this.form.keyword_=this.form.name;
					this.dialogVisibleConfig = true;
				},
                edit(row) {
                    this.addTitle = '修改';
                    this.form = JSON.parse(JSON.stringify(row));
					this.form.keyword_=this.form.name;
                    this.dialogVisible = true;
                },
                del(row) {

                    this.$confirm('确认删除?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        axios.get('<%=basePath%>/sySensorFsd/deleteSensorInfo?sensorId='
                        +row.id).then(res=> {

                            return new Promise((success, error)=> {
                                var newRes = res.data
                                if(!newRes || !newRes.code) { error('响应错误'); return; }
                                if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                                this.$message.success('操作成功');
                                // this.close();

                                this.query();
                            });
                        }).catch(error=>{ console.error(error); this.$message.error(error); });
                    }).catch(() => {  });
                },
                tableRowClassName ({row, rowIndex}) {
                    //把每一行的索引放进row
                    row.index = rowIndex;
                },
                // 分页切换
                paginationCurrentChange(val) {
                    this.currentPage = val;
                    this.query()
                },
                save(){
                    this.$refs['addForm'].validate((valid) => {

                        if (valid) {

                            var params = JSON.parse(JSON.stringify(this.form));
							delete params.keyword_;

                            axios.post('<%=basePath%>/sySensorFsd/addSensorInfo', params).then(res=> {
                                return new Promise((success, error)=> {
                                    var newRes = res.data
                                    if(!newRes || !newRes.code) { error('响应错误'); return; }
                                    if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                                    this.$message.success('操作成功');
                                    this.close();
                                    this.query();
									this.getConfig();
                                });
                            }).catch(error=>{ console.error(error); this.$message.error(error); });
                        }
                    });
                },
                formatSensorTypes(val) {
                    var obj = this.sensorTypeSelectData.find(item=> {
                        return item.value == val;
                    });
                    if(obj) { return obj.label; }
                },
                closeMsg() {
                    this.$confirm('确认取消操作?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        this.fSearchList=this.cgqList;
                        this.close();
                    }).catch(() => {  });
                },
                close() {
                    this.form = {
                        id: '',
						kuangming: '',
						fenzhan: '',
						code: '',
						name: '',
						minAlarm: '',
						maxAlarm: '',
						alarmScope: '',
						note: ''
                    };
                    this.$refs['addForm'].resetFields();
                    this.dialogVisible = false;
					this.dialogVisibleConfig = false;
                },
                getRowClass({ row, column, rowIndex, columnIndex }) {
                    if (rowIndex == 0) { return 'background: #ebeef5'; }
                    else { return ''; }
                },
            }
        });
	</script>


</html>