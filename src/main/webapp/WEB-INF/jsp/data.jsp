<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">  
<html>
<script  type="text/javascript"  src="<%=basePath%>/resources/static/js/vue.js"></script>
<!-- 引入组件库 -->
<script src="<%=basePath%>/resources/static/js/axios.min.js"></script>
<script src="<%=basePath%>/resources/static/js/element-index.js"></script>
	<head>
		<meta charset="utf-8">
		<title>传感器管理平台</title>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/style.css">
		<!-- <link rel="stylesheet" href="<%=basePath%>/resources/static/main.css" type="text/css"> -->
		<!-- 引入样式 -->
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/element-index.css">
		<%--<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">--%>
	</head>
	<body>
		<div id="app" style="width: 100%;display: flex;height: 100%;">
			<el-container>
				<el-header style=" width: 100%;height: 70px;font-size: 22px;">
					<div>传感器管理系统</div>
				</el-header>
				<el-container>
					<el-aside style="width: 202px;height: 100%;">
						<el-menu
							style="max-width: 200px!important;height: 100%;"
							class="el-menu-vertical-demo"
                            theme="dark"
                            default-active="1"
							unique-opened>

							<li style="color:#409eef" class="el-menu-item">
								<a href="<%=basePath%>data/getData" style="color:#409eef"><span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/blue.png"></image></span>
									<span>传感器管理</span></a>
							</li>
							<li style="color:#999999" class="el-menu-item">
								<a href="<%=basePath%>data/getSyData" style="color:#999999"><span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>传感器管理2</span></a>
							</li>

							<li style="color:#999999" class="el-menu-item">
								<a href="<%=basePath%>data/getSyData" style="color:#999999">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>全时段</span></a>
							</li>
							<li style="color:#409eef" class="el-menu-item">
								<a href="<%=basePath%>data/getSyDataFsd" style="color:#409eef">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/blue.png"></image></span>
									<span>分时段</span></a>
							</li>
							<li style="color:#409eef" class="el-menu-item">
								<a href="<%=basePath%>data/config" style="color:#409eef">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/blue.png"></image></span>
									<span>配置管理</span></a>
							</li>
						</el-menu>
					</el-aside>
					<el-container style="width: 100%!important;height: 100%;">
						<el-main style="color: red;">
							<div class="crumbs">
							   <el-breadcrumb>
								   <el-breadcrumb-item>传感器管理</el-breadcrumb-item>
							   </el-breadcrumb>
						   </div>
							<!-- 查询表单 开始 -->
							<div class="background-panel" style="margin-bottom: 10px;">
								<!-- <div class="box-card-title">查询条件</div> -->
                                <el-form
                                    size="mini"
                                    ref="queryForm"
                                    :model="queryForm"
                                    label-width="80px">
                                    <el-row>
                                        <el-col :span="8" style="padding-right: 10px;" class="form-col">
											<el-form-item size="mini" label="类别 " prop="sensorType">
												<el-select
                                                    style="width: 100%;"
                                                    size="mini"
                                                    v-model="queryForm.sensorType">
													<el-option
                                                        v-for="(item, index) in sensorTypeSelectData"
                                                        :label="item.label"
                                                        :value="item.value"
                                                        :key="index">
													</el-option>
												</el-select>
											</el-form-item>
										</el-col>
										<el-col :span="8" style="padding-right: 10px;" class="form-col">
											<el-form-item size="mini" label="编号" prop="sensorNo">
												<el-input
													style="width: 100%;"
													size="mini"
													v-model="queryForm.sensorNo"
													placeholder="请输入传感器编号"
													clearable></el-input>
											</el-form-item>
										</el-col>
										<el-col :span="8" style="padding-right: 0px;" class="form-col">
											<el-form-item size="mini" label="Xml中编号" prop="sensorXmlNo">
												<el-input
													style="width: 100%;"
													size="mini"
													v-model="queryForm.sensorXmlNo"
													placeholder="请输入Xml中编号"
													clearable></el-input>
											</el-form-item>
										</el-col>
										<el-col :span="8" style="padding-right: 10px;" class="form-col">
											<el-form-item size="mini" label="位置" prop="sensorLocation">
												<el-input
														style="width: 100%;"
														size="mini"
														v-model="queryForm.sensorLocation"
														placeholder="请输入位置"
														clearable></el-input>
											</el-form-item>
										</el-col>
										<el-col :span="16" class="form-col">
											<el-form-item size="mini" style="text-align: right; margin-bottom: 0;">
												<el-button size="mini" type="primary" @click="clear('queryForm')">清空</el-button>
												<el-button size="mini" type="primary" @click="query">查询</el-button>
											</el-form-item>
										</el-col>
									</el-row>
								</el-form>
							</div>

							<div class="background-panel">
                                <div class="box-card-title"
                                    style=" text-align: right;
                                        width: calc(100% - 0px);
                                        height: 28px;
                                        font-size: 14px;
                                        line-height: 28px;
                                        color: #606266;
                                        margin-bottom: 10px;
                                        line-height: 28px;">
									<span style="float: left;">查询列表</span>
									<el-switch
										  v-model="fileUpdate"
										  active-text="开启文件修改"
										  active-value="01"
										  inactive-text="关闭文件修改"
										  inactive-value="00"
										  active-color="#13ce66"
										  inactive-color="#ff4949"
										  @change="fileUpdateChange">
										</el-switch>
									<el-button size="mini" type="primary" @click="add">新增</el-button>
								</div>
								<el-table
                                    size="mini"
                                    ref="singleTable"
                                    v-loading="tableLoading"
                                    :data="tableData"
                                    style="width: 100%;"
                                    @current-change="handleCurrentChange"
                                    :header-cell-style="getRowClass">
                                        
									<el-table-column
											label="序号"
                                            width="80"
                                            show-overflow-tooltip>
										<template slot-scope="scope">
											{{(scope.$index+1) + (currentPage-1)*pagesize}}
										</template>
                                    </el-table-column>
                                    
									<el-table-column
											prop="sensorType"
											label="类型"
                                            min-width="90"
                                            show-overflow-tooltip>
                                        <template slot-scope="scope">
                                            {{formatSensorTypes(scope.row.sensorType)}}
                                        </template>
                                    </el-table-column>
                                    
									<el-table-column
                                        prop="sensorNo"
                                        label="编号"
                                        min-width="90"
                                        show-overflow-tooltip>
                                    </el-table-column>

									<el-table-column
                                        prop="sensorXmlNo"
                                        label="xml中编号"
                                        min-width="90"
                                        show-overflow-tooltip>
                                    </el-table-column>

									<el-table-column
                                        prop="sensorLocation"
                                        label="位置"
                                        min-width="90"
                                        show-overflow-tooltip>
									</el-table-column>

									<el-table-column
                                        label="实际报警值"
                                        min-width="90"
                                        show-overflow-tooltip>
                                        <template slot-scope="scope">
                                            {{scope.row.minAlarm}}
                                            ～
                                            {{scope.row.maxAlarm}}
                                        </template>
                                    </el-table-column>
                                    
									<el-table-column
                                        prop="alarmScope"
                                        label="调教报警范围"
                                        min-width="90"
                                        show-overflow-tooltip>
                                    </el-table-column>

									<el-table-column
											fixed="right"
											label="操作"
											width="100">
										<template slot-scope="scope">

                                            <el-button size="mini" type="text"
                                                @click="edit(scope.row)">
                                                修改</el-button>

                                            <el-button size="mini" type="text"
                                                style="color: #F56C6C;"
                                                @click="del(scope.row)">
                                                删除</el-button>

										</template>
                                    </el-table-column>
                                    
								</el-table>
								<div style="text-align: right;width: 100%;">
									<el-pagination
										layout="total, prev, pager, next"
										:current-page="currentPage"
										@current-change="paginationCurrentChange"
										:page-size="pagesize"
										:total="totalPage">
									</el-pagination>
								</div>
							</div>
						</el-main>
					</el-container>
				</el-container>
			</el-container>

            <el-dialog
                :title="addTitle"
                :visible.sync="dialogVisible"
                width="50%"
                :before-close="closeMsg">
				<el-form :model="form"
					:rules="rules"
                    ref="addForm"
					size="mini"
					label-width="140px">
					<el-form-item label="编号" prop="sensorNo">
						<el-input v-model="form.sensorNo"></el-input>
					</el-form-item>
					<el-form-item label="xml中的编码" prop="sensorXmlNo">
						<el-input v-model="form.sensorXmlNo"></el-input>
					</el-form-item>
					<el-form-item label="位置" prop="sensorLocation">
						<el-input v-model="form.sensorLocation"></el-input>
					</el-form-item>
					<el-form-item label="传感器类型" prop="sensorType">
						<el-select
								v-model="form.sensorType"
								placeholder="请选择"
								style="width: 100%;">
							<el-option
									v-for="item in sensorTypeSelectData"
									:key="item.value"
									:label="item.label"
									:value="item.value">
							</el-option>
						</el-select>
					</el-form-item>
					<el-form-item label="最小实际报警值" prop="minAlarm">
						<el-input v-model="form.minAlarm" min="0" type="number"></el-input>
					</el-form-item>
					<el-form-item label="最大实际报警值" prop="maxAlarm">
						<el-input v-model="form.maxAlarm" type="number"></el-input>
					</el-form-item>
					<el-form-item label="调教报警范围" prop="alarmScope">
						<el-input v-model="form.alarmScope"></el-input>
					</el-form-item>
					<el-form-item label="备注" prop="note">
						<el-input v-model="form.note" type="textarea"></el-input>
					</el-form-item>
				</el-form>

				<span slot="footer" class="dialog-footer">
					<el-button @click="closeMsg">取 消</el-button>
					<el-button type="primary" @click="save">确 定</el-button>
				</span>
			</el-dialog>


		</div>
	</body>



	<script>
        var vue = new Vue({
            el: '#app',
            data() {
                return {
                    meun:[
                        {
                            index: "1",
                            icon: "el-icon-menu",
                            name: "传感器管理",
							href: "<%=basePath%>data/getData",
                        },
						{
							index: "2",
							icon: "el-icon-menu",
							name: "传感器管理2",
							href: "<%=basePath%>data/getSyData",
						}
                    ],
                    tableLoading: false, // 表单loding
                    tableData: [],
                    currentPage: 1, // 当前页码
                    pagesize:10,    // 每页的数据
                    totalPage: 0, // 总条数
                    getSelectRow: "", // 获取表格中选中的数据
                    getIndex: "", // 获取选中的index
                    queryForm: { // 查询表单
                        sensorType: "",
                        sensorNo: "",
						sensorXmlNo: "",
                        sensorLocation: "",
					},
                    dialogVisible: false,
                    addTitle: '添加',
                    sensorTypeSelectData: [
                        { label: '甲烷传感器', value: '01' },
                        { label: '二氧化碳传感器', value: '02' },
                        { label: '氧化碳传感器', value: '03' },
                        { label: '风速传感器', value: '04' },
                        { label: '风压传感器', value: '05' },
                        { label: '温度传感器', value: '06' },
                        { label: '氧气传感器', value: '07' },
                    ],
                    form: {
                        id: '',
                        sensorNo: '',
                        sensorXmlNo: '',
                        sensorLocation: '',
                        sensorType: '',
                        minAlarm: '',
                        maxAlarm: '',
                        alarmScope: '',
                        note: ''
                    },
                    rules: {
                        sensorType: [
                            {required: true, message: '请选择传感器类型', trigger: 'change'},
                        ],
                        sensorNo: [
                            {required: true, message: '请输入编号', trigger: 'blur'},
                        ],
						sensorXmlNo: [
                            {required: true, message: '请输入XML中的编号', trigger: 'blur'},
                        ],
                        sensorLocation: [
                            {required: true, message: '请输入所在位置', trigger: 'blur'},
                        ],
                        minAlarm: [
                            {required: true, message: '请输入最小值', trigger: 'blur'},
                        ],
                        maxAlarm: [
                            {required: true, message: '请输入最大值', trigger: 'blur'},
                        ],
                    },
                    fileUpdate: '00',
            	}
            },
            created() {
                this.query();
            },
            methods: {
            	fileUpdateChange(val) {
            		axios.get('<%=basePath%>/sensor/switchThread?switchType='
                    +val).then(res=> {
                        return new Promise((success, error)=> {
                            var newRes = res.data
                            if(!newRes || !newRes.code) { error('响应错误'); return; }
                            if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                            this.$message.success('操作成功');
                        });
                    }).catch(error=>{
                        if(this.fileUpdate == '00') {
                            this.fileUpdate = '01';
						}else if(this.fileUpdate == '01') {
                            this.fileUpdate = '00';
						}
                        console.error(error);
                        this.$message.error(error);
                    });
            	},
                // 清空条件查询
                clear(queryForm) {
                    this.$refs[queryForm].resetFields();
                },
                // 查询
                query() {
                    var queryFrom =this.queryForm;

                    var params = {
                        pn: this.currentPage,
                        ps: this.pagesize,
                        sensorType: queryFrom.sensorType,
                        sensorNo: queryFrom.sensorNo,
                        sensorXmlNo: queryFrom.sensorXmlNo,
                        sensorLocation: queryFrom.sensorLocation,
                    }
                    axios.post('<%=basePath%>/sensor/querySensorList',
                    params).then(res=> {
                        return new Promise((success, error)=> {

                            var newRes = res.data

							if("inValid"==newRes){
								window.location.replace("<%=basePath%>data/inValid")
							}
                            if(!newRes || !newRes.code) { error('响应错误'); return; }
                            if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }

                            this.tableData = newRes.list;
                            this.totalPage = newRes.total;
                        });
                    }).catch(error=>{ console.error(error); this.$message.error(error); });
				},
                // 分页
                handleCurrentChange(val) {
                    this.getSelectRow = val;
                    this.query();
                },
                add(){
                    this.addTitle = '添加';
                    this.dialogVisible = true;
                },
                edit(row) {
                    this.addTitle = '修改';
                    this.form = JSON.parse(JSON.stringify(row));
                    this.dialogVisible = true;
                },
                del(row) {
                    this.$confirm('确认删除?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        axios.get('<%=basePath%>/sensor/deleteSensorInfo?sensorId='
                        +row.id).then(res=> {
                            return new Promise((success, error)=> {
                                var newRes = res.data
                                if(!newRes || !newRes.code) { error('响应错误'); return; }
                                if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                                this.$message.success('操作成功');
                                this.close();

                                this.query();
                            });
                        }).catch(error=>{ console.error(error); this.$message.error(error); });
                    }).catch(() => {  });
                },
                tableRowClassName ({row, rowIndex}) {
                    //把每一行的索引放进row
                    row.index = rowIndex;
                },
                // 分页切换
                paginationCurrentChange(val) {
                    this.currentPage = val;
                    this.query()
                },
                save(){
                    this.$refs['addForm'].validate((valid) => {
                        if (valid) {
                            var params = JSON.parse(JSON.stringify(this.form));
                            axios.post('<%=basePath%>/sensor/addSensorInfo', params).then(res=> {
                                return new Promise((success, error)=> {
                                    var newRes = res.data
                                    if(!newRes || !newRes.code) { error('响应错误'); return; }
                                    if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                                    this.$message.success('操作成功');
                                    this.close();
                                    this.query();
                                });
                            }).catch(error=>{ console.error(error); this.$message.error(error); });
                        }
                    });
                },
                formatSensorTypes(val) {
                    var obj = this.sensorTypeSelectData.find(item=> {
                        return item.value == val;
                    });
                    if(obj) { return obj.label; }
                },
                closeMsg() {
                    this.$confirm('确认取消操作?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        this.close();
                    }).catch(() => {  });
                },
                close() {
                    this.form = {
                        id: '',
                        sensorNo: '',
                        sensorXmlNo: '',
                        sensorLocation: '',
                        sensorType: '',
                        minAlarm: '',
                        maxAlarm: '',
                        alarmScope: '',
                        note: ''
                    };
                    this.$refs['addForm'].resetFields();
                    this.dialogVisible = false;
                },
                getRowClass({ row, column, rowIndex, columnIndex }) {
                    if (rowIndex == 0) { return 'background: #ebeef5'; }
                    else { return ''; }
                },
            }
        });
	</script>
</html>