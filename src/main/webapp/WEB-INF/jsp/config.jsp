<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">  
<html>
	<head>
		<meta charset="utf-8">
		<title>传感器管理平台</title>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/style.css">
		<!-- <link rel="stylesheet" href=".<%=basePath%>/resources/static/main.css" type="text/css"> -->
		<!-- 引入样式 -->
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/element-index.css">
		<%--<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">--%>
	</head>
	<style>
		.cgq_box{
			position: absolute;
			width: 570px;
			background: #fff;
			margin-top: -3px;
			padding-left: 140px;
			z-index: 20;
			height: 250px;
			overflow: auto;
		}
		.cle{margin:0; padding:0; list-sytle:none!important;}
	</style>
	<body>
		<div id="app" style="width: 100%;display: flex;height: 100%;">
			<el-container>
				<el-header style=" width: 100%;height: 70px;font-size: 22px;">
					<div>传感器管理平台</div>
				</el-header>
				<el-container>
					<el-aside style="width: 202px;height: 100%;">
						<el-menu
								style="max-width: 200px!important;height: 100%;"
								class="el-menu-vertical-demo"
								theme="dark"
								default-active="1"
								unique-opened>

							<li style="color:#999999" class="el-menu-item">
								<a href="<%=basePath%>data/getSyData" style="color:#999999">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>全时段</span></a>
							</li>
							<li v-if="parttimeMenuSwitch==1" style="color:#999999" class="el-menu-item">
								<a  href="<%=basePath%>data/getSyDataFsd" style="color:#999999">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/gray.png"></image></span>
									<span>分时段</span></a>
							</li>
							<li v-if="configMenuSwitch==1" style="color:#409eef" class="el-menu-item">
								<a href="<%=basePath%>data/config" style="color:#409eef">
									<span style="padding-left: 5px;padding-right: 5px;"><image src="<%=basePath%>/resources/static/images/blue.png"></image></span>
									<span>配置管理</span></a>
							</li>
						</el-menu>
					</el-aside>
					<el-container style="width: 100%!important;height: 100%;">
						<el-main style="color: red;">
							<div class="crumbs">
							   <el-breadcrumb>
								   <el-breadcrumb-item>配置管理</el-breadcrumb-item>
							   </el-breadcrumb>
						   </div>
							<!-- 查询表单 开始 -->
							<div class="background-panel">





									<el-form :model="form"
											 :rules="rules"
											 ref="addForm"
											 size="mini"
											 label-width="140px">



										<el-form-item label="DB14开关" prop="minAlarm" style="line-height: 50px;
													  height: 50px;">
											<el-switch
													style="margin-left:50px"
													v-if="db14SwitchOn==1"
													v-model="qsdSwitch"
													active-text=""
													active-value="1"
													inactive-text=""
													inactive-value="0"
													active-color="#13ce66"
													inactive-color="#ff4949"
													@change="editSwitch($event)">
											</el-switch>
										</el-form-item>
										<el-form-item label="新元开关" prop="minAlarm" style="line-height: 50px;
													  height: 50px;">
											<el-switch style="margin-left:50px"
													   v-if="sySwitchOn==1"
													   v-model="fsdSwitch"
													   active-text=""
													   active-value="1"
													   inactive-value="0"
													   active-color="#13ce66"
													   inactive-color="#ff4949"
													   @change="editSwitch($event)">
											</el-switch>
										</el-form-item>

										<el-form-item label="db修改开关" prop="minAlarm"
										style="line-height: 50px;
													  height: 50px;"
													  >
											<el-switch style="margin-left:50px"
													   v-if="dbSwitchOn==1"
													   v-model="dbSwitch"
													   active-text=""
													   active-value="1"
													   inactive-value="0"
													   active-color="#13ce66"
													   inactive-color="#ff4949"
													   @change="editSwitch($event)">
											</el-switch>
										</el-form-item>

										<el-form-item label="密采开关" prop="minAlarm" style="line-height: 50px;
													  height: 50px;">
											<el-switch style="margin-left:50px"
													   v-if="mcSwitchOn==1"
													   v-model="mcSwitch"
													   active-text=""
													   active-value="1"

													   inactive-value="0"
													   active-color="#13ce66"
													   inactive-color="#ff4949"
													   @change="editSwitch($event)">
											</el-switch>
										</el-form-item>


									</el-form>


							</div>
						</el-main>
					</el-container>
				</el-container>
			</el-container>
		</div>
	</body>

  	<script  type="text/javascript"  src="<%=basePath%>/resources/static/js/vue.js"></script>
	<!-- 引入组件库 -->
	<script src="<%=basePath%>/resources/static/js/axios.min.js"></script>
	<script src="<%=basePath%>/resources/static/js/element-index.js"></script>

	<script>
        var vue = new Vue({
            el: '#app',
            data() {
                return {
					fSearchList: [],
					showBox : false,
					cgqList : ['百度', '百度翻译', '百度地图', '百度网盘', '百度新闻'],
					meun:[
						{
							index: "1",
							icon: "el-icon-menu",
							name: "传感器管理",
							href: "<%=basePath%>data/getData",
						},
						{
							index: "2",
							icon: "el-icon-menu",
							name: "传感器管理2",
							href: "<%=basePath%>data/getSyData",
						}
					],
					formConfig: {

						fsdBeginTime: '',
						fsdEndTime: '',
					},
					tConfig: {},
					fsdSwitch: '',
					qsdSwitch: '',
					fsdBeginTime: '',
					fsdEndTime: '',
					mcSwitchOn: '',
					dbSwitchOn: '',
					sySwitchOn: '',
					db14SwitchOn: '',
					mcSwitch: '',
					dbSwitch: '',
					alltimeSwitch: '',
					parttimeMenuSwitch: '',
                    tableLoading: false, // 表单loding
                    tableData: [],
                    currentPage: 1, // 当前页码
                    pagesize:10,    // 每页的数据
                    totalPage: 0, // 总条数
                    getSelectRow: "", // 获取表格中选中的数据
                    getIndex: "", // 获取选中的index
                    queryForm: { // 查询表单
                        code: "",
                        name: "",
						fenzhan: "",
                        kuangming: "",
						type: "",
					},
                    dialogVisible: false,
					dialogVisibleConfig: false,
                    addTitle: '添加',
					addTitleConfig: '设置',
                    sensorTypeSelectData: [
                        { label: '低浓度甲烷', value: '1' },
                        { label: '高低浓度甲烷', value: '2' },
                        { label: '高浓度甲烷', value: '7' },
                        { label: '风速', value: '3' },
                        { label: 'CO', value: '4' },
                        { label: '温度', value: '5' },
                        { label: '水位', value: '23' },
						{ label: '粉尘', value: '37' },
						{ label: '负压', value: '39' },
                    ],
                    form: {
                        id: '',
                        code: '',
                        fenzhan: '',
                        kuangming: '',
						name: '',
                        type: '',
                        minAlarm: '',
                        maxAlarm: '',
                        alarmScope: '',
                        note: '',
						keyword_: '',
                    },
                    rules: {
                        type: [
                            {required: true, message: '请选择传感器类型', trigger: 'change'},
                        ],
                        code: [
                            {required: true, message: '请输入编号', trigger: 'blur'},
                        ],
						keyword_: [
                            {required: true, message: '请输入名称1', trigger: 'change'},
                        ],
                        kuangming: [
                            {required: true, message: '请输入矿名', trigger: 'blur'},
                        ],
						fenzhan: [
							{required: true, message: '请输入分站', trigger: 'blur'},
						],
                        minAlarm: [
                            {required: true, message: '请输入最小值', trigger: 'blur'},
                        ],
                        maxAlarm: [
                            {required: true, message: '请输入最大值', trigger: 'blur'},
                        ],
                    },
					focusMark: false,
                    fileUpdate: '00',
					fileUpdateSy: '00',
					fileUpdateFsd: ''
            	}
            },
            created() {

                this.query();
				this.getCgqList();
				this.getConfig();
            },
			computed : {
			},
            methods: {
				fSearchM(){
					let that=this;

					if(that.form.keyword_){
						// that.showBox=true;

						that.fSearchList=that.cgqList.filter((value)=>{  //过滤数组元素
							return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
						});
						// return this.cgqList.filter((value)=>{  //过滤数组元素
						// 	 return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
						// });
					}else{
						that.fSearchList=that.cgqList;
					}
				},
				
				hideBox: function(){

				this.showBox=false;
				},
				showBoxM: function(){
					debugger;
					// if(this.showBox==true){
					// 	this.showBox=false;
					// }
					// if(this.showBox==false){
					// 	this.showBox=true;
					// }
					this.showBox=true;
					this.focusMark=true;
					let that=this;
					if(that.form.keyword_){
						that.fSearchList=that.cgqList.filter((value)=>{  //过滤数组元素
							return value.addrname.includes(that.form.keyword_); //如果包含字符返回true
						});
					}

				},
				hideBoxM: function(){
					this.showBox=false;

				},
				getConfig: function(){
					let that=this;
debugger;
					axios.get('<%=basePath%>/tConfig/queryDetail').then(res=> {
						return new Promise((success, error)=> {

							// var newRes = res.data
							// if(!newRes || !newRes.code) { error('响应错误'); return; }
							// if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							that.tConfig=res.data.data;
							that.qsdSwitch=that.tConfig.qsdSwitch;
							that.fsdSwitch=that.tConfig.fsdSwitch;
							that.fsdEndTime=that.tConfig.fsdEndTime;
							that.fsdBeginTime=that.tConfig.fsdBeginTime;
							that.dbSwitch=that.tConfig.dbSwitch;
							that.mcSwitch=that.tConfig.mcSwitch;
							that.mcSwitchOn=that.tConfig.mcSwitchOn;
							that.dbSwitchOn=that.tConfig.dbSwitchOn;
							that.sySwitchOn=that.tConfig.sySwitchOn;
							that.db14SwitchOn=that.tConfig.db14SwitchOn;
							that.parttimeMenuSwitch=that.tConfig.parttimeMenuSwitch;
							that.configMenuSwitch=that.tConfig.configMenuSwitch;
							debugger;

							// this.$message.success('操作成功');
						});
					}).catch(error=>{

						console.error(error);
						this.$message.error(error);
					});
				},
				editFsdTime: function(){
					let that=this;
					var params = {
						fsdBeginTime: this.fsdBeginTime,
						fsdEndTime: this.fsdEndTime,
						fsdSwitch: this.fsdSwitch,
						qsdSwitch: this.qsdSwitch,
						id: '1'
					}
					debugger;
					axios.post('<%=basePath%>/tConfig/addConfig',params).then(res=> {
						return new Promise((success, error)=> {
							if(res.status==200){
								this.$message.success('操作成功');
								this.close();
								this.query();
							}
						});
					}).catch(error=>{
						if(this.fileUpdate == '0') {
							this.fileUpdate = '1';
						}else if(this.fileUpdate == '1') {
							this.fileUpdate = '0';
						}
						console.error(error);
						this.$message.error(error);
					});
				},
				editSwitch: function($event){
					debugger;
					let that=this;
					var params = {
						fsdSwitch: this.fsdSwitch,
						fsdBeginTime: this.fsdBeginTime,
						fsdEndTime: this.fsdEndTime,
						qsdSwitch: this.qsdSwitch,
						dbSwitch: this.dbSwitch,
						mcSwitch: this.mcSwitch,
						mcSwitchOn: this.mcSwitchOn,
						dbSwitchOn: this.dbSwitchOn,
						sySwitchOn: this.sySwitchOn,
						db14SwitchOn: this.db14SwitchOn,
						id: '1'
					}
					axios.post('<%=basePath%>/tConfig/addConfig',params).then(res=> {
						return new Promise((success, error)=> {
							if(res.status==200){
								this.$message.success('操作成功');
								this.close();
								this.query();
							}
							// var newRes = res.data
							// if(!newRes || !newRes.code) { error('响应错误'); return; }
							// if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							// that.tConfig=res.data;
							// that.qsdSwitch=that.tConfig.qsdSwitch;
							// that.fsdSwitch=that.tConfig.fsdSwitch;
							// that.fsdEndTime=that.tConfig.fsdEndTime;
							// that.fsdBeginTime=that.tConfig.fsdBeginTime;

							// this.$message.success('操作成功');
						});
					}).catch(error=>{
						// if(this.fileUpdate == '00') {
						// 	this.fileUpdate = '01';
						// }else if(this.fileUpdate == '01') {
						// 	this.fileUpdate = '00';
						// }
						console.error(error);
						this.$message.error(error);
					});
				},
				getCgqList: function(){
					let that=this;

					axios.get('<%=basePath%>/sySensor/getCgqList').then(res=> {
						return new Promise((success, error)=> {

							// var newRes = res.data
							// if(!newRes || !newRes.code) { error('响应错误'); return; }
							// if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							that.cgqList=res.data;
							that.fSearchList=res.data;
							// this.$message.success('操作成功');
						});
					}).catch(error=>{
						if(this.fileUpdate == '00') {
							this.fileUpdate = '01';
						}else if(this.fileUpdate == '01') {
							this.fileUpdate = '00';
						}
						console.error(error);
						this.$message.error(error);
					});
				},
				getMenu: function(value){
					debugger;
					this.form.keyword_=value.addrname;
					this.form.code=value.db14Code;
					this.form.name=value.addrname;

					this.showBox=false;

				},
            	fileUpdateChange(val) {
            		axios.get('<%=basePath%>/sySensor/switchThread?switchType='
                    +val).then(res=> {
                        return new Promise((success, error)=> {
                            var newRes = res.data
                            if(!newRes || !newRes.code) { error('响应错误'); return; }
                            if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                            this.$message.success('操作成功');
                        });
                    }).catch(error=>{
                        if(this.fileUpdate == '00') {
                            this.fileUpdate = '01';
						}else if(this.fileUpdate == '01') {
                            this.fileUpdate = '00';
						}
                        console.error(error);
                        this.$message.error(error);
                    });
            	},
				fileUpdateChangeSy(val) {
					axios.get('<%=basePath%>/sySensor/switchThread?switchType='
							+val).then(res=> {
						return new Promise((success, error)=> {
							var newRes = res.data
							if(!newRes || !newRes.code) { error('响应错误'); return; }
							if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
							this.$message.success('操作成功');
						});
					}).catch(error=>{
						if(this.fileUpdate == '00') {
							this.fileUpdate = '01';
						}else if(this.fileUpdate == '01') {
							this.fileUpdate = '00';
						}
						console.error(error);
						this.$message.error(error);
					});
				},
                // 清空条件查询
                clear(queryForm) {
                    this.$refs[queryForm].resetFields();
                },
                // 查询
                query() {
                    var queryFrom =this.queryForm;

                    var params = {
                        pn: this.currentPage,
                        ps: this.pagesize,
						type: queryFrom.type,
                        code: queryFrom.code,
                        name: queryFrom.name,
                        kuangming: queryFrom.kuangming,
						fenzhan: queryFrom.fenzhan,
                    }
                    axios.post('<%=basePath%>/tConfig/queryDetail',
                    params).then(res=> {
                        return new Promise((success, error)=> {
                            var newRes = res.data
							if("inValid"==newRes){
								window.location.replace("<%=basePath%>data/inValid")
							}
                            if(!newRes || !newRes.code) { error('响应错误'); return; }
                            if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }

                            this.tableData = newRes.list;
                            this.totalPage = newRes.total;
                        });
                    }).catch(error=>{ console.error(error); this.$message.error(error); });
				},
                // 分页
                handleCurrentChange(val) {
                    this.getSelectRow = val;
                    this.query();
                },
                add(){
                    this.addTitle = '添加';
                    this.dialogVisible = true;
                },
                edit(row) {
                    this.addTitle = '修改';
                    this.form = JSON.parse(JSON.stringify(row));
					this.form.keyword_=this.form.name;
                    this.dialogVisible = true;
                },
                del(row) {

                    this.$confirm('确认删除?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        axios.get('<%=basePath%>/sySensor/deleteSensorInfo?sensorId='
                        +row.id).then(res=> {

                            return new Promise((success, error)=> {
                                var newRes = res.data
                                if(!newRes || !newRes.code) { error('响应错误'); return; }
                                if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                                this.$message.success('操作成功');
                                // this.close();

                                this.query();
                            });
                        }).catch(error=>{ console.error(error); this.$message.error(error); });
                    }).catch(() => {  });
                },
                tableRowClassName ({row, rowIndex}) {
                    //把每一行的索引放进row
                    row.index = rowIndex;
                },
                // 分页切换
                paginationCurrentChange(val) {
                    this.currentPage = val;
                    this.query()
                },
                save(){
                    this.$refs['addForm'].validate((valid) => {
                        if (valid) {
                            var params = JSON.parse(JSON.stringify(this.form));
							delete params.keyword_;

                            axios.post('<%=basePath%>/sySensor/addSensorInfo', params).then(res=> {
                                return new Promise((success, error)=> {
                                    var newRes = res.data
                                    if(!newRes || !newRes.code) { error('响应错误'); return; }
                                    if(newRes.code != '0000') { error(newRes.msg?newRes.msg:'响应错误'); return; }
                                    this.$message.success('操作成功');
                                    this.close();
                                    this.query();
                                });
                            }).catch(error=>{ console.error(error); this.$message.error(error); });
                        }
                    });
                },
                formatSensorTypes(val) {
                    var obj = this.sensorTypeSelectData.find(item=> {
                        return item.value == val;
                    });
                    if(obj) { return obj.label; }
                },
                closeMsg() {
                    this.$confirm('确认取消操作?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        this.fSearchList=this.cgqList;
                        this.close();
                    }).catch(() => {  });
                },
                close() {
                    this.form = {
                        id: '',
						kuangming: '',
						fenzhan: '',
						code: '',
						name: '',
						minAlarm: '',
						maxAlarm: '',
						alarmScope: '',
						note: ''
                    };
                    this.$refs['addForm'].resetFields();
                    this.dialogVisible = false;
                },
                getRowClass({ row, column, rowIndex, columnIndex }) {
                    if (rowIndex == 0) { return 'background: #ebeef5'; }
                    else { return ''; }
                },
            }
        });
	</script>
</html>