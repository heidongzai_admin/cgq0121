<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">  
<html>
<script  type="text/javascript"  src="<%=basePath%>/resources/static/js/vue.js"></script>
<!-- 引入组件库 -->
<script src="<%=basePath%>/resources/static/js/axios.min.js"></script>
<script src="<%=basePath%>/resources/static/js/element-index.js"></script>
	<head>
		<meta charset="utf-8">
		<title>传感器系统</title>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/style.css">
		<!-- <link rel="stylesheet" href="<%=basePath%>/resources/static/main.css" type="text/css"> -->
		<!-- 引入样式 -->
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/resources/static/css/element-index.css">
		<%--<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">--%>
	</head>
	<body>
		<div id="app" style="width: 100%;display: flex;height: 100%;">
			<el-container>
				<el-header style=" width: 100%;height: 70px;font-size: 22px;background-color:#ebeef3!important">

					<div style="float:right">
					<a href="<%=basePath%>data/getSyData"><image style="width:30px;padding-top:20px;padding-right:30px;"
												   src="<%=basePath%>/resources/static/images/ho1me.png"></image></a></div>
					</el-header>
				<div style="display: flex;justify-content: center;align-items: center; top: 35%;
    position: relative;


    /* left: 50%; */
            "><div  style="display: flex;justify-content: center;align-items: center;">
					<image style="height:100px;width:100px" src="<%=basePath%>/resources/static/images/fail.png"></image></div>
					<div style="display: flex;justify-content: center;align-items: center;color:#999999;padding-left:30px">您的授权异常，请联系软件供应商!</div></div>

			</el-container>




		</div>
	</body>



	<script>
        var vue = new Vue({
            el: '#app',
            data() {
                return {
                    meun:[
                        {
                            index: "1",
                            icon: "el-icon-menu",
                            name: "传感器管理",
							href: "<%=basePath%>data/getData",
                        },
						{
							index: "2",
							icon: "el-icon-menu",
							name: "传感器管理2",
							href: "<%=basePath%>data/getSyData",
						}
                    ],
                    tableLoading: false, // 表单loding
                    tableData: [],
                    currentPage: 1, // 当前页码
                    pagesize:10,    // 每页的数据
                    totalPage: 0, // 总条数
                    getSelectRow: "", // 获取表格中选中的数据
                    getIndex: "", // 获取选中的index
                    queryForm: { // 查询表单
                        sensorType: "",
                        sensorNo: "",
						sensorXmlNo: "",
                        sensorLocation: "",
					},
                    dialogVisible: false,
                    addTitle: '添加',
                    sensorTypeSelectData: [
                        { label: '甲烷传感器', value: '01' },
                        { label: '二氧化碳传感器', value: '02' },
                        { label: '氧化碳传感器', value: '03' },
                        { label: '风速传感器', value: '04' },
                        { label: '风压传感器', value: '05' },
                        { label: '温度传感器', value: '06' },
                        { label: '氧气传感器', value: '07' },
                    ],
                    form: {
                        id: '',
                        sensorNo: '',
                        sensorXmlNo: '',
                        sensorLocation: '',
                        sensorType: '',
                        minAlarm: '',
                        maxAlarm: '',
                        alarmScope: '',
                        note: ''
                    },
                    rules: {
                        sensorType: [
                            {required: true, message: '请选择传感器类型', trigger: 'change'},
                        ],
                        sensorNo: [
                            {required: true, message: '请输入编号', trigger: 'blur'},
                        ],
						sensorXmlNo: [
                            {required: true, message: '请输入XML中的编号', trigger: 'blur'},
                        ],
                        sensorLocation: [
                            {required: true, message: '请输入所在位置', trigger: 'blur'},
                        ],
                        minAlarm: [
                            {required: true, message: '请输入最小值', trigger: 'blur'},
                        ],
                        maxAlarm: [
                            {required: true, message: '请输入最大值', trigger: 'blur'},
                        ],
                    },
                    fileUpdate: '00',
            	}
            },
            created() {
                //this.query();
            },
            methods: {

            }
        });
	</script>
</html>