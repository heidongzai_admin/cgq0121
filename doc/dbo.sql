/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : SQL Server
 Source Server Version : 10501600
 Source Host           : localhost:1433
 Source Catalog        : taiyue_mng_db14
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 10501600
 File Encoding         : 65001

 Date: 27/01/2021 23:28:21
*/


-- ----------------------------
-- Table structure for Address
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Address]') AND type IN ('U'))
	DROP TABLE [dbo].[Address]
GO

CREATE TABLE [dbo].[Address] (
  [AddrID] bigint  NOT NULL,
  [AddrCode] varchar(20) COLLATE Chinese_PRC_CI_AS  NULL,
  [AddrName] varchar(200) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Initials] varchar(200) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Describe] varchar(200) COLLATE Chinese_PRC_CI_AS  NULL,
  [IsActual] tinyint DEFAULT ((1)) NOT NULL,
  [TopoCode] varchar(31) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [MaxNum] int DEFAULT ((0)) NULL,
  [OrderNum] int  NULL,
  [DefTime] datetime  NOT NULL,
  [IsUse] tinyint  NOT NULL,
  [DelTime] datetime  NULL,
  [FPTypeID] int  NULL,
  [DataFlag] smallint  NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[Address] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AnalogRunRecord_TMP20210123
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AnalogRunRecord_TMP20210123]') AND type IN ('U'))
	DROP TABLE [dbo].[AnalogRunRecord_TMP20210123]
GO

CREATE TABLE [dbo].[AnalogRunRecord_TMP20210123] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[AnalogRunRecord_TMP20210123] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AnalogRunRecord_TMP20210124
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AnalogRunRecord_TMP20210124]') AND type IN ('U'))
	DROP TABLE [dbo].[AnalogRunRecord_TMP20210124]
GO

CREATE TABLE [dbo].[AnalogRunRecord_TMP20210124] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[AnalogRunRecord_TMP20210124] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AnalogRunRecord_TMP20210127
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AnalogRunRecord_TMP20210127]') AND type IN ('U'))
	DROP TABLE [dbo].[AnalogRunRecord_TMP20210127]
GO

CREATE TABLE [dbo].[AnalogRunRecord_TMP20210127] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[AnalogRunRecord_TMP20210127] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AnalogRunRecord20210127
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AnalogRunRecord20210127]') AND type IN ('U'))
	DROP TABLE [dbo].[AnalogRunRecord20210127]
GO

CREATE TABLE [dbo].[AnalogRunRecord20210127] (
  [ID] bigint  NOT NULL,
  [UniqueID] bigint  NOT NULL,
  [StatusID] int  NOT NULL,
  [SValue] numeric(18,5)  NULL,
  [STime] datetime  NOT NULL,
  [DataFlag] int DEFAULT ((1)) NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL
)
GO

ALTER TABLE [dbo].[AnalogRunRecord20210127] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AStatusRecord_TMP202101
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AStatusRecord_TMP202101]') AND type IN ('U'))
	DROP TABLE [dbo].[AStatusRecord_TMP202101]
GO

CREATE TABLE [dbo].[AStatusRecord_TMP202101] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[AStatusRecord_TMP202101] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for AStatusRecord202101
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[AStatusRecord202101]') AND type IN ('U'))
	DROP TABLE [dbo].[AStatusRecord202101]
GO

CREATE TABLE [dbo].[AStatusRecord202101] (
  [ID] bigint  NOT NULL,
  [CauseRecID] bigint  NOT NULL,
  [UniqueID] bigint  NULL,
  [StatID] int  NOT NULL,
  [BTime] datetime  NOT NULL,
  [ETime] datetime  NOT NULL,
  [MaxValue] numeric(18,5)  NULL,
  [MaxVTime] datetime  NULL,
  [MinValue] numeric(18,5)  NULL,
  [MinVTime] datetime  NULL,
  [AvgValue] numeric(18,5)  NULL,
  [DataFlag] int DEFAULT ((1)) NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[AStatusRecord202101] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for DeviceInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[DeviceInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[DeviceInfo]
GO

CREATE TABLE [dbo].[DeviceInfo] (
  [DevLabel] varchar(20) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [UniqueID] bigint  NOT NULL,
  [RegisterNum] int  NOT NULL,
  [TypeID] int  NOT NULL,
  [AddrID] bigint  NOT NULL,
  [AddrTypeID] bigint  NOT NULL,
  [VZero] int  NULL,
  [SZero] int  NULL,
  [VOne] int  NULL,
  [SOne] int  NULL,
  [VTwo] int  NULL,
  [STwo] int  NULL,
  [UAlarm] numeric(18,5)  NULL,
  [UCut] numeric(18,5)  NULL,
  [UAlarmRer] numeric(18,5)  NULL,
  [UCutRer] numeric(18,5)  NULL,
  [UWring] numeric(18,5)  NULL,
  [DAlarm] numeric(18,5)  NULL,
  [DCut] numeric(18,5)  NULL,
  [DAlarmRer] numeric(18,5)  NULL,
  [DCutRer] numeric(18,5)  NULL,
  [DWring] numeric(18,5)  NULL,
  [FBUniqueID] bigint  NULL,
  [CutArea] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [DefTime] datetime  NOT NULL,
  [ModTime] datetime  NOT NULL,
  [Status] int  NOT NULL,
  [DeleteTime] datetime  NULL,
  [SpecialDes] varchar(300) COLLATE Chinese_PRC_CI_AS  NULL,
  [CalRelationID] bigint  NULL,
  [DataFlag] int DEFAULT ((1)) NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[DeviceInfo] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RunRecord_TMP202101
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RunRecord_TMP202101]') AND type IN ('U'))
	DROP TABLE [dbo].[RunRecord_TMP202101]
GO

CREATE TABLE [dbo].[RunRecord_TMP202101] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[RunRecord_TMP202101] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for RunRecord202101
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[RunRecord202101]') AND type IN ('U'))
	DROP TABLE [dbo].[RunRecord202101]
GO

CREATE TABLE [dbo].[RunRecord202101] (
  [ID] bigint  NOT NULL,
  [UniqueID] bigint  NOT NULL,
  [StatusID] int  NOT NULL,
  [SValue] numeric(18,5)  NULL,
  [SVPosition] numeric(18,5)  NULL,
  [STime] datetime  NOT NULL,
  [DataFlag] int DEFAULT ((1)) NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[RunRecord202101] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneDay_TMP2021
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneDay_TMP2021]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneDay_TMP2021]
GO

CREATE TABLE [dbo].[StaOneDay_TMP2021] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[StaOneDay_TMP2021] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneDay2021
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneDay2021]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneDay2021]
GO

CREATE TABLE [dbo].[StaOneDay2021] (
  [ID] bigint  NOT NULL,
  [UniqueID] bigint  NOT NULL,
  [StaTime] datetime  NOT NULL,
  [MaxValue] numeric(18,5)  NULL,
  [MaxVTime] datetime  NULL,
  [MinValue] numeric(18,5)  NULL,
  [MinVTime] datetime  NULL,
  [AvgValue] numeric(18,5)  NULL,
  [DataFlag] int DEFAULT ((1)) NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[StaOneDay2021] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneHour_TMP2021
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneHour_TMP2021]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneHour_TMP2021]
GO

CREATE TABLE [dbo].[StaOneHour_TMP2021] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[StaOneHour_TMP2021] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneHour2020
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneHour2020]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneHour2020]
GO

CREATE TABLE [dbo].[StaOneHour2020] (
  [ID] bigint  NOT NULL,
  [UniqueID] bigint  NOT NULL,
  [StaTime] datetime  NOT NULL,
  [MaxValue] numeric(18,5)  NULL,
  [MaxVTime] datetime  NULL,
  [MinValue] numeric(18,5)  NULL,
  [MinVTime] datetime  NULL,
  [AvgValue] numeric(18,5)  NULL,
  [DataFlag] int  NOT NULL,
  [LastUpdateTime] datetime  NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[StaOneHour2020] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneHour2021
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneHour2021]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneHour2021]
GO

CREATE TABLE [dbo].[StaOneHour2021] (
  [ID] bigint  NOT NULL,
  [UniqueID] bigint  NOT NULL,
  [StaTime] datetime  NOT NULL,
  [MaxValue] numeric(18,5)  NULL,
  [MaxVTime] datetime  NULL,
  [MinValue] numeric(18,5)  NULL,
  [MinVTime] datetime  NULL,
  [AvgValue] numeric(18,5)  NULL,
  [DataFlag] int DEFAULT ((1)) NOT NULL,
  [LastUpdateTime] datetime DEFAULT (getdate()) NOT NULL,
  [Row_GUID] varchar(36) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [_MASK_FROM_V2] timestamp  NOT NULL
)
GO

ALTER TABLE [dbo].[StaOneHour2021] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneMinute_TMP20210123
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneMinute_TMP20210123]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneMinute_TMP20210123]
GO

CREATE TABLE [dbo].[StaOneMinute_TMP20210123] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[StaOneMinute_TMP20210123] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneMinute_TMP20210124
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneMinute_TMP20210124]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneMinute_TMP20210124]
GO

CREATE TABLE [dbo].[StaOneMinute_TMP20210124] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[StaOneMinute_TMP20210124] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for StaOneMinute_TMP20210127
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[StaOneMinute_TMP20210127]') AND type IN ('U'))
	DROP TABLE [dbo].[StaOneMinute_TMP20210127]
GO

CREATE TABLE [dbo].[StaOneMinute_TMP20210127] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[StaOneMinute_TMP20210127] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for t_config
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[t_config]') AND type IN ('U'))
	DROP TABLE [dbo].[t_config]
GO

CREATE TABLE [dbo].[t_config] (
  [qsd_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [fsd_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [fsd_begin_time] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [fsd_end_time] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [mark] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [id] varchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [alltime_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [parttime_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [statusIds] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [mc_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [db_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [parttime_menu_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [config_menu_switch] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [mc_switch_on] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [db_switch_on] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [sy_switch_on] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [db14_switch_on] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [parttime_statusIds] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [target_status] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[t_config] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'db14开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'qsd_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新元开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'fsd_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分时段开始时间',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'fsd_begin_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分时段结束时间',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'fsd_end_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'全时段开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'alltime_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分时段开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'parttime_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'全时修改的状态id“,”隔开',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'statusIds'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密采开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'mc_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'数据库修改开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'db_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分时段菜单开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'parttime_menu_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置菜单开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'config_menu_switch'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密采菜单开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'mc_switch_on'
GO

EXEC sp_addextendedproperty
'MS_Description', N'db菜单开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'db_switch_on'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新元菜单开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'sy_switch_on'
GO

EXEC sp_addextendedproperty
'MS_Description', N'db14开关',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'db14_switch_on'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分时修改的状态',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'parttime_statusIds'
GO

EXEC sp_addextendedproperty
'MS_Description', N'目标状态',
'SCHEMA', N'dbo',
'TABLE', N't_config',
'COLUMN', N'target_status'
GO


-- ----------------------------
-- Table structure for t_log_info
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[t_log_info]') AND type IN ('U'))
	DROP TABLE [dbo].[t_log_info]
GO

CREATE TABLE [dbo].[t_log_info] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [file_name] varchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [status] varchar(4) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[t_log_info] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for t_sensor_info
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[t_sensor_info]') AND type IN ('U'))
	DROP TABLE [dbo].[t_sensor_info]
GO

CREATE TABLE [dbo].[t_sensor_info] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [sensor_no] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [sensor_xml_no] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [sensor_location] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [sensor_type] varchar(2) COLLATE Chinese_PRC_CI_AS  NULL,
  [max_alarm] decimal(10,5)  NULL,
  [min_alarm] decimal(10,5)  NULL,
  [alarm_scope] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [note] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_valid] int  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[t_sensor_info] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'点号（编号）',
'SCHEMA', N'dbo',
'TABLE', N't_sensor_info',
'COLUMN', N'sensor_no'
GO


-- ----------------------------
-- Table structure for t_sy_log_info
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[t_sy_log_info]') AND type IN ('U'))
	DROP TABLE [dbo].[t_sy_log_info]
GO

CREATE TABLE [dbo].[t_sy_log_info] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [file_name] varchar(64) COLLATE Chinese_PRC_CI_AS  NULL,
  [status] varchar(4) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[t_sy_log_info] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for t_sy_sensor_info
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[t_sy_sensor_info]') AND type IN ('U'))
	DROP TABLE [dbo].[t_sy_sensor_info]
GO

CREATE TABLE [dbo].[t_sy_sensor_info] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [kuangming] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [fenzhan] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [code] varchar(30) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] varchar(2) COLLATE Chinese_PRC_CI_AS  NULL,
  [name] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [max_alarm] decimal(10,5)  NULL,
  [min_alarm] decimal(10,5)  NULL,
  [alarm_scope] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [note] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_valid] int  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL,
  [status_cd] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [unique_id] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[t_sy_sensor_info] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'点号',
'SCHEMA', N'dbo',
'TABLE', N't_sy_sensor_info',
'COLUMN', N'code'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N't_sy_sensor_info',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上限',
'SCHEMA', N'dbo',
'TABLE', N't_sy_sensor_info',
'COLUMN', N'max_alarm'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下限',
'SCHEMA', N'dbo',
'TABLE', N't_sy_sensor_info',
'COLUMN', N'min_alarm'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用点号也可以找到',
'SCHEMA', N'dbo',
'TABLE', N't_sy_sensor_info',
'COLUMN', N'unique_id'
GO


-- ----------------------------
-- Table structure for t_sy_sensor_info_fsd
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[t_sy_sensor_info_fsd]') AND type IN ('U'))
	DROP TABLE [dbo].[t_sy_sensor_info_fsd]
GO

CREATE TABLE [dbo].[t_sy_sensor_info_fsd] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [kuangming] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [fenzhan] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [code] varchar(30) COLLATE Chinese_PRC_CI_AS  NULL,
  [type] varchar(2) COLLATE Chinese_PRC_CI_AS  NULL,
  [name] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [max_alarm] decimal(10,5)  NULL,
  [min_alarm] decimal(10,5)  NULL,
  [alarm_scope] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [note] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_valid] int  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL,
  [status_cd] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [unique_id] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[t_sy_sensor_info_fsd] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for test_TMP123
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[test_TMP123]') AND type IN ('U'))
	DROP TABLE [dbo].[test_TMP123]
GO

CREATE TABLE [dbo].[test_TMP123] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[test_TMP123] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for test_TMP2020
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[test_TMP2020]') AND type IN ('U'))
	DROP TABLE [dbo].[test_TMP2020]
GO

CREATE TABLE [dbo].[test_TMP2020] (
  [id] bigint  NULL
)
GO

ALTER TABLE [dbo].[test_TMP2020] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Primary Key structure for table Address
-- ----------------------------
ALTER TABLE [dbo].[Address] ADD CONSTRAINT [PK__Address__BCDB8FA31ED998B2] PRIMARY KEY CLUSTERED ([AddrID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table AStatusRecord202101
-- ----------------------------
ALTER TABLE [dbo].[AStatusRecord202101] ADD CONSTRAINT [PK__AStatusR__3214EC271A14E395] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table DeviceInfo
-- ----------------------------
ALTER TABLE [dbo].[DeviceInfo] ADD CONSTRAINT [PK__DeviceIn__1FCC5ABB164452B1] PRIMARY KEY CLUSTERED ([DevLabel])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table RunRecord202101
-- ----------------------------
ALTER TABLE [dbo].[RunRecord202101] ADD CONSTRAINT [PK__RunRecor__3214EC27571DF1D5] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table StaOneDay2021
-- ----------------------------
ALTER TABLE [dbo].[StaOneDay2021] ADD CONSTRAINT [PK__StaOneDa__3214EC27117F9D94] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table StaOneHour2020
-- ----------------------------
ALTER TABLE [dbo].[StaOneHour2020] ADD CONSTRAINT [PK__StaOneHo__3214EC2766603565] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table StaOneHour2021
-- ----------------------------
ALTER TABLE [dbo].[StaOneHour2021] ADD CONSTRAINT [PK__StaOneHo__3214EC275EBF139D] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table t_config
-- ----------------------------
ALTER TABLE [dbo].[t_config] ADD CONSTRAINT [PK_t_config] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for t_log_info
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[t_log_info]', RESEED, 2000)
GO


-- ----------------------------
-- Primary Key structure for table t_log_info
-- ----------------------------
ALTER TABLE [dbo].[t_log_info] ADD CONSTRAINT [PK_13d0da5a7d464cffa9e3afd7804edec] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for t_sensor_info
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[t_sensor_info]', RESEED, 1)
GO


-- ----------------------------
-- Auto increment value for t_sy_log_info
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[t_sy_log_info]', RESEED, 158)
GO


-- ----------------------------
-- Primary Key structure for table t_sy_log_info
-- ----------------------------
ALTER TABLE [dbo].[t_sy_log_info] ADD CONSTRAINT [PK_13d0da5a7d464cffa9e3afd78004edec] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for t_sy_sensor_info
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[t_sy_sensor_info]', RESEED, 10)
GO


-- ----------------------------
-- Primary Key structure for table t_sy_sensor_info
-- ----------------------------
ALTER TABLE [dbo].[t_sy_sensor_info] ADD CONSTRAINT [PK_6eed1930c7c343cab8ba13c3ff95647b] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for t_sy_sensor_info_fsd
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[t_sy_sensor_info_fsd]', RESEED, 9)
GO


-- ----------------------------
-- Primary Key structure for table t_sy_sensor_info_fsd
-- ----------------------------
ALTER TABLE [dbo].[t_sy_sensor_info_fsd] ADD CONSTRAINT [PK_6eed1930c7c343cab8ba13c3ff956472] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

